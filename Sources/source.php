<h2>Source</h2>
<table id="sources" class="tablesorter">
  <tr><th>Source ID</th><td><input id="source_id" type="text"/></td></tr>
  <tr><th>Isotope</th><td><input id="isotope" type="text"/></td></tr>
</table>

<script>
$(function() {
  $("#sources").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_source("<?=$_GET["source_id"];?>");
});

function load_source(source_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_source",source_id:source_id},
    success: function(data) {
      console.log(data);
      sources=JSON.parse(data);
      source=sources[0];
      $("#source_id").val(source['source_id']);
      $("#isotope").val(source['isotope']);
    }
  });
}
</script>
