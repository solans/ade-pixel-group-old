<h2>Add measurement</h2>
<form method="GET" id="add_measurement">
<table id="sources" class="tablesorter">
  <tr><th>Measurement</th><td><input id="measurement_value" type="text"/></td></tr>
  <tr><th>Measurement unit</th><td>
    <select id="measurement_unit">
      <option value=""></option>
      <option value="Bq">Bq</option>
      <option value="kBq">kBq</option>
      <option value="MBq">MBq</option>
    </select>
  </td></tr>
  <tr><th>Measurement date</th><td><input id="measurement_date" type="text"/></td></tr>
</table>
<input type="hidden" id="source_id" value="<?=$_GET["source_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_measurement_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#measurement_date").datepicker({dateFormat:"yy-mm-dd"});
});

$("#add_measurement").submit(function(){
  add_measurement();
  return false;
});

function add_measurement(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"add_measurement",
      source_id:$("#source_id").val(),
      measurement:$("#measurement_value").val(),
      unit:$("#measurement_unit").val(),
      date:$("#measurement_date").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_measurement_reply").text("Something went wrong");
      }else if ("error" in reply){
          $("#add_measurement_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_measurement_reply").text("Measurement stored");
        load_measurements($("#source_id").val());
      }
    }
  });
}
</script>
