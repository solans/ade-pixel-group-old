<h2>Measurement</h2>
<form method="GET" id="measurement">
<table id="sources" class="tablesorter">
  <tr><th>Measurement</th><td><input id="measurement_value" type="text"/></td></tr>
  <tr><th>Measurement unit</th><td>
    <select id="measurement_unit">
      <option value=""></option>
      <option value="Bq">Bq</option>
      <option value="kBq">kBq</option>
      <option value="MBq">MBq</option>
    </select>
  </td></tr>
  <tr><th>Measurement date</th><td><input id="measurement_date" type="text"/></td></tr>
</table>
<input type="hidden" id="measurement_id" value="<?=$_GET["id"];?>">
<input type="hidden" id="source_id" value="<?=$_GET["source_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="measurement_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#measurement_date").datepicker({dateFormat:"yy-mm-dd"});
 load_measurement();
});

$("#measurement").submit(function(){
  update_measurement();
  return false;
});

function load_measurement(){
  $.ajax({
    url: "../dbread.php",
    type: "get",
    data: {
      cmd:"get_measurement",
      source_id:$("#source_id").val(),
      measurement_id:$("#measurement_id").val()
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data);
      reply=rows[0];
      $("#measurement_value").val(reply["measurement"]);
      $("#measurement_unit").val(reply["unit"]);
      $("#measurement_date").val(reply["date"]);
    }
  });  
}

function update_measurement(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"update_measurement",
      source_id:$("#source_id").val(),
      measurement_id:$("#measurement_id").val(),
      measurement:$("#measurement_value").val(),
      unit:$("#measurement_unit").val(),
      date:$("#measurement_date").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#measurement_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#measurement_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#measurement_reply").text("Measurement stored");
        load_measurement();
      }
    }
  });
}
</script>
