<h2>Measurements</h2>
<button id="measurements_reset">Reset filters</button>
<button id="measurements_export">Export</button>
<div id="measurements_found" style="display:inline-block"></div>
<table id="measurements" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Measurement date</th>
		<th>Actions</th>
	</thead>
	<tbody id="measurements_body">
	</tbody>
</table>

<script>

$(function() {
  $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_measurements("<?=$_GET["source_id"];?>");
});

$("#measurements").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#measurements").on("filterEnd",function(){
  $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2))
});

$("#measurements_export").click(function() {
  $("#measurements").trigger("outputTable");
});

$("#measurements_reset").click(function() {
  $("#measurements").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_measurements(source_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_measurements",source_id:source_id},
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data);
      $("#measurements_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["measurement"]+"&nbsp;"+row["unit"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td><a href=\"index.php?page=measurement&source_id="+row["source_id"]+"&id="+row["id"]+"\">update</td>";
        tt+="</tr>\n"; 
        $("#measurements_body").append(tt);
      }
      $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2));
    }
  });
}
</script>
