<?php
//echo "hello";
include_once("includes.php");

//echo "open";
$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}

$d=$_GET;

if($d["cmd"]=="get_source"){
  $sql ="SELECT * FROM sources WHERE source_id='".$d["source_id"]."';";
}
else if($d["cmd"]=="get_sources_summary"){
  $sql ="SELECT * FROM sources ";
  $sql.="LEFT JOIN (";
  $sql.=" SELECT locations.source_id, locations.date AS location_date, location FROM locations ";
  $sql.=" INNER JOIN (SELECT source_id, max(date) AS date FROM locations GROUP BY source_id) AS aux ON aux.source_id=locations.source_id AND aux.date=locations.date ";
  $sql.=") as t1 ON t1.source_id = sources.source_id ";
  $sql.="LEFT JOIN ( "; 
  $sql.=" SELECT measurements.source_id AS source_id, measurements.date AS measurement_date, measurement, unit AS measurement_unit FROM measurements ";
  $sql.=" INNER JOIN (SELECT source_id, max(date) AS date FROM measurements GROUP BY source_id) AS aux ON aux.source_id=measurements.source_id AND aux.date=measurements.date ";
  $sql.=") as t2 ON t2.source_id = sources.source_id;";
}
else if($d["cmd"]=="get_measurement"){
  $sql ="SELECT * FROM measurements WHERE id='".$d["measurement_id"]."';";
}
else if($d["cmd"]=="get_measurements"){
  $sql ="SELECT * FROM measurements WHERE source_id='".$d["source_id"]."';";
}
else if($d["cmd"]=="get_location"){
  $sql ="SELECT * FROM locations WHERE id='".$d["location_id"]."';";
}
else if($d["cmd"]=="get_locations"){
  $sql ="SELECT * FROM locations WHERE source_id='".$d["source_id"]."';";
}
  
//echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>