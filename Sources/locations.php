<h2>Locations</h2>
<button id="locations_reset">Reset filters</button>
<button id="locations_export">Export</button>
<div id="locations_found" style="display:inline-block"></div>
<table id="locations" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Measurement date</th>
	</thead>
	<tbody id="locations_body">
	</tbody>
</table>

<script>

$(function() {
  $("#locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_locations("<?=$_GET["source_id"];?>");
});

$("#locations").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#locations").on("filterEnd",function(){
  $("#locations_found").html("Found: "+($("#locations tr:visible").length-2))
});

$("#locations_export").click(function() {
  $("#locations").trigger("outputTable");
});

$("#locations_reset").click(function() {
  $("#locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_locations(source_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_locations",source_id:source_id},
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data);
      $("#locations_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="</tr>\n"; 
        $("#locations_body").append(tt);
      }
      $("#locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#locations_found").html("Found: "+($("#locations tr:visible").length-2));
    }
  });
}
</script>
