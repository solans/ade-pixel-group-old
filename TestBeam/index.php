<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Test-beam</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Test-beam</p>

Test-beam activities of the group are connected with sensor qualification.

<ul>
  <li><a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/PHADEFECharacterization">EP-ADE-TK Testbeam Twiki</a></li>
  <li><a href="https://ade-pixel-testbeam.web.cern.ch">EP-ADE-TK Testbeam Web Interface</a></li>
  <li><a href="sps.php">MALTA TB SPS</a></li>
</ul>

<p class="SUBTITLE">MALTA telescope</p>

The MALTA telescope is based on MALTA pixel detector planes with stand-alone control software with ethernet based read-out.
It is compatible with AIDA EUDAQ TLU and read-out system.

<p></p><img class="IMAGE" src="malta-telescope-sps.png">

<p class="SUBTITLE">Presentations</p>
<p>
  <ul>
    <li><a href="https://indico.cern.ch/event/1058977/contributions/4631218/attachments/2465399/4227716/BTTB2021.pdf">M. van Rijnbach, BTTB 10, June 2022</a></li>
    <li><a href="https://indico.cern.ch/event/945675/contributions/4174397/attachments/2186683/3694842/andrea_BTTB2021.pdf">A. Gabrielli, BTTB 9, Feb 2021</a></li>
    <li><a href="https://indico.cern.ch/event/813822/contributions/3653067/attachments/1978263/3294212/mdyndal_Malta_bttb8.pdf">M. Dyndal, BTTB 8, Jan 2020</a></li>
    <li><a href="https://indico.cern.ch/event/731649/contributions/3237195/attachments/1780433/2896319/Ignacio_MALTA_20190117.pdf">I. Asensi, BTTB 7, Jan 2019</a></li>
  </ul>
</p>

<p class="SUBTITLE">SBM telescope</p>

A Silicon Beam Monitor (SBM) Telescope provides the reference planes for track reconstruction.
It is a multi-purpose FE-I4 based telescope with and RCE HSIO-II read-out using a Pixel interface board.

<p></p><img class="IMAGE" src="sbm-telescope.png">



<p class="SUBTITLE">Documentaion</p>
<p>
  <ul>
    <li><a href="https://indico.cern.ch/event/361445/contributions/1776527/attachments/719550/987715/SBM-short_presentation.pdf">SBM telecope details</a></li>
    <li><a href="setup_slides.pdf">SBM telescope setup slides</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/PHADEFECharacterization">EP-ADE-ID Testbeam Twiki</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/TestbeamPixelCmos">Pixel CMOS testbeam 2017</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Main/TestBeam_May">May 2015 testbeam log with SBM01</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Main/ATLASSBMAIDATELESCOPEUserGuide">May 2015 telecope setup with SBM01</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Sandbox/MagneticfieldtestbeamJune2015">June 2015 telecope setup with SBM02</a></li>
    <li><a href="https://twiki.cern.ch/twiki/pub/Sandbox/MagnetTBJuly2015/SBM02-Documentation.pdf">June 2015 SBM02 HVCMOS</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Sandbox/MagnetTBJuly2015">July 2015 testbeam log with SBM02</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/RCEDevelopmentLab">RCE twiki</a></li>
    <li><a href="https://dfsweb.web.cern.ch/dfsweb/Services/DFS/DFSBrowser.aspx/Experiments/PH-ADE-ID-TestBeam/data/HVCMOS/simon">HVCMOS Testbeam data</a></li>    
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
