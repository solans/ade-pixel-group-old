<?
include_once("helper.php");
include_once("conference.php");
include_once("querystring.php");

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    

$collection=$helper->GetData();

?>
<p class="SUBTITLE">List of conferences</p>

<table style="text-align:left; width:100%" id="main">
  <thead class="tablesorter">
  <?
  $conf=new Conference();
  foreach($conf->GetMembers() as $k){
    if($conf->id==$k){continue;}
    echo '  <td>'.$conf->GetLabel($k).'</td>'."\n";
  }
  echo '  <td>Actions</td>'."\n";
  ?>
  </thead>
  <tbody class="tablesorter">
  <?
  foreach($collection->GetDataOrderBy("startdate","DESC") as $p){
    ?>
    <tr>
      <?
      foreach($conf->GetMembers() as $k){
        if($conf->id==$k){continue;}
        echo '  <td>'.$p->GetValue($k).'</td>'."\n";
      }

      ?>
      <td>
        <a href="<? $qs->set('action','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
        <a href="<? $qs->set('action','Remove'); $qs->set('id',urlencode($p->GetId())); echo 'logic.php'.$qs->toFullString()?>" 
          onclick="return confirm('Are you sure to delete this conference?')">delete</a> 
      </td>
      <?

      ?>
    </tr>
    <?
  }
  ?>
</tbody>
</table>
