<?
include_once("helper.php");
include_once("conference.php");
include_once("querystring.php");

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$qs = new querystring();
?>
<p class="SUBTITLE">Add a new conference</p>
<?
$conf=new Conference();
echo $conf->ToHtmlForm(array("url"=>$gobase."Conferences/logic.php", "method"=>"POST", "action"=>"Add"));  
?>


