<?
include_once("helper.php");
include_once("conference.php");
include_once("querystring.php");

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
?>

<p class="SUBTITLE">Edit a conference</p>
<?
$collection = $helper->GetData();
$conference=$collection->Get($qs->get("id"));
if(!$conference){echo "<h3>Conference not found: ".$qs->get("id")."<h3>";}
else{
  echo $conference->ToHtmlForm(array("url"=>"logic.php", "method"=>"POST", "action"=>"Edit", "redirect"=>"?action=edit&id=".$qs->get("id")));  
}
?>
<p>
<a onclick="return confirm('Are you sure to delete the conference?')" href="<? $qs->set('action','Remove'); $qs->set('id',urlencode($qs->get("id"))); echo 'logic.php'.$qs->toFullString()?>">Delete the conference</a> 
</p>