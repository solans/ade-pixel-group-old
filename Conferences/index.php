<?php
include_once('../functions.php');
include_once('../scripts/tablesorter.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<?php $table_sorter->show_includes();?>
<title>Conferences</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php   
  show_login(); 
?>
<p class="TITLE">Conferences</p>
<?  
  include_once("querystring.php");
  $qs=new querystring();
  
  $qs_back=new querystring();
  $qs_back->remove("action");
  $qs_back->remove("id");  
  $qs_list=new querystring();
  $qs_list->set("action","list");
  $qs_list->remove("id");
  $qs_add =new querystring();
  $qs_add->set("action","add");
  $qs_add->remove("id");
  $a_back='<a href="index.php'.$qs_back->toFullString().'">Back</a>';
  $a_list='<a href="'.$qs_list->toFullString().'">List</a>';
  $a_add ='<a href="'.$qs_add ->toFullString().'">Add</a>';
    
  if(@$qs->get("action")=="add"){
    echo $a_list;
    include_once("add.php");
  }else if(@$qs->get("action")=="edit" && @$qs->get("id")>=0){
    echo $a_add."&nbsp;".$a_list;
    include_once("edit.php");    
  }else{
    echo $a_add;
    include_once("list.php");
  }
?>
</div>
<?php
  show_footer();
  $table_sorter->show_body();
?>
</div>

</body>
</html>

