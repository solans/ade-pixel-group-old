<?
/** Dec 2020 **/
include_once("../functions.php");
include_once("helper.php");
include_once("conference.php");
include_once("querystring.php");

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$collection = $helper->GetData();

$app = "https://ade-pixel-group.web.cern.ch/Conferences/";
    
if(@$_POST["action"]=="Add"){
    
  $obj = new Conference();
  $_POST["id"]=$collection->GetNextId();
  $obj->FromArray($_POST);
  $collection->Add($obj);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');

  //redirect 
  $qs = new querystring();
  $qs->remove('action');
  $qs->remove('id');
  redirect($app.$qs->toFullString());
  
}else if(@$_POST["action"]=="Edit"){
   
  //Add to collection
  $obj = $collection->Get($_POST["id"]);
  $obj->FromArray($_POST);
  $collection->Del($obj->GetId());
  $collection->Add($obj);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');
  
  //redirect 
  $qs = new querystring();
  $qs->remove('action');
  $qs->remove('id');

  //$obj->Dump();print_r($_POST);exit();
  if(@$_POST["redirect"]){redirect($app.$_POST["redirect"]);}
  redirect($app.$qs->toFullString());

}else if(@$_GET["action"]=="Remove"){

  //Delete from collection
  $obj = $collection->Get($_GET["id"]);
  $collection->Del($_GET["id"]);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');
  
  //redirect 
  $qs = new querystring();
  $qs->set('action','list');
  $qs->remove('id');
  redirect($app.$qs->toFullString());

}else{
  print_r($_POST);
  echo "<h1>Nothing to do here</h1>";
}

?>