<?
/* Date: December 2020 */

include_once("helper.php");

Helper::AddType("Conference");

class Conference extends Collectible{

  function __construct(){
    $this->members = array(
      "id"=>array("type"=>"string","label"=>"id","display"=>"hidden","value"=>"0"),
      "name"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Conference","value"=>""),
      "startdate"=>array("type"=>"date","required"=>true,"display"=>"50","label"=>"Start date","value"=>""),
      "venue"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Venue","value"=>""),
      "webpage"=>array("type"=>"string","required"=>false,"display"=>"50","label"=>"Conference webpage","value"=>""),
      "speaker"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Speaker","value"=>""),
      /*"contribution"=>array("type"=>"string","required"=>false,"display"=>"50","label"=>"Contribution","value"=>""),*/
      "contribution"=>array("type"=>"enum","values"=>array("","Oral","Poster"),"required"=>false,"display"=>"50","label"=>"Contribution","value"=>""),
    );
    $this->classname = "Conference";
    $this->tag = "conference";
    $this->id = "id";
  }
  
}

?>