<?
class Publication{
    var $id;
    var $title; 
    var $author; 
    var $year; 
    var $journal;
    var $volume;
    var $note;
    var $reference;
    var $url;
    var $doi;
    var $tags;
	 
  //Constructor	
	function __construct(){
	  $this->tags=array();
	}
  
	//Dump
	function Dump(){
		?>
		<?=$this->author;?>,&nbsp;
        <?=$this->title;?>,&nbsp;
		<?=$this->journal;?>,&nbsp;
		<?=$this->year;?>,&nbsp;
        <?=$this->volume;?>,&nbsp;
        <?=$this->note;?>,&nbsp;
        <a target="_blank" href="<?=$this->url;?>">link</a>
		<?
    }
  
	//Dump to XML
	function ToXml($ident=0){
        $s = str_repeat(" ",$ident);
		$xml = $s."<publication>\n";
		$xml .= $s.$s."<id>".$this->id."</id>\n";
		$xml .= $s.$s."<note>".$this->note."</note>\n";
		$xml .= $s.$s."<title>".$this->title."</title>\n";
        $xml .= $s.$s."<author>".$this->author."</author>\n";
        $xml .= $s.$s."<year>".$this->year."</year>\n";
        $xml .= $s.$s."<journal>".$this->journal."</journal>\n";
        $xml .= $s.$s."<volume>".$this->volume."</volume>\n";
        $xml .= $s.$s."<doi>".$this->doi."</doi>\n";
        $xml .= $s.$s."<reference>".$this->reference."</reference>\n";
        $xml .= $s.$s."<url>".$this->url."</url>\n";
        foreach($this->tags as $tag){
            $xml.= $s.$s."<tag>".$tag."</tag>\n";
        }
		$xml .= $s."</publication>\n";
		return $xml;
    }
}

class Publications{

    //internal data
	var $data;
  
    //Constructor
	  function __construct(){
        $this->data = array();
    }
	
	//dump function
	function Dump(){
		?>
		<ul>
		<?
        foreach($this->data as $c){
			?>
			<li><?=$c->Dump();?></li>
			<?
		}
		?>
		</ul>
		<?
    }
  
    //helper
	function AddPublication($p){
		$this->data[]=$p;
	}
	
	//Get a given publications
	function GetPublication($id){
		foreach($this->data as $e){
			if($e->id == $id){
				return $e;
			}
		}
		return NULL;
  }
  
	//Remove a given publications
	function DelPublication($id){
		for($i=0;$i<count($this->data);$i++){
			if($this->data[$i]->id==$id){
				unset($this->data[$i]);
			}
		}
	}
	
    //Dump to XML
	function ToXml(){
	  	$nn = "\n";
		$xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'.$nn;
		$xml .= '<publications>'.$nn;
		foreach($this->data as $e){
			$xml .= $e->ToXml(1);
		}
		$xml .= '</publications>'.$nn;
		return $xml;
    }
    
    function GetLastId(){
        $id=0;
        for($i=0;$i<count($this->data);$i++){
            if(intval($this->data[$i]->id)>$id){
                $id=$this->data[$i]->id;
            }
        }
        return $id;
    }
  
}

class Publications_Helper{
  
    //data
    var $publications;
	
    //helpers
    var $publication;
  
    //internal
    var $verbose;  
	
	function __construct(){
		$this->tag = "";
		$this->publications = new Publications();
	}

    // helper function
	function GetData(){
        return $this->publications;
    }
	
    // Open tag
	function startElement($parser, $name, $attrs){
		if($this->verbose){echo "Open tag: ".$name."<br>\n";}
		$this->tag=$name;
		switch($this->tag){
			case "PUBLICATION":
				$this->publication = new Publication();
				break;
        }
    }

    // Close tag
	function endElement($parser, $name){
		if($this->verbose){echo "Close tag: ".$name."<br>\n";}    
		switch($name){
		case "PUBLICATION":
            $this->publications->addPublication($this->publication);
			break;
		}
		$this->tag="";
    } 
	
	// Value inside a tag
	function characterData($parser, $data){
		if($this->verbose){echo "Data: ".$data."<br>\n";}
		switch($this->tag){
		    case "ID":
		        $this->publication->id.=$data;
		        break;
		    case "NOTE":
                $this->publication->note.=$data;
				break;
		    case "DOI":
		        $this->publication->doi.=$data;
		        break;
		    case "REFERENCE":
      	        $this->publication->reference.=$data;
				break;
			case "TITLE":
      	        $this->publication->title.=$data;
				break;
			case "AUTHOR":
                $this->publication->author.=$data;
                break;
            case "YEAR":
                $this->publication->year.=$data;
                break;
            case "JOURNAL":
                $this->publication->journal.=$data;
                break;
            case "VOLUME":
                $this->publication->volume.=$data;
                break;
            case "REFERENCE":
                $this->publication->reference.=$data;
                break;
            case "URL":
      	        $this->publication->url.=$data;
				break;
            case "TAG":
                $this->publication->tags[]=$data;
		        break;  
		    }
	}
}
?>