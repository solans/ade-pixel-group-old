<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>MALTA Interface Board</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">MALTA Interface Board V4</p>
<p>The MALTA interface board is designed to provide extra input and outputs for the MALTA read-out.
</p>

<img class="IMAGE" alt="MALTA-interface-V4" src="MALTA-interface-V4.png">

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/2142554/">Project in EDMS</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
