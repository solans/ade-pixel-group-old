<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>PCB design</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">PCB desgin</p>

<p class="SUBTITLE">PCB 
  <ul>

    <li><a href="<?=$gobase;?>VhdciHdmiA">VHDCI-HDMI-A</a></li>
    <li><a href="<?=$gobase;?>VHDCI-GBT-SFP">VHDCI-GBT-SFP</a></li>
    <li><a href="<?=$gobase;?>ERF-8xDisplayPort">ERF-8xDisplayPort</a></li>
    <li><a href="<?=$gobase;?>PCBs/MALTA-interface-V4.php">MALTA Interface Board</a></li>
    
  </ul>
</p>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://em-design-office-data.web.cern.ch/altium_lib.asp">CERN Altium library</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
