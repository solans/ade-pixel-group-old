<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>ERF-8xDisplayPort</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">ERF-8xDisplayPort</p>
<p>The ERF-8xDisplayPort board is an interface designed for the FELIX read-out of the ITK demonstrator prototype with RD53A.
This board connects seamlessly to the Optoboard through the ERF and to 8 single chip RD53A cards through the Display Ports.</p>

<img class="IMAGE" alt="ERF-8xDisplayPort_small.jpeg" src="ERF-8xDisplayPort_small.jpeg">

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/2282687/">Project in EDMS</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
