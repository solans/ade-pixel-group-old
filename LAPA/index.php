<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>LAPA</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">LAPA: A pseudo-LVDS driver for Pixel detectors</p>

<img class="IMAGEMEDIUM" src="images/lapa-diagram.png"/>

<p class="SUBTITLE">Main details</p>
<ul>
    <li>Modular selectable HBRIDGE (7x 0.8 mA )</li>
    <li>Modular capacitive coupled  pre-emphasis : 16 blocks driving 25 fF each </li>
    <li>Common mode Voltage feedback at 0.8 V</li>
    <li>External 100 Ohm termination</li>
</ul>
<p class="SUBTITLE">Power consumption at 1.28 Gbps</p>
    <table style="border:1px">
        <tr><td></td><td>Current [mA]</td><td>Power [mW]</td></tr>
        <tr><td>5 Hbridge</td><td>6 (4)</td><td>10.8 (7.2)</td></tr>
        <tr><td>7 Hbridge</td><td>5.2 (5.2)</td><td>14.4 (10)</td></tr>
    </table>
<img class="IMAGEMEDIUM" src="images/lapa-eye-diagram.png"/>

<p class="SUBTITLE">LAPA test chip</p>
<ul>
    <li>10 Input CMOS/LVDS channels</li>
    <li>10 output CMOS/LVDS channels</li>
    <li>CHIP input configurable CMOS/LVDS </li>
    <li>Channel output configurable CMOS/LVDS </li>
    <li>Power Supply 1.8V</li>
    <li>Internal Input termination resistor 100Ohm</li>
    <li>Tunable LVDS pre emphasis. </li>
    <li>Configuration pads with internal pull-up, all active high.</li>
    <li>LVDS OUTPUT with selectable VCM internal feedback</li>
</ul>


<img class="IMAGEMEDIUM" src="images/lapa-test-chip-footprint.png"/>
<img class="IMAGEMEDIUM" src="images/lapa-test-chip-on-pcb.png"/>

<p class="SUBTITLE">How to operate the LAPA test chip</p>


<p class="SUBTITLE">Documentation</p>
<ul>
  <li><a href="https://edms.cern.ch/document/2766470/">LAPA test chip specifications</a></li> 
  <li><a href="https://edms.cern.ch/document/2450642/">LAPA test chip wire-bonding diagram</a></li>
  <li><a href="https://pos.sissa.it/313/038/pdf">Proceedings of TEWPP 2017</a></li>
  <li><a href="https://cds.cern.ch/record/2702969">CMOS detector and system developments for LHC detector upgrades. Roberto Cardella. Univ of Oslo. Nov 2019</a></li>
</ul>

<p class="SUBTITLE">Presentations
<ul>
    <li><a href="https://indico.cern.ch/event/734767/contributions/3039208/attachments/1668705/2676122/TJ_DDJUNE2018.pdf">R. Cardella, EP-DT-DD group meeting, June 2018</a></li>
    <li><a href="https://indico.cern.ch/event/653120/contributions/2660768/attachments/1492620/2323621/LAPA_specs_28062017.pdf">R. Cardella, MALTA read-out meeting, June 2017</a></li>
    <li><a href="https://indico.cern.ch/event/737102/contributions/3041196/attachments/1668110/2675003/LAPA_Glasgow_results_barrelflexMeeting.pdf">R. Cardella, CMOS TJ design meeting, June 2018</a></li>
<ul>
</p>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
