<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Staves</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

 <div class="CONTENT">

<p class="TITLE">Stave prototyping</p>

<img class="MEDIUMIMAGE" src="shell_longeron.png">


<p class="SUBTITLE">Links</p>
<ul>
  <li><a href="https://indico.cern.ch/category/1904/">ITK mechanics prototyping meeting agendas</a></li>
  <li><a href="https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10198300">Egroup: ep-ade-itk-mechanics-prototyping</a></li>
</ul>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
