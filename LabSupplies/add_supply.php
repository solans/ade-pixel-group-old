<h2>Add supply</h2>
<form method="GET" id="add_supply">
<table class="tablesorter">
  <tr><th>Reference</th><td><input id="supply_reference" type="text" style="width:500px;"/></td></tr>
  <tr><th>Store</th><td>
    <select id="supply_store">
      <option value=""></option>
      <option value="CERN store">CERN Stores</option>
      <option value="Digikey">Digikey</option>
      <option value="Distrelec">Distrelec</option>
      <option value="Farnell">Farnell</option>
      <option value="RS">RS</option>
    </select>
  </td></tr>
  <tr><th>Manufacturer part number</th><td><input id="supply_mpn" type="text" style="width:500px;"/></td></tr>
  <tr><th>Description</th><td><input id="supply_description" type="text" style="width:500px;"/></td></tr>
  <tr><th>URL</th><td><input id="supply_url" type="text" style="width:500px;"/>&nbsp;<button id="supply_default_url">default</button><button id="supply_go">Go</button></td></tr>
</table>
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_supply_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#supply_date").datepicker({dateFormat:"yy-mm-dd"});
});

$("#add_supply").submit(function(){
  add_supply();
  return false;
});

$("#supply_default_url").click(function(){
  if (!$("#supply_reference").val()){return;}
  url="";
  if ($("#supply_store").val()=="RS"){
    url="http://fr.rs-online.com/web/p/products/"+$("#supply_reference").val().replaceAll("-","");
  }else if ($("#supply_store").val()=="CERN store"){
    url="https://edh.cern.ch/edhcat/Browser?scem="+$("#supply_reference").val()+"&command=searchItems";
  }else if ($("#supply_store").val()=="Distrelec"){
    url="https://www.distrelec.ch/fr/p/"+$("#supply_reference").val().replaceAll("-","");
  }else if ($("#supply_store").val()=="Farnell"){
    url="https://fr.farnell.com/"+$("#supply_reference").val().replaceAll("-","");
  }
  $("#supply_url").val(url);
  return false;
});

$("#supply_go").click(function(){
  if(!$("#supply_url").val()){return false;}
  window.open($("#supply_url").val(),'_blank');
  return false;
});

function add_supply(){
  $.ajax({
    url: "dbwrite.php",
    type: "get",
    data: {
      cmd:"add_supply",
      reference:$("#supply_reference").val(),
      store:$("#supply_store").val(),
      mpn:$("#supply_mpn").val(),
      description:$("#supply_description").val(),
      url:$("#supply_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_supply_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_supply_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_supply_reply").text("Supply stored");
        load_supplies();
      }
    }
  });
}
</script>
