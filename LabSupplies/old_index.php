<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>ATLAS-icon.ico">
<title>Laboratory supplies</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">
<?php   
  show_certificate(); 
  show_login(); 
?>

<?php
  $authorised=isAuthorised();
?>

<p class="TITLE">Laboratory supplies</p>

<table>
	<?
		
		
		include_once("parser.php");
		include_once("helper.php");
    include_once("material.php");
	
		$filename = getcwd()."/data/data.xml";
		$parser = new Parser();
		$helper = new Helper();
    $helper->AddType("Material");
		$helper->verbose = (@$_GET["debug"]=="true"?true:false);
		
		$parser->SetFilename($filename);
		$parser->SetDataHolder($helper);
		$parser->Parse();
		
		$collection = $helper->GetData();
		
		include_once("querystring.php");
		$qs = new querystring();
	?>
		<tr>
			<td> 
			<? 
			if(@$_GET["action"]=="admin" && $authorised){
				$qs->remove("action");
				$qs->remove("do");
				$qs->remove("id");
				?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">View</a><? 
				$qs->set("action",$_GET["action"]);
				if(isset($_GET["do"])) $qs->set("do",$_GET["do"]);
				if(isset($_GET["id"])) $qs->set("id",$_GET["id"]);
			}else if($authorised){
        $qs->remove("do");
        $qs->remove("id");
			  $qs->set("action","admin");
        ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">Admin</a><? 
      }
			?>
			</td>
		</tr>
		<?
	  
		function randomstring(){
			$arr = str_split('abcdefghijklmnopqrstuwxyz'); // get all the characters into an array
    	shuffle($arr); // randomize the array
    	$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    	return implode('', $arr); // smash them back into a string
		}
		
		if(@$_GET["action"]=="admin" && $authorised){	
			
			if(@$_POST["action"]=="add"){
				//Pre-process POST
				//continue
				$material = new Material();
                $material->FromArray($_POST);
                //Append to file
				$collection->Add($material);
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->set('action','admin');
				$qs->remove('do');
				$qs->remove('id');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
			}else if(@$_POST["action"]=="edit"){
				$material = $collection->Get($_GET["id"]);
				$material->FromArray($_POST);
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->set('action','admin');
				$qs->remove('do');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
			}else if(@$_GET["do"]=="remove" && $authorised){
				$collection->Del($_GET["id"]);
				$collection->Dump();
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->remove('do');
				$qs->remove('id');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
		}else{
			?>
			<tr><td style="text-align:left">
				<table style="text-align:left; width:100%">
       	       <?
				foreach($collection->data as $p){
				?>
                <tr>
					<td><a href="<?=$p->GetMember('url');?>" target="blank"><?=$p->GetMember("reference");?></a></td>
                    <td><?=$p->GetMember("name");?></td>
                    <td><a href="<? $qs->set('do','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
						<a href="<? $qs->set('do','remove'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">delete</a> 
                    </td>
					</tr>
					<?
				} 
				?>  
				</table>
 			</div>
			<a href="<? $qs->remove('do'); $qs->remove('id'); echo $_SERVER['PHP_SELF'].'?'.$qs->toString();?>" >Add</a>
			<form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
				<input type="hidden" name="action" value="add"/>
				<label style="display:inline-block;width:100px;">Reference:</label><input type="text" name="reference" size="80"/><br/>
				<label style="display:inline-block;width:100px;">Name:</label><input type="text" name="name" size="80"/><br/>
				<label style="display:inline-block;width:100px;">Store:</label><input type="text" name="store" size="80"/><br/>
                <label style="display:inline-block;width:100px;">URL:</label><input type="text" name="url" size="80"/><br/>
                <input name="submit" type="submit" value="Add" /><br>
                * Required fields
			</form>
        <?
				if(@$_GET["do"]=="edit" && $authorised){
					$cc = $collection->Get($_GET["id"]);
					?>
					<script>
				 		<? 
                        foreach($cc->GetData() as $k=>$v){
                            ?>document.forms["editor"].elements["<?=$k;?>"].value='<?=$v;?>';<?
                        }
                        ?>
                    document.forms["editor"].elements["action"].value='edit';
                    document.forms["editor"].elements["submit"].value='Update';
                    </script>
					<?
				}
			}
			?>
			</td>
		</tr>
		<?
		}else{
			?>
			<tr>
        <td>Reference</td>
        <td>Name</td>
      </tr>
      <?
        foreach($collection->data as $p){
          ?>
           <tr>
             <td><a href="<?=$p->GetMember('url');?>" target="blank"><?=$p->GetMember("reference");?></a></td>
             <td><?=$p->GetMember("name");?></td>
           </tr>
          <?
        }   
			}
		?>
	</table>
</div>
<?
show_footer();
?>
</body>
</html>