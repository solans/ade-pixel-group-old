<h2>Supply</h2>
<form method="GET" id="supply">
<table class="tablesorter">
  <tr><th>Reference</th><td><input id="supply_reference" type="text" style="width:500px;"/></td></tr>
  <tr><th>Store</th><td>
    <select id="supply_store">
      <option value=""></option>
      <option value="CERN store">CERN Stores</option>
      <option value="Digikey">Digikey</option>
      <option value="Distrelec">Distrelec</option>
      <option value="Farnell">Farnell</option>
      <option value="RS">RS</option>
    </select>
  </td></tr>
  <tr><th>Manufacturer part number</th><td><input id="supply_mpn" type="text" style="width:500px;"/></td></tr>
  <tr><th>Description</th><td><input id="supply_description" type="text" style="width:500px;"/></td></tr>
  <tr><th>URL</th><td><input id="supply_url" type="text" style="width:500px;"/>&nbsp;<button id="supply_default_url">default</button></td></tr>
</table>
<input type="hidden" id="supply_id" value="<?=$_GET["id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="supply_reply" style="display:inline-block;"></div>

<script>
$(function() {
  load_supply();
});

$("#supply").submit(function(){
  update_supply();
  return false;
});

$("#supply_default_url").click(function(){
  if (!$("#supply_reference").val()){return;}
  url="";
  if ($("#supply_store").val()=="RS"){
    url="http://fr.rs-online.com/web/p/products/"+$("#supply_reference").val().replaceAll("-","");
  }else if ($("#supply_store").val()=="CERN store"){
    url="https://edh.cern.ch/edhcat/Browser?scem="+$("#supply_reference").val()+"&command=searchItems";
  }else if ($("#supply_store").val()=="Distrelec"){
    url="https://www.distrelec.ch/fr/p/"+$("#supply_reference").val().replaceAll("-","");
  }
  $("#supply_url").val(url);
  return false;
});

function load_supply(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_supply",
      id:$("#supply_id").val() 
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      supply=reply[0];
      $("#supply_reference").val(supply['reference']);
      $("#supply_store").val(supply['store']);
      $("#supply_mpn").val(supply['mpn']);
      $("#supply_description").val(supply['description']);
      $("#supply_url").val(supply['url']);
    }
  });
}

function update_supply(){
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:"update_supply",
      id:$("#supply_id").val(),
      reference:$("#supply_reference").val(), 
      store:$("#supply_store").val(), 
      mpn:$("#supply_mpn").val(), 
      description:$("#supply_description").val(), 
      url:$("#supply_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#supply_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#supply_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#supply_reply").text("Supply stored");
      }
    }
  });
}
</script>
