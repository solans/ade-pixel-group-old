<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Hardware</title>
<style>
/*
 table {
    border-collapse: collapse;
}
table, th, td {
    border: 1px solid black;
    padding: 15px;
}
*/  
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<?
$repo="https://gitlab.cern.ch/solans/PySerialComm/blob/master/python/";
?>

<div class="CONTENT">

<p class="TITLE">Hardware control libraries</p>

<p>Below is a list of python classes that can be easily used to quickly setup the devices for remote operations.</p>

<div class="nicetable">
<table>
<tr>
  <td><strong>File</td>
  <td><strong>Description</td>
  <td><strong>Reference</td>
</tr>
<tr>
  <td><a href="<?=$repo;?>SerialCom.py">SerialCom.py</a></td>
  <td>SerialCom base class for FTDI USB to COM</td>
  <td></td>
</tr>
<tr>
  <td><a href="<?=$repo;?>Keithley.py">Keithley.py</a></td>
  <td>Keithley 2410 Power Supply</td>
  <td><a href="Keithley2400Manual.pdf">Manual</a></td>
</tr>
<tr>
  <td><a href="<?=$repo;?>TTi.py">TTi.py</a></td>
  <td>TTi PL303QMD-P Dual Power Supply</td>
  <td><a href="TTi_PL303QMD-P_Manual.pdf">Manual</a></td>
</tr>
<tr>
  <td><a href="<?=$repo;?>Newport.py">Newport.py</a></td>
  <td>Newport Motion Controller ESP301</td>
  <td><a href="ESP301_Manual.pdf">Manual</a></td>
</tr>
<tr>
  <td><a href="<?=$repo;?>Iseg.py">Iseg.py</a></td>
  <td>Iseg SHQ-224M communication class</td>
  <td><a href="SHQx2x_11_eng.pdf"> Manual</a></td>
</tr>
</table>
</div>

<p style="height:200px"></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
