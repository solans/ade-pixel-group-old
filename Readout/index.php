<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/ATLAS-icon.ico">
<title>Readout systems</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Readout and Software</p>

<p class="SUBTITLE">FELIX</p>
From the ATLAS Phase-I upgrade and onward, new or upgraded detectors and 
trigger systems will be interfaced to the data acquisition, detector control and 
timing (TTC) systems by the Front-End Link eXchange (FELIX). 
FELIX is the core of the new ATLAS Trigger/DAQ architecture. 
Functioning as a router between custom serial links and a commodity network, 
FELIX is implemented by server PCs with commodity network interfaces and 
PCIe cards with large FPGAs and many high speed serial optical fiber transceivers. 
By separating data transport from data manipulation, the latter can be done 
by software in commodity servers attached to the network. Replacing traditional 
point-to-point links between Front-end components and the DAQ system by a switched 
network, FELIX provides scaling, flexibility uniformity and upgradability. 
Different Front-end data types or different data sources can be routed to different 
network endpoints that handle that data type or source. This reduces the diversity 
of custom hardware solutions in favour of software. Front-end connections can be 
either high bandwidth serial connections from FPGAs (e.g. 10 Gb/s) or those from 
the radiation tolerant CERN GBTx ASIC which aggregates many slower serial links 
onto one 5 Gb/s high speed link. Already in the Phase 1 Upgrade there will be 
about 2000 fiber connections. In addition to connections to a commodity network and 
Front-ends, FELIX receives Timing, Trigger and Control (TTC) information and 
distributes it with fixed latency to the GBTx connections.

<ul>
  <li><a href="<?=$gobase;?>/Felix">More information</a></li>
  <li><a href="https://atlas-project-felix.web.cern.ch">Project page</a></li>
  <li><a href="https://gitlab.cern.ch/atlas-tdaq-felix">Gitlab repository</a></li>
</ul>


<p class="SUBTITLE">RCE</p>
The Reconfigurable Cluster Element (RCE) is a modular read-out architecture for ATLAS based on buidling blocks: 
the RCE itself and the CIM (Cluster Interconnect Module) in the GEN-1 incarnation in the ATCA packaging standard,
and in the GEN-3 version hardware evolved to the modern ZYNQ SoC since 2013, 
merging the RCE/CIM into a single type of board of COB (Cluster on Board) 
containing modular RCE mezzanine cards which can be more easily upgraded. 
The platform is further extended to stand alone bench top readout setups with the 
HSIO-II + RCE mezzanine that is suitable for medium scaled lab and test beam operations. 

<ul>
  <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/RCEDevelopmentLab">RCE twiki</a></li>
</ul>

<p class="SUBTITLE">Yarr</p>
YARR is a readout system based around the concept of moving intelligence from the FPGA firmware into the host computer software. 
The role of the FPGA is the one of a reconfigurable I/O interface and not anymore the one of a hardware accelerator. 
To move the raw data into the host memory a fast bus is needed, common to all modern computers is the high-speed PCIe bus a natural interface choice. 
The PCIe card, the SPEC board, has been developed by the CERN beams department and is available from multiple vendors.

<ul>
  <li><a href="https://yarr.rtfd.org">YARR documentation</a></li>
  <li><a href="https://gitlab.cern.ch/YARR/YARR">YARR GitLab repository</a></li>
</ul>

<p class="SUBTITLE">USBPix</p>
The USB based FE-I3/4 readout system was developed as an alternative to the common TurboDAQ (TPLL, TPCC) system for a couple of applications. 
Since the system has a modular structure, it will be adapted to serve also as a FE-I4 r/o system
requiring a new module adapter card and changes in FPGA firmware and DAQ application.
Mailing list atlas-pixel-usbpix.

<ul>
  <li><a href="http://icwiki.physik.uni-bonn.de/svn/USBpixI4">USBPix FE-I4 SVN repository</a></li>
  <li><a href="http://icwiki.physik.uni-bonn.de/svn/USBPix">USBPix FE-I3 SVN repository</a></li>
</ul>

<p class="SUBTITLE">BDAQ53</p>
BDAQ is a readout system developed for testing the <a href="https://twiki.cern.ch/twiki/bin/view/RD53/RD53ATesting">RD53A</a> chip.

<ul>
  <li><a href="https://gitlab.cern.ch/silab/bdaq53">BDAQ repository and wiki</a></li>
</ul>

<p class="SUBTITLE">Proteus</p>
Reconstruction software used for test-beam data analysis.

<ul>
  <li><a href="proteus.php">Proteus</a></li>
</ul>

<p class="SUBTITLE">TB Converter</p>
Modular software developed to convert the data format from several read-out systems.

<ul>
  <li><a href="tbconverter.php">TB Converter</a></li>
</ul>

<p class="SUBTITLE">DRS</p>
DRS is a sampling device in the Giga samples per second range. It is specifically designed for HEP applications.
Example source code to read the evaluation board DRS4 is available 
<a href="https://www.psi.ch/drs/evaluation-board">here</a>. 

A python module has been developed to control the DRS from python. 

<ul>
  <li><a href="DRS4_rev09.pdf">DRS4 Datasheet rev. 0.9</a></li>
  <li><a href="manual_rev50.pdf">DRS4 Evaluation Board Rev. 5.0 manual</a></li>
  <li><a href="https://gitlab.cern.ch/solans/drsboard/">Python module for the DRS</a></li>
</ul>

<p class="SUBTITLE">Voltron</p>
A temperature read-out system designed for thermal measurements in the range of -25 C to 20 C is available to use.
Several implementations exist with up to 16 channels.

<ul>
  <li><a href="VolTRON_Manual.pdf">Voltron Manual</a></li>
  <li><a href="Voltron_Calibration_Report.pdf">Voltron calibration report</a>
      (<a href="Voltron_Calibration_Report.zip">Source code</a>)</li>
  <li><a href="https://gitlab.cern.ch/solans/SimpleTemperatureLogger">Software for single temperature logger</a></li>
</ul>

<p class="SUBTITLE">CO2 leak testing</p>
To check if the cooling pipe, which is connected to a stave prototype, has a leak, the pressure read-out system is available.    

<ul>
  <li><a href="LeakTesting.pdf">CO2 leak testing setup</a></li>
  <li><a href="../Software">Software for pressure logger</a></li>
</ul>

<p class="SUBTITLE">Ethernet readout for Lecroy WaveRunner 204mxi-a</p>
Readout library and application for the Lecroy WaveRunner 204mxi-a allows to acquire and event from the screen and store it into ASCII files.

<ul>
  <li><a href="https://gitlab.cern.ch/solans/LeCroyEthScope">Software</a></li>
</ul>

<p class="TITLE">Documentation and links
<ul>
  <li><a href="https://gitlab.cern.ch/groups/PH-ADE-ID_Pixel_RnD">Group gitlab repository</a></li>
  <li><a href="http://www.sciencedirect.com/science/article/pii/S016890021400552X">G. McGoldrick, Matevz Cerv, Andrej Gorisek, NIM A (2014) 765 140-145</a></li>
  <li><a href="https://github.com/Yarr/Yarr">Old Yarr github repository</a></li>
  <li><a href="http://icwiki.physik.uni-bonn.de/twiki/bin/view/Systems/UsbPix">USBPix twiki at Bonn University</a></li>
  <li><a href="https://www.psi.ch/drs/evaluation-board">DRS4 evaluation board</a></li>
</ul>
</p>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
