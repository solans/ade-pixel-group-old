<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<title>Thermal Figure of Merit Setup</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>


<div class="CONTENT">

<?php
show_login();
?>

<p class="TITLE">Thermal Figure of Merit Setup in SR1</p>

<p class="SUBTITLE">Definition</p>
<p>
  We define the Thermal Figure of Merit (TFM) as the temperature gradient per unit power in equilibrium.</p>
<p><img class="SMALLIMAGE" src="<?=$gobase;?>/TFM/tfm-formula.png"></p>

<p>
A stave under test is placed in the climate chamber, loaded with heaters and connected to an evaporative CO<sub>2</sub> cooling system (TRACI). 
The temperature at different points is measured by 
<a href="http://www.digikey.com/product-search/en?keywords=495-7072-ND">100k&Omega; NTCs</a>.
Temperature difference is measured between the point of interest and the temperature of the pipe.
The temperature of the pipe is interpolated between inlet and outlet temperatures.
We assume the temperature on the surface of the pipe is the temperature of the coolant.
We measure the TFM for different values of heat flux, and 
we quote the global TFM as the value of the TFM at 0.7 W/cm<sup>2</sup>, and as the asymptotic value as a function of heat flux.  

</p>
<div class="IMAGECANVAS">
  <img class="SMALLIMAGE" src="<?=$gobase;?>/TFM/Climate_Chamber.png">
  <img class="SMALLIMAGE" src="<?=$gobase;?>/TFM/ir-image.png">
  <img class="SMALLIMAGE" src="<?=$gobase;?>/TFM/cTfmNtcVsW-19.50.png">
</div>

<p class="SUBTITLE">Results</p>

<ul>
  <li><a href="https://edms.cern.ch/project/ATU-0000000269">ITK Pixel Local Support and Integration folder in EDMS</a></li>
</ul>

<?php if($authorised){ ?>

<ul>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_Alpine_20160708.pdf">Alpine cell</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_BarrelCell2_20160707.pdf">SLIM barrel cell MPC315</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_BarrelCell_20160629.pdf">SLIM barrel cell</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/SLIM_BarrelFlat1Cell_20160923.pdf">SLIM barrel cell orientation study</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_BarrelCell_20160907.pdf">SLIM barrel 3 cell stave</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_BarrelCell_20160920.pdf">SLIM barrel 3 cell stave (2nd measurement)</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_BarrelCell_20161110.pdf">SLIM barrel 3 cell stave (3rd measurement)</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_TiltedCell_20160512.pdf">SLIM tilted cell</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_I-Beam-wide_20160322.pdf">I-Beam wide section</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_I-Beam-narrow_20160322.pdf">I-Beam narrow section</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_GTFM_HalfRing_20160906.pdf">EndCap half ring</a></li>
  <li><a href="<?=$gobase;?>TFM/Results/Carlos_HeaterPoweringStrategy_20160629.pdf">Heater powering strategy - 29th June 2016</a></li>
</ul>
  
<?php }else{ ?>

<ul><li>
Please <a href="login/">login</a> to see more results.  
</li></ul>  

<?php } ?>
     
<p class="SUBTITLE">Presentations</p>
<ul>
  <li><a href="https://indico.cern.ch/event/720592/contributions/2970404/attachments/1633714/2605648/ASharma_AUW_20180416.pdf">A. Sharma AUW Pixel mechanics April 2018</a></li>
  <li><a href="https://indico.cern.ch/event/678563/contributions/2779276/attachments/1553871/2442697/2017_10_28_TFM_Update.pdf">A. Sharma ITK Pixel mechanics November 2017</a></li>
  <li><a href="https://indico.cern.ch/event/625353/contributions/2528088/attachments/1434481/2205206/TFM_AUW_20170327.pdf.pdf">A. Sharma AUW March 2017</a></li>
  <li><a href="https://indico.cern.ch/event/562672/contributions/2296214/attachments/1335291/2008249/ITK_Week_Valencia_small_comp2.pdf">A. Sharma ITK Week September 2016</a></li>
  <li><a href="https://indico.cern.ch/event/521364/contributions/2135389/attachments/1259437/1860559/Carlos_AUW_GTFM_20160418.pdf">C. Solans AUW April 2016</a></li>
  <li><a href="https://indico.cern.ch/event/461382/contributions/1132856/attachments/1229775/1802171/Carlos_IBeam_20160217.pdf">C. Solans ITK Week February 2016</a></li>
</ul>

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="<?=$gobase;?>/TFM/Manual_StaveThermalStudies.pdf">Instruction manual of TFM measurements in SR1</a></li>
  <li><a href="<?=$gobase;?>/TFM/Voltron_Calibration.pdf">Voltron Calibration</a></li>
  <li><a href="<?=$gobase;?>/TFM/Carlos_GTFM_Setup.pdf">Description of the TFM setup calibration</a></li>
  <li><a href="https://edms.cern.ch/document/1534572/1">ITk Pixel Local Support Design Parameters</a></li>
  <li><a href="https://atlas-sfb.cern.ch">CO2 HTC calculation (link is internal to CERN)</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
