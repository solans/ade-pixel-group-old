<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>System tests</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">System Tests</p>


<h2 class="SUBTITLE">ATLAS ITK Pixel Outer Barrel pre-production tests</h2>
<div><img src="ob-demonstrator.png" class="MEDIUMIMAGE"/></div>
<div><img src="rd53a-demonstrator.png" class="MEDIUMIMAGE"/></div>


<p class="SUBTITLE">Presentations</p>
<ul>
  <li><a href="https://indico.cern.ch/event/1255624/contributions/5444014/attachments/2728127/4741691/Outer_Barrel_services_chain_characterization_for_the_ATLAS_ITk_Pixel_Detector_TWEPP2023_Leyre_Flores_final.pdf">L. Flores, TWEPP, September 2023</a></li>
  <li><a href="https://indico.cern.ch/event/1223748/contributions/5384225/attachments/2645500/4579151/Flash_talk_AUW_May_2023_Leyre_Flores.pdf">L. Flores, AUW March 2023</a></li>
  <li><a href="https://indico.cern.ch/event/1179940/contributions/5037408/attachments/2508382/4310775/20220914_ITkWeek_OBDemonstrator_ElectricalResults.pdf">H. Joos, ITK week September 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1179940/contributions/5037409/attachments/2508334/4310677/20220914_llsqc.pdf">B. Vormwald, ITK week September 2022</a></li>
  <li><a href="https://agenda.infn.it/event/28874/contributions/170751/">B. Moser, ICHEP, July 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1149581/contributions/4865630/attachments/2441179/4181981/20220510_itkgeneral_systemtest.pdf">B. Vormwald, AUW May 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1149581/contributions/4860746/attachments/2443252/4186105/Preparations_towards_a_multi_module_DAQ_testbench_AUW12052022.pdf">L. Flores, AUW, May 2022</a></li>
  <li><a href="https://indico.cern.ch/event/983068/contributions/4229352/attachments/2190430/3708512/Zambito_Trento2021_ItkOBDemonstrator.pdf">S. Zambito, TREDI, February 2021</a></li>
  <li><a href="https://indico.cern.ch/event/813597/contributions/3727762/attachments/1988575/3314685/quentin_buat_trento_2020.pdf">Q. Buat, TREDI, February 2020</a></li>
  <li><a href="https://indico.cern.ch/event/777112/contributions/3312285/attachments/1801246/2938115/ITKPixelPrototyping.pdf">S. Schaeppe, TREDI, February 2019</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
