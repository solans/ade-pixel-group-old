<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>ITkPix quad modules QC</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>

<p class="TITLE">ITkPix Quad Module QC</p>

<div>
  <img class="MEDIUMIMAGE" src="images/itkpix-quad-module-qc-setup.png"/>
  <br/>
  <b>ITkPix module QC</b>
</div>

<div>
  <img class="MEDIUMIMAGE" src="images/xray-electrical-test-modules.jpg"/>
  <br/>
  <b>X-ray and electrical test setup for modules</b>
</div>

<div>
  <img class="MEDIUMIMAGE" src="images/storage-cabinet-itk-pixel-modules.jpg"/>
  <br/>
  <b>Storage cabinet of ITk Pixel Modules</b>
</div>

<p class="SUBTITLE">Documentation
  <ul>
    <li><a href="https://gitlab.cern.ch/atlas-itk/pixel/module/itkpix-electrical-qc/builds/artifacts/master/file/ITkPix_electrical_QC.pdf?job=build_pdf">ITKPix module QC</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/CERNADETKDomain">Group Twiki</a></li>
  </ul>
</p>


<p class="SUBTITLE">Presentations
  <ul>
    <li><a href="https://indico.cern.ch/event/779148/contributions/3291032/attachments/1783298/2906352/QAQC-IntegrationinSR1-SK-v2.pdf">S. Kuhn, QA/QC workshop, January 2019</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
