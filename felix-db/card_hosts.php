<h2>Hosts</h2>
<input hidden class="getsearch" type="search" data-column="1">
	<button onclick="export_card_hosts();">CSV</button>Export <span id="card_hosts_counter"></span> 
  <button onclick="reset_card_hosts();">Reset filters</button>
	<table id="card_hosts" class="tablesorter" style="font-size: smaller;width:initial">
		<thead>
			<th data-placeholder="Search...">Host sn</th>
			<th data-placeholder="Search...">Host name</th>
			<th data-placeholder="Search...">Date</th>
		  <th >Actions</th>
		</thead>
		<tbody id="card_hosts_body">
		</tbody>
	</table>
  <div id="card_hosts_reply"></div>
<script>

/* Load the table sorter **/
$("#card_hosts").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

/* Trigger the tablesorter */
$(function() {
  $("#card_hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function reset_cards(){
  $("#card_hosts").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export hosts */
function export_cards(){
  $("#card_hosts").trigger('outputTable');
}

/* load the cards */
function load_card_hosts(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_card_hosts",card_id:$("#card_id").val()},
    success: function(data) {
      console.log(data);   
      rows=JSON.parse(data);
      $("#card_hosts_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["host_sn"]+"</td>";
        tt+="<td>"+row["host_name"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=host&host_id="+row["host_id"]+"\">show</a>&nbsp;";
        tt+="<a href=\"?page=host_card&host_card_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_host_card('"+row["id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#card_hosts_body").append(tt);
      }
      $("#card_hosts_counter").html("("+rows.length+")");
      $("#card_hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a host card */
function delete_host_card(id){
  if(!window.confirm("Are you sure to delete host card?")) return false;
  $("#card_hosts_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"delete_host_card",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#card_hosts_reply").text("Member deleted");
        if(typeof load_card_hosts == "function") load_card_hosts();
      }else if (reply["affected_rows"]==0){
         $("#card_hosts_reply").text("Something went wrong");
      }
    }
  });
  return false;
};

$( document ).ready(function() {
  load_card_hosts();
});

</script>

