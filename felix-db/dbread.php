<?php
//echo "hello";
include_once("includes.php");

//echo "open";
$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}  

$d=$_GET;

if($d["cmd"]=="get_hosts"){
  $sql ="SELECT id as host_id, name as host_name, sn as host_sn FROM hosts ORDER BY sn;";
}
else if($d["cmd"]=="get_host"){
  $sql ="SELECT * FROM hosts WHERE id='".$d["host_id"]."';";
}
else if($d["cmd"]=="get_host_locations"){
  $sql ="SELECT * FROM host_locations WHERE host_id='".$d["host_id"]."';";
}
else if($d["cmd"]=="get_host_location"){
  $sql ="SELECT * FROM host_locations WHERE id='".$d["host_location_id"]."';";
}
else if($d["cmd"]=="get_host_cards"){
  $sql ="SELECT * FROM host_cards WHERE host_id='".$d["host_id"]."';";
}
else if($d["cmd"]=="get_host_card"){
  $sql ="SELECT * FROM host_cards WHERE id='".$d["host_card_id"]."';";
}
else if($d["cmd"]=="get_hosts_with_last_location"){
  $sql ="SELECT * FROM hosts LEFT JOIN (";
  $sql.=" SELECT host_locations.host_id, host_locations.date, host_locations.location, host_locations.system FROM host_locations INNER JOIN (";
  $sql.="  SELECT host_id, max(date) AS date FROM host_locations GROUP BY host_id";
  $sql.=" ) AS aux ON aux.host_id=host_locations.host_id AND aux.date=host_locations.date";
  $sql.=") as tt ON tt.host_id=hosts.id;";
}
else if($d["cmd"]=="get_cards"){
  $sql ="SELECT * FROM cards ORDER BY card_id;";
}
else if($d["cmd"]=="get_card"){
  $sql ="SELECT * FROM cards WHERE card_id='".$d["card_id"]."';";
}
else if($d["cmd"]=="get_card_hosts"){
  $sql ="SELECT host_cards.id, host_cards.host_id, host_cards.card_id, host_cards.date, ";
  $sql.="hosts.name AS host_name, hosts.sn AS host_sn FROM host_cards ";
  $sql.="INNER JOIN hosts ON hosts.id=host_cards.host_id WHERE host_cards.card_id='".$d["card_id"]."' ;";
}
else if($d["cmd"]=="get_card_locations"){
  $sql ="SELECT * FROM card_locations WHERE card_id='".$d["card_id"]."';";
}
else if($d["cmd"]=="get_card_location"){
  $sql ="SELECT * FROM card_locations WHERE id='".$d["card_location_id"]."';";
}
else if($d["cmd"]=="get_cards_with_last_location"){
  $sql ="SELECT * ";
  $sql.="FROM cards "; 
  $sql.="LEFT JOIN ( ";
  $sql.="SELECT card_locations.card_id AS cid, card_locations.date, card_locations.location, card_locations.system ";
  $sql.="FROM card_locations ";
  $sql.="INNER JOIN (SELECT card_id, max(date) AS date FROM card_locations GROUP BY card_id) AS aux ON aux.card_id=card_locations.card_id AND aux.date=card_locations.date ";
  $sql.=") as tt ON tt.cid = cards.card_id;";
}
else if($d["cmd"]=="get_systems"){
  $sql ="SELECT host_cards.id, hosts.name, hosts.sn as host_sn, hosts.id as host_id, cards.card_id, host_cards.date FROM host_cards ";
  $sql.="INNER JOIN hosts ON host_cards.host_sn = hosts.sn ";
  $sql.="INNER JOIN cards ON host_cards.card_id = cards.card_id;";
}
  
//echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>