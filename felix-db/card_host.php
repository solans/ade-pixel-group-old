<h2>Card Host</h2>
<form method="POST" id="card_host">
  <table class="border" >
	  <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="card_host_date"/></td></tr>
  </table>
<input type="hidden" id="card_host_id" name="card_host_id" value="<?=$_GET["card_host_id"];?>" >
<input type="submit" value="Save card host">
<input type="reset" value="Reset">
</form>
<div id="hcard_host_reply"></div>

<script>

/* Trigger the date picker */
$(function() {
  $("#card_host_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* load the locations */
function load_card_host(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_host_card", host_card_id:$("#host_card_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data);
      for (row of reply){
        console.log(row);
        $("#host_card_date").val(row["date"]);
        if(typeof load_host == "function"){load_host(row["host_id"]);}
        if(typeof load_card == "function"){load_card(row["card_id"]);}
      }
    }
  });
}

/* add a location */
$("#card_host").submit(function () {
  $("#card_host_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"update_host_card",
           id:$("#host_card_id").val(),
           date:$("#host_card_date").val()},
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $('#card_host_reply').text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $('#card_host_reply').text("Record updated");
       }
    }
  });
  return false;
});

/* onload */
$( document ).ready(function() {
  load_card_host();
});


</script>

