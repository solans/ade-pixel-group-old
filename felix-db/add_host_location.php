<h2>Add Location</h2>
<form id="add_location">
  <table class="border">  
    <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="add_location_date"/></td></tr>
    <tr><th class='regform-done-caption'>Location</th><td class="regform-done-title"><input id="add_location_location" type="text"/></td></tr>
  	<tr><th class='regform-done-caption'>System</th><td class="regform-done-title">
  	   <select id="add_location_system">
  				<?php foreach (array('Pixel','Strips','BCM','HGTD','MDT','ADE','None','Other','Stock','PLR','') as $k => $v) {echo "<option value=\"$v\">$v</option>";}?>
  			</select>
  	</td></tr>
    <tr><th class='regform-done-caption'>Responsible</th><td class="regform-done-title"><input id="add_location_responsible" type="text"/></td></tr>
    <tr><th class='regform-done-caption'>EDH number</th><td class="regform-done-title"><input id="add_location_edh" type="text"/></td></tr>  
  </table>
  <input type="submit" value="Save new location">
  <input type="reset" id="add_location_reset" value="Reset">
</form>
<div id="add_location_reply"></div>

<script>

/* Trigger the date */
$(function() {
  $("#add_location_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* add a location */
$('#add_location').submit(function () {
  $("#add_location_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"add_host_location",
           host_id:$("#host_id").val(),
           date:$("#add_location_date").val(),
           location:$("#add_location_location").val(),
           system:$("#add_location_system option:selected").text(),
           responsible:$("#add_location_responsible").val(),
           edh:$("#add_location_edh").val()},
    success: function(data) {
       console.log(data);
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
			 if (reply["affected_rows"]=="1"){
         $("#add_location_reset").click();
         $("#add_location_reply").text("Location added successfully");
         if(typeof host_locations_load == "function"){host_locations_load();}
       }else{
         $("#add_location_reply").text("Error: "+data);
       }
    }
  });
  return false;
});


</script>


