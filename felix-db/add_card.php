<h2>Add card</h2>
<form method="POST" id="card">
  <table class="border" >
	  <tr><th class='regform-done-caption'>ID</th><td class="regform-done-title"><input id="card_id"/></td></tr>
    <tr><th class='regform-done-caption'>SN</th><td class="regform-done-title"><input id="card_sn" type="text"/></td></tr>
	    </table>
<input type="submit" value="Add card">
<input type="reset" value="Reset">
</form>
<div id="card_reply"></div>

<script>

/* add a location */
$('#card').submit(function () {
  $("#card_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"add_card",
           card_id:$("#card_id").val(),
           card_sn:$("#card_sn").val()
    },
    success: function(data) {
      reply=JSON.parse(data);
      if (reply["affected_rows"]>0){
        $("#card_reply").text("Card added");
        if (typeof load_cards == "function") {load_cards();}
      }else{
        $("#card_reply").text("Error: "+data);
      }
    }
  });
  return false;
});

</script>

