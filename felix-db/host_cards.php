<h2>Host cards</h2>
<button onclick="host_cards_export();">CSV</button>Export <span id="host_cards_counter"></span> 
<button onclick="host_cards_reset();">Reset filters</button>
<table id="host_cards" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">Card id </th>
		<th data-placeholder="Search...">Date</th>
	  <th >Actions</th>
	</thead>
	<tbody id="host_cards_body">
	</tbody>
</table>
<div id="host_cards_reply"></div>
<script>

/* Load the table sorter **/
$("#host_cards").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#host_cards_counter").html("("+($('#host_cards tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#host_cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function host_cards_reset(){
  $("#host_cards").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function host_cards_export(){
  $("#host_cards").trigger('outputTable');
}

/* load the cards */
function host_cards_load(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_host_cards",host_id:$("#host_id").val()},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      $("#host_cards_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["card_id"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=card&card_id="+row["card_id"]+"\">show</a>&nbsp;";
        tt+="<a href=\"?page=host_card&host_card_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"host_cards_delete('"+row["id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#host_cards_body").append(tt);
      }
      $("#host_cards_counter").html("("+rows.length+")");
      $("#host_cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function host_cards_delete(id){
  if(!window.confirm("Are you sure to delete host card?")) return false;
  $("#host_cards_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"delete_host_card",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#host_cards_reply").text("Member deleted");
        if(typeof host_cards_load == "function") host_cards_load();
      }else if (reply["affected_rows"]==0){
         $("#host_cards_reply").text("Something went wrong");
      }
    }
  });
  return false;
};

$( document ).ready(function() {
  host_cards_load();
});

</script>

