<h2>Add card</h2>
<form id="add_host_card">
  <table class="border">  
    <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="add_host_card_date"/></td></tr>
    <tr><th class='regform-done-caption'>Card</th><td class="regform-done-title"><select id="add_host_card_card_id"></select></td></tr>
  </table>
  <input type="submit" value="Add card">
  <input type="reset" id="add_host_card_reset" value="Reset">
</form>
<div id="add_host_card_reply"></div>

<script>

/* Trigger the date */
$(function() {
  $("#add_host_card_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* add a location */
$("#add_host_card").submit(function () {
  $("#add_host_card_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"add_host_card",
           host_id:$("#host_id").val(),
           date:$("#add_host_card_date").val(),
           card_id:$("#add_host_card_card_id").val()},
    success: function(data) {
       console.log(data);
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
			 if (reply["affected_rows"]=="1"){
         $("#add_host_card_reset").click();
         $("#add_host_card_reply").text("Card added successfully");
         if(typeof load_cards == "function") load_cards();
         if(typeof host_cards_load == "function") host_cards_load();
       }else{
         $("#add_host_card_reply").text("Error: "+data);
       }
    }
  });
  return false;
});

function load_add_host_card_cards(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_cards"},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      tt="";
      for (row of rows){
        tt+="<option value=\""+row["card_id"]+"\">"+row["card_id"]+"</option>\n";
      }
      $("#add_host_card_card_id").html(tt);
    }
  });
}

/** onload **/
$( document ).ready(function() {
  load_add_host_card_cards();
});

</script>


