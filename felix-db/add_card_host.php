<h2>Add host</h2>
<form id="add_card_host">
  <table class="border">  
    <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="add_card_host_date"/></td></tr>
    <tr><th class='regform-done-caption'>Host</th><td class="regform-done-title"><select id="add_card_host_host_id"></select></td></tr>
  </table>
  <input type="submit" value="Add card">
  <input type="reset" id="add_card_host_reset" value="Reset">
</form>
<div id="add_card_host_reply"></div>

<script>

/* Trigger the date */
$(function() {
  $("#add_card_host_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* add a location */
$("#add_card_host").submit(function () {
  $("#add_card_host_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"add_host_card",
           card_id:$("#card_id").val(),
           date:$("#add_card_host_date").val(),
           host_id:$("#add_card_host_host_id").val()},
    success: function(data) {
       console.log(data);
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
			 if (reply["affected_rows"]=="1"){
         $("#add_card_host_reset").click();
         $("#add_card_host_reply").text("Added");
         if(typeof load_card_hosts == "function") load_card_hosts();
       }else{
         $("#add_card_host_reply").text("Error: "+data);
       }
    }
  });
  return false;
});

function load_add_card_host_hosts(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_hosts"},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      tt="";
      for (row of rows){
        tt+="<option value=\""+row["host_id"]+"\">"+row["host_sn"]+" ("+row["host_name"]+")</option>\n";
      }
      $("#add_card_host_host_id").html(tt);
    }
  });
}

/** onload **/
$( document ).ready(function() {
  load_add_card_host_hosts();
});

</script>


