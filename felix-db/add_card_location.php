<h2>Add location</h2>
<form method="POST" id="card_location">
  <table class="border" >
	  <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="card_location_date"/></td></tr>
    <tr><th class='regform-done-caption'>Location</th><td class="regform-done-title"><input id="card_location_location" type="text"/></td></tr>
	  <tr><th class='regform-done-caption'>System</th><td class="regform-done-title">
	   <select id="card_location_system">
				<?php foreach (array('Pixel','Strips','BCM','HGTD','MDT','ADE','None','Other','Stock','PLR','') as $k => $v) {echo "<option value=\"$v\">$v</option>";}?>
			</select>
	  </td></tr>
	  <tr><th class='regform-done-caption'>Responsible</th><td class="regform-done-title"><input id="card_location_responsible" type="text"/></td></tr>
    <tr><th class='regform-done-caption'>EDH number</th><td class="regform-done-title"><input id="card_location_edh" type="number"/></td></tr>
  </table>
<input type="hidden" id="card_location_id" name="card_location_id" value="<?=@$_GET["card_location_id"];?>" >
<input type="submit" value="Save location">
<input type="reset" value="Reset">
</form>
<div id="card_location_reply"></div>

<script>

/* Trigger the date picker */
$(function() {
  $("#card_location_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* add a location */
$('#card_location').submit(function () {
  $("#card_location_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"add_card_location",
           id:$("#card_location_id").val(),
           card_id:$("#card_id").val(),
           date:$("#card_location_date").val(),
           location:$("#card_location_location").val(),
           system:$("#card_location_system option:selected").text(),
           responsible:$("#card_location_responsible").val(),
           edh:$("#card_location_edh").val()},
    success: function(data) {
       console.log(data);        
			 reply=JSON.parse(data);
       if (reply["affected_rows"]>0){
         $("#card_location_reply").text("Location added successfully");
         if(typeof load_card_locations == "function"){load_card_locations();}
       }else{
         $("#card_location_reply").text("Error: "+data);
       }
    }
  });
  return false;
});

</script>


