<h2>Add host</h2>
<form id="add_host">
  <table class="border">
    <tr><th class='regform-done-caption'>ID</th><td class="regform-done-title"><input id="host_id" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>SN</th><td class="regform-done-title"><input id="host_sn" type="text"></td></tr>
    <tr><th class='regform-done-caption'>Name</th><td class="regform-done-title"><input id="host_name" type="text"></td></tr>
    <tr><th class='regform-done-caption'>Model</th><td class="regform-done-title"><input id="host_model" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>Motherboard</th><td class="regform-done-title"><input id="host_mb" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>CPU</th><td class="regform-done-title"><input id="host_cpu" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>RAM</th><td class="regform-done-title"><input id="host_ram" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>HDD</th><td class="regform-done-title"><input id="host_disk" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>Chassis</th><td class="regform-done-title"><input id="host_chassis" type="text" ></td></tr>
    <tr><th class='regform-done-caption'>MAC0</th><td class="regform-done-title"><input id="host_mac0" type="text"></td></tr>
    <tr><th class='regform-done-caption'>MAC1</th><td class="regform-done-title"><input id="host_mac1" type="text"></td></tr>
    <tr><th class='regform-done-caption'>EDH</th><td class="regform-done-title"><input id="host_edh" type="text"></td></tr>
  </table>
  <input type="submit" value="Save">
  <input type="reset" id="add_host_reset" value="Reset">
</form>
<div id="add_host_reply"></div>

<script>


/* update the host details */
$('#add_host').submit(function () {
  $("#add_host_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    data: {cmd:"add_host",
           host_id:$("#host_id").val(),
           name:$("#host_name").val(),
           edh:$("#host_edh").val(),
           mac0:$("#host_mac0").val(),
           mac1:$("#host_mac1").val(),
    model:$("#host_model").val(),
    sn:$("#host_sn").val(),
    mb:$("#host_mb").val(),
    cpu:$("#host_cpu").val(),
    ram:$("#host_ram").val(),
    disk:$("#host_disk").val(),
    chassis:$("#host_chassis").val(),
    },
    type: 'get',
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       console.log(reply["affected_rows"]);
       if (reply["affected_rows"]==0){
         $('#add_host_reply').text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $('#add_host_reply').text("Host added");
         if( typeof load_host == "function"){load_hosts();}
       }
    }
  });
  return false;
});


</script>

