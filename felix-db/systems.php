<h2>Systems</h2>
<input  hidden class="getsearch" type="search" data-column="1">
<button onclick="export_systems();">CSV</button> Export <span id="systems_counter"></span> 
<button onclick="reset_systems();">Reset filters</button>
<table id="systems" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">ID</th>
		<th data-placeholder="Search...">Host name</th>
		<th data-placeholder="Search...">Host SN</th>
		<th data-placeholder="Search...">Card</th>
		<th data-placeholder="Search...">Date</th>
	</thead>
	<tbody id="systems_body">
  </tbody>
</table>


<script>

/* Load the table sorter **/
$("#systems").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

/* Trigger the tablesorter */
$(function() {
  $("#systems").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function reset_systems(){
  $("#systems").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function export_systems(){
  $("#systems").trigger("outputTable");
}

/* load the systems */
function load_systems(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_systems"},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      $("#systems_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["id"]+"</td>";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["host_sn"]+"<a href=\"?page=host&uid="+row["host_id"]+"\"> <img class=\"cursor newtabicon\" src=\"../img/newtab.png\"></a></td>";
        tt+="<td>"+row["card_id"]+"<a href=\"?page=card&card_id="+row["card_id"]+"\"> <img class=\"cursor newtabicon\" src=\"../img/newtab.png\"></a></td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="</tr>\n";
        $("#systems_body").append(tt);
      }
      $("#systems_counter").html("("+rows.length+")");
      $("#systems_body").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/** onload **/
$( document ).ready(function() {
  load_systems();
});

</script>
