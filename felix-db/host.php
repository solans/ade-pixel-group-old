<h2>Host details</h2>
<form id="host_details">
  <table class="border">
    <tr><th class='regform-done-caption'>ID</th><td class="regform-done-title"><input id="host_id" type="text" disabled value="<?=@$_GET["host_id"];?>"></td></tr>
    <tr><th class='regform-done-caption'>SN</th><td class="regform-done-title"><input id="host_sn" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Name</th><td class="regform-done-title"><input id="host_name" type="text"></td></tr>
    <tr><th class='regform-done-caption'>Model</th><td class="regform-done-title"><input id="host_model" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Motherboard</th><td class="regform-done-title"><input id="host_mb" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>CPU</th><td class="regform-done-title"><input id="host_cpu" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>RAM</th><td class="regform-done-title"><input id="host_ram" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>HDD</th><td class="regform-done-title"><input id="host_disk" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Chassis</th><td class="regform-done-title"><input id="host_chassis" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>MAC0</th><td class="regform-done-title"><input id="host_mac0" type="text"></td></tr>
    <tr><th class='regform-done-caption'>MAC1</th><td class="regform-done-title"><input id="host_mac1" type="text"></td></tr>
    <tr><th class='regform-done-caption'>EDH</th><td class="regform-done-title"><input id="host_edh" type="text"></td></tr>
  </table>
  <input type="submit" value="Save">
  <input type="reset" id="host_details_reset" value="Reset">
</form>
<div id="host_details_reply"></div>

<script>

/* load the hosts */
function load_host(host_id){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_host", host_id:host_id},
    success: function(data) {
      console.log(data);   
      rows=JSON.parse(data);
      row=rows[0];
      $("#host_id").val(row["id"]);
      $("#host_sn").val(row["sn"]);
      $("#host_name").val(row["name"]);
      $("#host_model").val(row["model"]);
      $("#host_mb").val(row["mb"]);
      $("#host_cpu").val(row["cpu"]);
      $("#host_ram").val(row["ram"]);
      $("#host_disk").val(row["disk"]);
      $("#host_chassis").val(row["chassis"]);
      $("#host_mac0").val(row["mac0"]);
      $("#host_mac1").val(row["mac1"]);
      $("#host_edh").val(row["edh"]);      
    }
  });
}

/* update the host details */
$('#host_details').submit(function () {
  $("#host_details_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    data: {cmd:"add_host_details",
           host_id:$("#host_id").val(),
           name:$("#host_name").val(),
           edh:$("#host_edh").val(),
           mac0:$("#host_mac0").val(),
           mac1:$("#host_mac1").val()
    },
    type: 'get',
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $('#host_details_reply').text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $('#host_details_reply').text("Record updated");
       }
    }
  });
  return false;
});

<?php
if(isset($_GET["host_id"])){
  ?>
  /* onload */
  $( document ).ready(function() {
    load_host("<?=$_GET["host_id"];?>");
  });
  <?
}?>

</script>

