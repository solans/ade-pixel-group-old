<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<title>Felix hosts database</title>
<style>
.ui-datepicker {
  background: #fff;
}

pre {
    font-size: 13px;
    font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,sans-serif;
    line-height: 1.30769231;
    color: var(--highlight-color);
    background-color: #DDDDDD;
    border-radius: 5px;
    margin: 0;
        margin-top: 0px;
        margin-bottom: 0px;
    padding: 12px;
    overflow: auto;
    scrollbar-color: var(--scrollbar) transparent;
}
table.border tr{
    border-collapse: collapse;
}


.regform-done-caption {
    border-bottom: 1px dashed #e1e1e1;
    border-right: 1px dashed #e1e1e1;
    text-align: right;
    padding: 0 8px 0 0;
    width: 120px;
}
.regform-done-title{
    border-bottom: 1px solid #e1e1e1;
}
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
  //$dt1 = round(microtime(true) - $time_start,1);
?>
<div class="CONTENT">
<?php   
  $dbOK=True;
  show_certificate();
  show_login(true); 
  //$dt2 = round(microtime(true) - $time_start,1);
  $uid=0;
  try {
      if(isset($_GET['uid'])){
          $uid=intval($_GET['uid']);
          // Create connection
          $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
          // Check connection
          if ($conn->connect_error) {
              throw new Exception('Database error');
              die("Connection failed: " . $conn->connect_error);
              echo "Error connection to database";
          }else{
            $hosts = $conn->query("SELECT * from hosts");
            $cards = $conn->query("SELECT * from cards");
            $conn->close();
          }
      }else{
          echo "No object";
      }
  }catch (Exception $e) {
      $dbOK=False;
      echo "<h2>Error connecting to database</h2>";//.$e->getMessage();
  }
  ?>
<p class="TITLE">System</p>
<p><a href="systems.php">back</a></p>
<?php
if ($dbOK==True){?>
  
<form method="POST" id="add_system">
  <table class="border">
  	<tr><th class='regform-done-caption'>Host</th><td class="regform-done-title">
  	   <select id="host_id">
  				<?php while ($v=$hosts->fetch_assoc()) {?><option value="<?=$v["id"];?>"><?=$v["name"]."-".$v["sn"];?></option><? } ?>
  		 </select>
  	</td></tr>
  	<tr><th class='regform-done-caption'>Card</th><td class="regform-done-title">
  	   <select id="card_id">
  				<?php while ($v=$cards->fetch_assoc()) {?><option value="<?=$v["card_id"];?>"><?=$v["card_id"]."-".$v["sn"];?></option><? } ?>
  		 </select>
  	</td></tr>
    <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="date"/></td></tr>
  </table>
<input type="submit" value="Save new location">
<input type="reset" id="reset" value="Reset">
</form>



<?php 

}else{
    echo "Error connecting to database";
}

?>
<div id="db_response"></div>

</div>
<?php
  show_footer();
  // Display Script End time
?>
</div>
<script>
	process_table("search", 0,0, false);
  $( function() {
    $( "#date" ).datepicker({dateFormat:'yy-mm-dd'});
  } );

$('#add_location').submit(function () {
 if (confirm("Are you sure you want to add new entry? This entry can not be deleted")){  
  $("#db_response").text("");
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"insert_host_card",
           host_id:$("#host_id option:selected").text(),
           card_id:$("#card_id option:selected").text(),
           date:$("#date").val()}
    type: 'post',
    success: function(data) {
       console.log(data);        
			 if (data.includes("OK")){console.log("reset form");$('#reset').click();location.reload();$('#reset').click();}
			 else{$("#db_response").text("Error. Check duplicated entry or connection error.");}
    }
  });
 }else{
 console.log("Negative response");
}
  return false;
});

</script>
</body>
</html>

