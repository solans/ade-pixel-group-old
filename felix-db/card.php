<h2>Card details</h2>
<table class="border" >
  <tr><th class='regform-done-caption'>Card ID</th><td class="regform-done-title"><input id="card_id" type="text" disabled value="<?=@$_GET["card_id"];?>"/></td></tr>
  <tr><th class='regform-done-caption'>Card SN</th><td class="regform-done-title"><input id="card_sn" type="text" disabled/></td></tr>
</table>

<script>

/* load the host */
function load_card(card_id){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_card", card_id:card_id},
    success: function(data) {
      console.log(data);   
      cards=JSON.parse(data);
      console.log(cards);
      $("#card_id").val(cards[0]["card_id"]);
      $("#card_sn").val(cards[0]["card_sn"]);
    }
  });
}

<?php
if(isset($_GET["card_id"])){
  ?>
  /* onload */
  $( document ).ready(function() {
    load_card("<?=$_GET["card_id"];?>");
  });
  <?
}?>
  
</script>

