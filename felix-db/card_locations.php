<h2>Locations</h2>
<button onclick="export_card_locations();">CSV</button>Export <span id="card_locations_counter"></span> 
<button onclick="reset_card_locations();">Reset filters</button>	
<table id="card_locations" class="tablesorter" style="font-size: smaller;width:initial">
  <thead>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Location</th>
		<th data-placeholder="Search...">System</th>
		<th data-placeholder="Search...">Responsible</th>
		<th data-placeholder="Search...">EDH</th>
		<th >Actions</th>
	</thead>
	<tbody id="card_locations_body">
	</tbody>
</table>

<script>

/* Load the table sorter **/
$("#card_locations").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

/* Trigger the tablesorter */
$(function() {
  $("#card_locations_body").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function reset_locations(){
  $("#card_locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export locations */
function export_locations(){
  $("#card_locations").trigger('outputTable');
}

/* load the locations */
function load_card_locations(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_card_locations", card_id:$("#card_id").val()},
    success: function(data) {
      console.log(data);   
      locations=JSON.parse(data);
      $("#card_locations_body").empty();
      for (row of locations){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["system"]+"</td>";
        tt+="<td>"+row["responsible"]+"</td>";
        tt+="<td><a href='https://edh.cern.ch/Document/SupplyChain/SHIP/"+row["edh"]+"' target=\"_blank\">"+row["edh"]+"</td>";
        tt+="<td><a href='?page=card_location&card_location_id="+row["id"]+"'\">edit</a>";
        tt+="&nbsp;<a href=\"#\" title=\"Delete card location\" onclick=\"delete_card_location("+row["id"]+");\">delete</a></td>";
        tt+="</tr>\n";
        $("#card_locations_body").append(tt);
      }
      $('#card_locations').trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function delete_card_location(id){
  if(!window.confirm("Are you sure to delete card location?")) return false;
  $("#add_card_location_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_card_location",
      id:id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#add_card_location_reply").text("Member deleted");
        load_card_locations();
      }else if (reply["affected_rows"]==0){
         $("#add_card_location_reply").text("Something went wrong");
      }
    }
  });
  return false;
};

/* onload */
$( document ).ready(function() {
  load_card_locations();
});

</script>

