<?php
include_once("../functions.php");
include_once("includes.php");
head(isset($_GET["page"])?$_GET["page"]:"Home");
?>
<body>

<?php top();?>

<?php
if(@$_GET['page']=="hosts"){
  //add_onload("load_member('".$_GET['member_id']."');");
  include("hosts.php");
  include("add_host.php");
}
if(@$_GET['page']=="host"){
  include("host.php");
  include("host_locations.php");
  include("add_host_location.php");
  include("host_cards.php");
  include("add_host_card.php");
}
if(@$_GET['page']=="host_location"){
  include("host.php");
  include("host_location.php");
}
else if(@$_GET['page']=="host_card"){
  include("host.php");
  include("card.php");
  include("host_card.php");
}
else if(@$_GET['page']=="cards"){
  include("cards.php");
  include("add_card.php");
}
else if(@$_GET['page']=="card"){
  include("card.php");
  include("card_locations.php");
  include("add_card_location.php");
  include("card_hosts.php");
  include("add_card_host.php");
}
else if(@$_GET['page']=="card_location"){
  include("card.php");
  include("card_location.php");
}
else if(@$_GET['page']=="card_host"){
  include("card.php");
  include("host.php");
  include("host_card.php");
}
else if(@$_GET['page']=="systems"){
  include("systems.php");
  include("add_system.php");
}
?>

<?php
onload();
tail();
?>
</body>
</html>
