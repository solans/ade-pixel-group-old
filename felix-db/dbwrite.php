<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_host"){
  $sql ="INSERT INTO hosts (name, sn, model, mb, cpu, ram, disk, chassis, mac0, mac1, edh) VALUES ( ";
  $sql.="'".$d["name"]."',"; 
  $sql.="'".$d["sn"]."', ";
  $sql.="'".$d["model"]."', ";
  $sql.="'".$d["mb"]."', ";
  $sql.="'".$d["cpu"]."', ";
  $sql.="'".$d["ram"]."', ";
  $sql.="'".$d["disk"]."', ";
  $sql.="'".$d["chassis"]."', ";
  $sql.="'".$d["mac0"]."', ";
  $sql.="'".$d["mac1"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}else if($d["cmd"]=="add_host_details"){
  $sql ="UPDATE hosts SET ";
  $sql.="name='".$d["name"]."', ";
  $sql.="mac0='".$d["mac0"]."', ";
  $sql.="mac1='".$d["mac1"]."', ";
  $sql.="edh='".$d["edh"]."' ";
  $sql.="WHERE id='".$d["host_id"]."';";
}else if($d["cmd"]=="add_host_location"){
  $sql ="INSERT INTO host_locations (host_id, date, location, system, responsible, edh) VALUES ( ";
  $sql.="'".$d["host_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["system"]."', ";
  $sql.="'".$d["responsible"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_host_location"){
  $sql ="UPDATE host_locations SET ";
  $sql.="host_id='".$d["host_id"]."',"; 
  $sql.="date='".$d["date"]."', ";
  $sql.="location='".$d["location"]."', ";
  $sql.="system='".$d["system"]."', ";
  $sql.="responsible='".$d["responsible"]."', ";
  $sql.="edh='".$d["edh"]."' ";
  $sql.="WHERE id='".$d["id"]."';";  
}else if($d["cmd"]=="delete_host_location"){
  $sql = "DELETE FROM host_locations WHERE id='".$d["id"]."'";
}else if($d["cmd"]=="add_card"){
  $sql ="INSERT INTO cards (card_id, card_sn) VALUES ( ";
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["card_sn"]."' ";
  $sql.=");";
}else if($d["cmd"]=="add_card_location"){
  $sql ="INSERT INTO card_locations (card_id, date, location, system, responsible, edh) VALUES ( ";
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["system"]."', ";
  $sql.="'".$d["responsible"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_card_location"){
  $sql ="UPDATE card_locations SET ";
  $sql.="card_id='".$d["card_id"]."',"; 
  $sql.="date='".$d["date"]."', ";
  $sql.="location='".$d["location"]."', ";
  $sql.="system='".$d["system"]."', ";
  $sql.="responsible='".$d["responsible"]."', ";
  $sql.="edh='".$d["edh"]."' ";
  $sql.="WHERE id='".$d["id"]."';";  
}else if($d["cmd"]=="delete_card_location"){
  $sql = "DELETE FROM card_locations WHERE id='".$d["id"]."'";
}else if($d["cmd"]=="add_host_card"){
  $sql ="INSERT INTO host_cards (host_id, card_id, date) VALUES ( ";
  $sql.="'".$d["host_id"]."',"; 
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["date"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_host_card"){
  $sql ="UPDATE host_cards SET ";
  $sql.="date='".$d["date"]."' ";
  $sql.="WHERE id='".$d["id"]."';";    
}else if($d["cmd"]=="delete_host_card"){
  $sql = "DELETE FROM host_cards WHERE id='".$d["id"]."'";
}

//echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
  $ret["sql"]=$sql;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>