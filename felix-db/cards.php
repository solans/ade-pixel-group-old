<h2>Cards</h2>
<button onclick="cards_export();">CSV</button>Export <span id="cards_counter"></span> 
<button onclick="cards_reset();">Reset filters</button>
<table id="cards" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">Card id </th>
		<th data-placeholder="Search...">Serial</th>
    <th data-placeholder="Search...">System</th>
	  <th class="first-name filter-select" data-placeholder="Select...">Last location</th>
	  <th >Actions</th>
	</thead>
	<tbody id="cards_body">
	</tbody>
</table>

<script>

/* Load the table sorter **/
$("#cards").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#cards_counter").html("("+($('#cards tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function cards_reset(){
  $("#cards").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export hosts */
function cards_export(){
  $("#cards").trigger('outputTable');
}

/* load the cards */
function cards_load(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_cards_with_last_location"},
    success: function(data) {
      rows=JSON.parse(data);
      $("#cards_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["card_id"]+"</td>";
        tt+="<td>"+row["card_sn"]+"</td>";
        tt+="<td>"+row["system"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+"<a href=\"?page=card&card_id="+row["card_id"]+"\">edit</a></td>"
        tt+="</tr>\n";
        $("#cards_body").append(tt);
      }
      $("#cards_counter").html("("+rows.length+")");
      $("#cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}


$( document ).ready(function() {
  cards_load();
});

</script>

