<h2>Card Host</h2>
<form method="POST" id="host_card">
  <table class="border" >
	  <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="host_card_date"/></td></tr>
  </table>
<input type="hidden" id="host_card_id" name="host_card_id" value="<?=$_GET["host_card_id"];?>" >
<input type="submit" value="Save card host">
<input type="reset" value="Reset">
</form>
<div id="host_card_reply"></div>

<script>

/* Trigger the date picker */
$(function() {
  $("#host_card_date").datepicker({dateFormat:'yy-mm-dd'});
});

/* load the locations */
function load_host_card(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_host_card", host_card_id:$("#host_card_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data);
      for (row of reply){
        console.log(row);
        $("#host_card_date").val(row["date"]);
        if(typeof load_host == "function"){load_host(row["host_id"]);}
        if(typeof load_card == "function"){load_card(row["card_id"]);}
      }
    }
  });
}

/* add a location */
$("#host_card").submit(function () {
  $("#host_card_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"update_host_card",
           id:$("#host_card_id").val(),
           date:$("#host_card_date").val()},
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $('#host_card_reply').text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $('#host_card_reply').text("Record updated");
       }
    }
  });
  return false;
});

/* onload */
$( document ).ready(function() {
  load_host_card();
});


</script>

