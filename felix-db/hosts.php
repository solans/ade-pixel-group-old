<h2>Hosts</h2>
<button onclick="hosts_export();">CSV</button>Export <span id="hosts_counter"></span> 
<button onclick="hosts_reset();">Reset filters</button>
<table id="hosts" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<!--<th data-placeholder="Search...">ID</th>-->
		<th data-placeholder="Search...">Name</th>
		<th data-placeholder="Search...">Model</th>
		<th data-placeholder="Search...">Serial</th>
		<th class="first-name filter-select" data-placeholder="Search...">System</th>
		<th class="first-name filter-select" data-placeholder="Search...">EDH</th>
		<th class="first-name filter-select" data-placeholder="Select...">Last location</th>
    <th>Action</th>
		</thead>
	<tbody id="hosts_body">
	</tbody>
</table>



<script>

/* Load the table sorter **/
$("#hosts").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter', 'zebra','output']
}).bind('filterEnd', function() {
  $("#hosts_counter").html("("+($('#hosts tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function hosts_reset(){
  $("#hosts").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export hosts */
function hosts_export(){
  $("#hosts").trigger('outputTable');
}

/* load the hosts */
function hosts_load(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_hosts_with_last_location"},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      $("#hosts_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        //tt+="<td>"+row["id"]+"</td>";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["model"]+"</td>";
        tt+="<td>"+row["sn"]+"</td>";
        tt+="<td>"+row["system"]+"</td>";
        tt+="<td><a href=\"#\" onclick=\"hosts_edh('"+row["edh"]+"');\">"+row["edh"]+"</a></td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td><a href=\"?page=host&host_id="+row["id"]+"\">edit</a></td>";
        tt+="</tr>\n";
        $("#hosts_body").append(tt);
      }
      $("#hosts_counter").html("("+rows.length+")");
      $("#hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

function hosts_edh(doc){
  url="https://edh.cern.ch/Document/";
  if(isNaN(doc)){url="https://edh.cern.ch/Info/Order/";}
  window.open(url+doc);
}

$( document ).ready(function() {
  hosts_load();
});

</script>


