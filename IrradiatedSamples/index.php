<?php
  include_once('../functions.php');
  include_once('../scripts/tablesorter.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<?$table_sorter->show_includes();?>
<script src="JS/dataformat.js"></script>
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">


<title>Irradiated samples</title>
<style>
.ui-datepicker {
  background: #fff;
}
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">
<?php   
  show_certificate(); 
  show_login();
?>

<p class="TITLE">Irradiated samples</p>

	<table>
	<?
		
		
		include_once("parser.php");
        include_once("helper.php");
        include_once("material.php");
	
		$filename = getcwd()."/data/data.xml";
		$parser = new Parser();
		$helper = new Helper();
        $helper->AddType("Material");
		$helper->verbose = (@$_GET["debug"]=="true"?true:false);
		
		$parser->SetFilename($filename);
		$parser->SetDataHolder($helper);
		$parser->Parse();
		
		$collection = $helper->GetData();
		
		include_once("querystring.php");
		$qs = new querystring();
	   ?>
		<tr>
			<td> 
			<? 
			if(@$_GET["action"]=="admin" && $authorised){
				$qs->remove("action");
				$qs->remove("do");
				$qs->remove("id");
				?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">View</a><? 
				$qs->set("action",$_GET["action"]);
				if(isset($_GET["do"])) $qs->set("do",$_GET["do"]);
				if(isset($_GET["id"])) $qs->set("id",$_GET["id"]);
			}else if($authorised){
                $qs->remove("do");
                $qs->remove("id");
			    $qs->set("action","admin");
            ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">Admin</a><? 
            }
			?>
			</td>
		</tr>
	</table>
	<table border="0" id="main" class="tablesorter">
		<?
	  
		function randomstring(){
			$arr = str_split('abcdefghijklmnopqrstuwxyz'); // get all the characters into an array
    	shuffle($arr); // randomize the array
    	$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    	return implode('', $arr); // smash them back into a string
		}
		
		if(@$_GET["action"]=="admin" && $authorised){	
			
			if(@$_POST["action"]=="add"){
				//Pre-process POST
				//continue
				$material = new Material();
        $material->FromArray($_POST);
        //Append to file
				$collection->Add($material);
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->set('action','admin');
				$qs->remove('do');
				$qs->remove('id');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
			}else if(@$_POST["action"]=="edit"){
				$material = $collection->Get($_GET["id"]);
				$material->FromArray($_POST);
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->set('action','admin');
				$qs->remove('do');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
			}else if(@$_GET["do"]=="remove" && $authorised){
				$collection->Del($_GET["id"]);
				$collection->Dump();
				$fh = fopen($filename, 'w');
				fwrite($fh, $collection->ToXml());
				fclose($fh);	
				$qs = new querystring(); 
				$qs->remove('do');
				$qs->remove('id');
				redirect($_SERVER['PHP_SELF'].$qs->toFullString());
		  }else{
				?>
			<tr><td style="text-align:left">
				<table style="text-align:left; width:100%">
       	<?
				foreach($collection->data as $p){
					?>
          <tr>
					<td>
					 	<? $p->Dump(); ?>
            <a href="<? $qs->set('do','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
						<a href="<? $qs->set('do','remove'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">delete</a> 
          </td>
					</tr>
					<?
				} 
				?>  
				</table>
 			</div>
			<a href="<? $qs->remove('do'); $qs->remove('id'); echo $_SERVER['PHP_SELF'].'?'.$qs->toString();?>" >Add</a>
			<form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
					<input type="hidden" name="action" value="add"/>
					<label style="display:inline-block;width:100px;">Reference(*):</label><input type="text" name="reference" size="80"/><br/>
					<label style="display:inline-block;width:100px;">Name:</label><input type="text" name="name" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Responsible:</label><input type="text" name="responsible" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Location:</label><input type="text" name="location" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Start date:</label><input type="text" id="startdate" name="startdate" size="80"/><br/>
          <label style="display:inline-block;width:100px;">End date:</label><input type="text" id="enddate" name="enddate" size="80"/><br/>
          <input name="submit" type="submit" value="Add" /><br>
          * Required fields
			</form>
			<script>
            /*$(function() {
              $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd'}); 
              $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd'}); 
            });*/
            triggerdate();
			  </script>
        <?
				if(@$_GET["do"]=="edit" && $authorised){
					$cc = $collection->Get($_GET["id"]);
					?>
					<script>
				 		<? 
            foreach($cc->GetData() as $k=>$v){
             ?>document.forms["editor"].elements["<?=$k;?>"].value='<?=$v;?>';<?
            }
            ?>
            document.forms["editor"].elements["action"].value='edit';
				 		document.forms["editor"].elements["submit"].value='Update';
					</script>
					<?
				}
			}
			?>
			</td>
		</tr>
		<?    
    }else if($authorised){  
			?>
		  <thead class="tablesorter">
      <tr>
      <?
      $item = new material();
      foreach($item->GetData() as $k=>$v){
          ?>
          <th><?=$k;?></th>
          <?
        } 
      ?>
      </tr>
      </thead>
      <tbody class="tablesorter">
      <?
        foreach($collection->data as $p){
          ?>
          <tr>
            <?
            foreach($p->GetData() as $k=>$v){
              ?><td><?=$v;?></td><? 
            }
            ?>
           </tr>
          <?
        }  
			}else{
			  ?>
     <tr>
      <td>
        <p class="h2">Please login to see the list</p>
        <p><a href="login/">Login</a></p>
      </td>
      </tr>
     <? 
      }
		?></tbody>
	</table>
</div>

<?
show_footer();
?>
<!--
<script>
//trigger tablesorter
$(document).ready(function(){

    $("#main").tablesorter({
        theme: 'blue',
    
        // hidden filter input/selects will resize the columns, so try to minimize the change
        widthFixed : true,
    
        // initialize zebra striping and filter widgets
        widgets: ["zebra", "filter"],//, "filter"
    
        ignoreCase: false,
    
        widgetOptions : {
    
          // filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option
    
          // If there are child rows in the table (rows with class name from "cssChildRow" option)
          // and this option is true and a match is found anywhere in the child row, then it will make that row
          // visible; default is false
          filter_childRows : false,
    
          // if true, filter child row content by column; filter_childRows must also be true
          filter_childByColumn : false,
    
          // if true, include matching child row siblings
          filter_childWithSibs : true,
    
          // if true, a filter will be added to the top of each table column;
          // disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
          // if you set this to false, make sure you perform a search using the second method below
          filter_columnFilters : true,
    
          // if true, allows using "#:{query}" in AnyMatch searches (column:query; added v2.20.0)
          filter_columnAnyMatch: true,
    
          // extra css class name (string or array) added to the filter element (input or select)
          filter_cellFilter : '',
    
          // extra css class name(s) applied to the table row containing the filters & the inputs within that row
          // this option can either be a string (class applied to all filters) or an array (class applied to indexed filter)
          filter_cssFilter : '', // or []
    
          // add a default column filter type "~{query}" to make fuzzy searches default;
          // "{q1} AND {q2}" to make all searches use a logical AND.
          filter_defaultFilter : {},
    
          // filters to exclude, per column
          filter_excludeFilter : {},
    
          // jQuery selector (or object) pointing to an input to be used to match the contents of any column
          // please refer to the filter-any-match demo for limitations - new in v2.15
          filter_external : '',
    
          // class added to filtered rows (rows that are not showing); needed by pager plugin
          filter_filteredRow : 'filtered',
    
          // ARIA-label added to filter input/select; {{label}} is replaced by the column header
          // "data-label" attribute, if it exists, or it uses the column header text
          //filter_filterLabel : 'Filter "{{label}}" column by...',
    
          // add custom filter elements to the filter row
          // see the filter formatter demos for more specifics
          filter_formatter : null,
    
          // add custom filter functions using this option
          // see the filter widget custom demo for more specifics on how to use this option
          filter_functions : null,
    
          // hide filter row when table is empty
          filter_hideEmpty : true,
    
          // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
          // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
          // in v2.26.6, this option will also accept a function
          filter_hideFilters : false,
    
          // Set this option to false to make the searches case sensitive
          filter_ignoreCase : true,
    
          // if true, search column content while the user types (with a delay).
          // In v2.27.3, this option can contain an
          // object with column indexes or classnames; "fallback" is used
          // for undefined columns
          filter_liveSearch : true,
    
          // global query settings ('exact' or 'match'); overridden by "filter-match" or "filter-exact" class
          filter_matchType : { 'input': 'exact', 'select': 'exact' },
    
          // a header with a select dropdown & this class name will only show available (visible) options within that drop down.
          filter_onlyAvail : 'filter-onlyAvail',
    
          // default placeholder text (overridden by any header "data-placeholder" setting)
          filter_placeholder : { search : '', select : '' },
    
          // jQuery selector string of an element used to reset the filters
          filter_reset : 'button.reset',
    
          // Reset filter input when the user presses escape - normalized across browsers
          filter_resetOnEsc : true,
    
          // Use the $.tablesorter.storage utility to save the most recent filters (default setting is false)
          filter_saveFilters : true,
    
          // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
          // every character while typing and should make searching large tables faster.
          filter_searchDelay : 300,
    
          // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
          filter_searchFiltered: true,
    
          // include a function to return an array of values to be added to the column filter select
          filter_selectSource  : null,
    
          // if true, server-side filtering should be performed because client-side filtering will be disabled, but
          // the ui and events will still be used.
          filter_serversideFiltering : false,
    
          // Set this option to true to use the filter to find text from the start of the column
          // So typing in "a" will find "albert" but not "frank", both have a's; default is false
          filter_startsWith : false,
    
          // Filter using parsed content for ALL columns
          // be careful on using this on date columns as the date is parsed and stored as time in seconds
          filter_useParsedData : false,
    
          // data attribute in the header cell that contains the default filter value
          filter_defaultAttrib : 'data-value',
    
          // filter_selectSource array text left of the separator is added to the option value, right into the option text
          filter_selectSourceSeparator : '|'
    
        }
    
      });
});
</script>
-->
<?$table_sorter->show_body();?>
</body>
</html>