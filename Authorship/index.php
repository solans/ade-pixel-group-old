<?php
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="<?=$gobase;?>JS/jquery-simple-upload.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="<?=$gobase;?>Authorship/style.css" rel="stylesheet" type="text/css" />
<title>MALTA Authorship</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php
  show_login(); 
?>
<p id="title" class="TITLE">MALTA authorship</p>

<ul>
  <li><a href="?page=authorlist">Authorlist</a></li> 
  <?php if(isAuthorised()){ ?>
  <li><a href="?page=authors">Authors</a></li>
  <li><a href="?page=institutes">Institutes</a></li>
  <li><a href="?page=affiliations">Affiliations</a></li>
  <?php } ?>
</ul>
 
<?php
if(@$_GET['page']=="authorlist"){
  include("authorlist.php");
}
else if(@$_GET['page']=="authors"){
  include("authors.php");
  include("author.php");
}
else if(@$_GET['page']=="author"){
  include("author.php");
  include("author_affiliations.php");
  include("author_affiliation.php");
  //include("add_affiliation.php");
}
else if(@$_GET['page']=="institutes"){
  include("institutes.php");
  include("institute.php");
}
else if(@$_GET['page']=="institute"){
  include("institute.php");
}
else if(@$_GET['page']=="affiliations"){
  include("affiliations.php");
  include("affiliation.php");
}
else if(@$_GET['page']=="affiliation"){
  include("affiliation.php");
}
?>



</div>
<?php
  show_footer();
?>
</div>
</body>
</html>

