<h2>Affiliation</h2>
<form method="GET" id="affiliation">
<table>
  <tr><th>Institute</th><td><select id="institute_id"></select></td></tr>
  <tr><th>Author</th><td><select id="author_id"></select></td></tr>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>End date</th><td><input type="text" id="enddate"></td></tr>
  <tr><th>Details</th><td><input type="text" id="details"></td></tr>  
</table>
<input type="hidden" id="affiliation_id" value="<?=@$_GET["affiliation_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="affiliation_reply" style="display:inline-block;"></div>

<script>
$(function() {
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
  $("#enddate").datepicker({dateFormat:'yy-mm-dd'});
  load_institutes();
  //load_authors();
  //if($("#affiliation_id").val()!=""){load_affiliation();}
});
  
$("#affiliation").submit(function(){
  if($("#affiliation_id").val()==""){add_affiliation();}
  else{update_affiliation();}
  return false;
});

function load_institutes(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_institutes"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["institute_id"]+">"+row["name"]+"</option>\n";
      }
      $("#institute_id").html(tt);
      load_authors();
    }
  }); 
}

function load_authors(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_authors"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["author_id"]+">"+row["initials"]+" "+row["lastname"]+"</option>\n";
      }
      $("#author_id").html(tt);
      if($("#affiliation_id").val()!=""){load_affiliation();}
    }
  }); 
}

function load_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_affiliation",
      affiliation_id:$("#affiliation_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#author_id").val(row["author_id"]);
      $("#institute_id").val(row["institute_id"]);
      $("#startdate").val(row["startdate"]);
      $("#enddate").val(row["enddate"]);
      $("#details").val(row["details"]);
    }
  });
}

function add_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_affiliation",
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_affiliations === 'function'){load_affiliations();}
        //$("#affiliation_id").val(reply["last_insert_id"]);
      }
      $("#affiliation_reply").text(msg);
      $("#affiliation").trigger("reset");
    }
  });
}

function update_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_affiliation",
      affiliation_id:$("#affiliation_id").val(),
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_affiliations === 'function'){load_affiliations();}
      }
      $("#affiliation_reply").text(msg);
    }
  });
}
</script>
