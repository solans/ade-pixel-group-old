<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_author"){
  $sql ="INSERT INTO authors ";
  $sql.=" (`firstname`, `lastname`, `initials`, `topics`, `comments`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["firstname"]."',"; 
  $sql.="'".$d["lastname"]."', ";
  $sql.="'".$d["initials"]."', ";
  //$sql.="('".implode(",", $d["topics"])."'), ";
  $sql.="'".$d["topics"]."', ";
  $sql.="'".$d["comments"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_author"){
  $sql ="UPDATE authors SET ";
  $sql.="`firstname`='".$d["firstname"]."', ";
  $sql.="`lastname`='".$d["lastname"]."', ";
  $sql.="`initials`='".$d["initials"]."', ";
  //$sql.="`topics`=('".implode(",", $d["topics"])."'), ";
  $sql.="`topics`='".$d["topics"]."', ";
  $sql.="`comments`='".$d["comments"]."' ";
  $sql.="WHERE ";
  $sql.="`author_id`='".$d["author_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_author"){
  $sql ="DELETE FROM authors WHERE ";
  $sql.="`author_id`='".$d["author_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_institute"){
  $sql ="INSERT INTO institutes ";
  $sql.=" (`name`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["name"]."' "; 
  $sql.=");";
}
else if($d["cmd"]=="update_institute"){
  $sql ="UPDATE institutes SET ";
  $sql.="`name`='".$d["name"]."' ";
  $sql.="WHERE ";
  $sql.="`institute_id`='".$d["institute_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_institute"){
  $sql ="DELETE FROM institutes WHERE ";
  $sql.="`institute_id`='".$d["institute_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_affiliation"){
  $sql ="INSERT INTO affiliations ";
  $sql.=" (`author_id`,`institute_id`,`startdate`,`enddate`,`details`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["author_id"]."', "; 
  $sql.="'".$d["institute_id"]."', "; 
  $sql.="'".$d["startdate"]."', "; 
  $sql.="'".$d["enddate"]."', "; 
  $sql.="'".$d["details"]."' "; 
  $sql.=");";
}
else if($d["cmd"]=="update_affiliation"){
  $sql ="UPDATE affiliations SET ";
  $sql.="`author_id`='".$d["author_id"]."', ";
  $sql.="`institute_id`='".$d["institute_id"]."', ";
  $sql.="`startdate`='".$d["startdate"]."', ";
  $sql.="`enddate`='".$d["enddate"]."', ";
  $sql.="`details`='".$d["details"]."' ";
  $sql.="WHERE ";
  $sql.="`affiliation_id`='".$d["affiliation_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_affiliation"){
  $sql ="DELETE FROM affiliations WHERE ";
  $sql.="`affiliation_id`='".$d["affiliation_id"]."' "; 
  $sql.=";";
}

//echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
  $ret["last_insert_id"]=$conn->insert_id;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>