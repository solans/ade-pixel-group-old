<h2>Author Affiliation</h2>
<form method="GET" id="author_affiliation">
<table>
  <tr><th>Institute</th><td><select id="institute_id"></select></td></tr>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>End date</th><td><input type="text" id="enddate"></td></tr>
  <tr><th>Details</th><td><input type="text" id="details"></td></tr>  
</table>
<input type="hidden" id="author_affiliation_id" value="<?=@$_GET["author_affiliation_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="author_affiliation_reply" style="display:inline-block;"></div>

<script>
$(function() {
  load_institutes();
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
  $("#enddate").datepicker({dateFormat:'yy-mm-dd'});
//  if($("#author_affiliation_id").val()!=""){load_author_affiliation();}
});
  
$("#author_affiliation").submit(function(){
  add_author_affiliation();
  //if($("#author_affiliation_id").val()==""){add_author_affiliation();}
  //else{update_author_affiliation();}
  return false;
});

function load_institutes(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_institutes"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["institute_id"]+">"+row["name"]+"</option>\n";
      }
      $("#institute_id").html(tt);
    }
  });
}

function load_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_author_affiliation",
      author_affiliation_id:$("#author_affiliation_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#institute_id").val(row["institute_id"]);
      $("#startdate").val(row["startdate"]);
      $("#enddate").val(row["enddate"]);
      $("#details").val(row["details"]);
    }
  });
}

function add_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_affiliation",
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_author_affiliations === 'function'){load_author_affiliations();}
        //$("#author_affiliation_id").val(reply["last_insert_id"]);
        $("#author_affiliation").trigger("reset");
      }
      $("#author_affiliation_reply").text(msg);
    }
  });
}

function update_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_affiliation",
      author_affiliation_id:$("#author_affiliation_id").val(),
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_author_affiliations === 'function'){load_author_affiliations();}
      }
      $("#author_affiliation_reply").text(msg);
    }
  });
}
</script>
