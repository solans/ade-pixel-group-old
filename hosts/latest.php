<?
include_once("helper.php");
include_once("schema.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    

?>
<p class="SUBTITLE">Last update</p>

<table style="text-align:left; width:100%">
  <thead>
  <?
  $card=new Computer();
  foreach($card->GetMembers() as $k){
    echo '  <td>'.$card->GetLabel($k).'</td>'."\n";
  }
  ?>
  </thead>
  <?
  foreach($helper->GetData()->Select("hostname","date") as $p){
    ?>
    <tr>
      <?
      foreach($card->GetMembers() as $k){
        echo '  <td>'.$p->GetValue($k).'</td>'."\n";
      }
      ?>
    </tr>
    <?
  }
  ?>
</table>
