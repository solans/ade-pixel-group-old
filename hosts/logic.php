<?
/** August 2020 **/
include_once("../functions.php");
include_once("helper.php");
include_once("schema.php");
include_once("querystring.php");
include_once('email.php');

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$collection = $helper->GetData();

$sub = "ADE computers";
$app = "https://ade-pixel-group.web.cern.ch/hosts/";
    
if(@$_POST["action"]=="Add"){

  //Add to collection
  $obj = new Computer();
  $_POST["id"]=$collection->GetNextId();
  $obj->FromArray($_POST);
  $collection->Add($obj);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');

  //send a mail
  $muser = mailuser();
  $email = new email();
  $email->setSender("ATLAS Pixel group", $muser["email"]);
  $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
  $email->setAuthType("LOGIN");
  $email->setCrypto("TLS");
  $email->setSmtp(true);
  $email->setSubject($sub);
  $email->addTo("carlos.solans@cern.ch");
  $message  = "Dear responsible,\n\n";
  $message .= "object was added:\n";
  $message .= $obj->ToString();
  $message .= "Link: ".$app."?action=edit&id=".$_POST["id"]."\n";
  $message .= "\n";
  $message .= "Have a nice day";
  $email->setMessage($message);
  $sent=$email->send();
  
  //redirect 
  $qs = new querystring();
  $qs->remove('action');
  $qs->remove('id');
  redirect($app.$qs->toFullString());
  
}else if(@$_POST["action"]=="Edit"){

  //Add to collection
  $obj = $collection->Get($_POST["id"]);
  $obj->FromArray($_POST);
  $collection->Del($obj->GetId());
  $collection->Add($obj);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');

  //send a mail
  $muser = mailuser();
  $email = new email();
  $email->setSender("ATLAS Pixel group", $muser["email"]);
  $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
  $email->setAuthType("LOGIN");
  $email->setCrypto("TLS");
  $email->setSmtp(true);
  $email->setSubject($sub);
  $email->addTo("carlos.solans@cern.ch");
  $message  = "Dear responsible,\n\n";
  $message .= "object was modified:\n";
  $message .= $obj->ToString();
  $message .= "Link: ".$app."?action=edit&id=".$_POST["id"]."\n";
  $message .= "\n";
  $message .= "Have a nice day";
  $email->setMessage($message);
  $sent=$email->send();
  
  //redirect 
  $qs = new querystring();
  $qs->remove('action');
  $qs->remove('id');

  //$obj->Dump();print_r($_POST);exit();
  if(@$_POST["redirect"]){redirect($app.$_POST["redirect"]);}
  redirect($app.$qs->toFullString());

}else if(@$_GET["action"]=="Remove"){

  //Delete from collection
  $obj = $collection->Get($_GET["id"]);
  $collection->Del($_GET["id"]);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');
  
  //send a mail
  $muser = mailuser();
  $email = new email();
  $email->setSender("ATLAS Pixel group", $muser["email"]);
  $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
  $email->setAuthType("LOGIN");
  $email->setCrypto("TLS");
  $email->setSmtp(true);
  $email->setSubject($sub);
  $email->addTo("carlos.solans@cern.ch");
  $message  = "Dear responsible,\n\n";
  $message .= "object was removed:\n";
  $message .= $obj->ToString();
  $message .= "Link: ".$app."?action=edit&id=".$_GET["id"]."\n";
  $message .= "\n";
  $message .= "Have a nice day";
  $email->setMessage($message);
  $sent=$email->send();
  
  //redirect 
  $qs = new querystring();
  $qs->set('action','list');
  $qs->remove('id');
  redirect($app.$qs->toFullString());

}else{
  print_r($_POST);
  echo "<h1>Nothing to do here</h1>";
}

?>