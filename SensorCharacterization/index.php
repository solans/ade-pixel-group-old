<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/img/ATLAS-icon.ico">
<title>Sensor characterization</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">Sensor characterization</p>

<p class="SUBTITLE">Why an R&amp;D on CMOS sensors?
  <ul>
		<li>Large deplpetion length</li>
		<li>Fully produced by industrial CMOS process</li>
		<li>Low power consumption.</li>
		<li>Reduction in production costs.</li>
		<li>Consequent reduction of radiation length in passive material.</li>
	</ul>
	<br/>
  <img width="30%" alt="Hybrid pixel" src="images/hybrid-pixel.png">
  <img width="40%" alt="Monolithic pixel" src="images/monolithic-pixel.png">
</p>

<p class="SUBTITLE">What do we do?
  <ul>
    <li>Characterization of different sensor technologies</li>
    <li>Assistance with test-beam measurements</li>
  </ul>
  <br/>
</p>

<p class="SUBTITLE">Links
  <ul>
    <li><a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/CernAtlasPixelSensorsRD">CERN ATLAS Pixel Sensor R&amp;D twiki</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Main/CernAtlasPixelSensorsLaserSetup">CERN Lab Laser setup</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
