<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_sample"){
  $sql ="INSERT INTO samples (";
  $sql.=" `type`,";
  $sql.=" `subsystem`,";
  $sql.=" `responsible`,";
  $sql.=" `handler`,";
  $sql.=" `description`";
  $sql.=" ) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["type"]."',"; 
  $sql.="'".$d["subsystem"]."', ";
  $sql.="'".$d["responsible"]."', ";
  $sql.="'".$d["handler"]."', ";
  $sql.="'".$d["description"]."' ";
  $sql.=");";
}
if($d["cmd"]=="add_measurement"){
  $sql ="INSERT INTO measurements ";
  $sql.=" (`sample_id`, `date`, `measurement`, `unit`, `env_temp_celsius`, `shifter`, `type`,`equipment`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["measurement"]."', ";
  $sql.="'".$d["unit"]."', ";
  $sql.="'".$d["env_temp_celsius"]."', ";
  $sql.="'".$d["shifter"]."', ";
  $sql.="'".$d["type"]."', ";
  $sql.="'".$d["equipment"]."' ";
  $sql.=");";
}
if($d["cmd"]=="update_measurement"){
  $sql ="UPDATE measurements SET ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`measurement`='".$d["measurement"]."', ";
  $sql.="`unit`='".$d["unit"]."', ";
  $sql.="`env_temp_celsius`='".$d["env_temp_celsius"]."', ";
  $sql.="`shifter`='".$d["shifter"]."', ";
  $sql.="`type`='".$d["type"]."' ";
  $sql.="WHERE `measurement_id`='".$d["measurement_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_location"){
  $sql ="INSERT INTO locations ";
  $sql.=" (`sample_id`, `date`, `location`,`tracking`,`reference`,`responsible`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["tracking"]."', ";
  $sql.="'".$d["reference"]."', ";
  $sql.="'".$d["responsible"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="delete_location"){
  $sql ="DELETE FROM locations ";
  $sql.="WHERE location_id='".$d["location_id"]."';";
}

echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>