<h2>Measurements</h2>
<button id="measurements_reset">Reset filters</button>
<button id="measurements_export">Export</button>
<div id="measurements_found" style="display:inline-block"></div>
<table id="measurements" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Type</th>
		<th data-placeholder="Search...">Equipment</th>
		<th data-placeholder="Search...">Shifter</th>		
		<th>Actions</th>
	</thead>
	<tbody id="measurements_body">
	</tbody>
</table>

<script>

$(function() {
  $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_measurements("<?=$_GET["sample_id"];?>");
});

$("#measurements").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#measurements").on("filterEnd",function(){
  $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2))
});

$("#measurements_export").click(function() {
  $("#measurements").trigger("outputTable");
});

$("#measurements_reset").click(function() {
  $("#measurements").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_measurements(sample_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {
      cmd:"get_measurements",
      sample_id:sample_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#measurements_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["measurement"]+"&nbsp;"+row["unit"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["type"]+"</td>";
        tt+="<td>"+row["equipment"]+"</td>";
        tt+="<td>"+row["shifter"]+"</td>";
        tt+="<td><a href=\"index.php?page=measurement&sample_id="+row["sample_id"]+"&measurement_id="+row["measurement_id"]+"\">update</td>";
        tt+="</tr>\n"; 
        $("#measurements_body").append(tt);
      }
      $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2));
    }
  });
}
</script>
