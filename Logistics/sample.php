<h2>Sample</h2>
<form method="GET" id="sample">
<table>
  <tr><th>Reference</th><td><div id="reference"></div></td></tr>
  <tr><th>Type</th><td>
    <select id="type">
      <option value=""></option>
      <option value="CBL">CBL</option>
    </select>
  </td></tr>
  <tr><th>Subsystem</th><td>
    <select id="subsystem">
      <option value=""></option>
      <option value="SRV">SRV</option>
      <option value="GEN">GEN</option>
      <option value="PIX">PIX</option>
      <option value="STR">STR</option>
      <option value="CEL">CEL</option>
      <option value="CEM">CEM</option>
    </select>
  </td></tr>
  <tr><th>Other ID</th><td><input id="other_id" type="text"/></td></tr>
  <tr><th>Responsible</th><td><input id="responsible" type="text"/></td></tr>
  <tr><th>Handler</th><td><input id="handler" type="text"/></td></tr>
  <tr><th>Description</th><td><input id="description" type="text"/></td></tr>
</table>
<input type="hidden" id="sample_id" value="<?=@$_GET['sample_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="sample_reply" style="display:inline-block;"></div>

<script>

$("#sample").submit(function(){
  add_sample();
  return false;
});

$(function() {
  load_sample("<?=$_GET["sample_id"];?>");
});

$("#type").on('change',function(){$("#reference").html(build_reference());});
$("#subsystem").on('change',function(){$("#reference").html(build_reference());});

function load_sample(sample_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {
      cmd:"get_sample",
      sample_id:sample_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      sample=reply[0];
      $("#sample_id").val(sample["sample_id"]);
      $("#type").val(sample["type"]);
      $("#subsystem").val(sample["subsystem"]);
      $("#responsible").val(sample["responsible"]);
      $("#handler").val(sample["handler"]);
      $("#description").val(sample["description"]);
      $("#other_id").val(sample['other_id']);
      $("#other_id").val(sample['other_id']);
      $("#reference").html(build_reference());
    }
  });
}

function build_reference(){
  ref ="ITK-"+$("#type").val()+"-"+$("#subsystem").val()+"-";
  for(i=0;i<4-$("#sample_id").val().length;i++){ref+="0";}
  ref+=$("#sample_id").val();
  return ref;
}

function add_sample(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"add_sample",
      type:$("#type").val(),
      subsystem:$("#subsystem").val(),
      responsible:$("#responsible").val(),
      handler:$("#handler").val(),
      description:$("#description").val(),
      other_id:$("#other_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_sample_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_sample_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_sample_reply").text("Stored");
      }
    }
  });
}
</script>
