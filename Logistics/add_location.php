<h2>Add location</h2>
<form method="GET" id="add_location">
<table id="sources" class="tablesorter">
  <tr><th>Location</th><td><input id="location_value" type="text"/></td></tr>
  <tr><th>Date</th><td><input id="location_date" type="text"/></td></tr>
  <tr><th>Tracking</th><td>
    <select id="location_tracking">
      <option value=""></option>
      <option value="EDH">EDH</option>
      <option value="DHL">DHL</option>
      <option value="UPS">UPS</option>
    </select>  
  </td></tr>
  <tr><th>Reference</th><td><input id="location_reference" type="text"/></td></tr>
  <tr><th>Responsible</th><td><input id="location_responsible" type="text"/></td></tr>
</table>
<input type="hidden" id="sample_id" value="<?=$_GET["sample_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_location_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#location_date").datepicker({dateFormat:"yy-mm-dd"});
});

$("#add_location").submit(function(){
  add_location();
  return false;
});

function add_location(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"add_location",
      sample_id:$("#sample_id").val(),
      location:$("#location_value").val(),
      date:$("#location_date").val(),
      tracking:$("#location_tracking").val(),
      reference:$("#location_reference").val(),
      responsible:$("#location_responsible").val()
  },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_location_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_location_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_location_reply").text("Stored");
        load_locations($("#sample_id").val());
      }
    }
  });
}
</script>
