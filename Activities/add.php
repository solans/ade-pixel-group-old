<?
include_once("helper.php");
include_once("activity.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$collection = $helper->GetData();
$qs = new querystring();
?>
<p class="SUBTITLE">Add a new activity to the schedule</p>
<?
$activity=new activity();
echo $activity->ToHtmlForm(array("url"=>"logic.php", "method"=>"POST", "action"=>"Add"));  
?>


