<?
include_once("helper.php");
include_once("activity.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    

$collection=$helper->GetData();

?>
<p class="SUBTITLE">List of activities in the schedule</p>

<table style="text-align:left; width:100%">
  <thead>
  <?
  $activity=new Activity();
  foreach($activity->GetMembers() as $k){
    echo '  <td>'.$activity->GetLabel($k).'</td>'."\n";
  }
  ?>
  </thead>
  <?
  foreach($collection->data as $p){
    ?>
    <tr>
      <?
      foreach($activity->GetMembers() as $k){
        echo '  <td>'.$p->GetValue($k).'</td>'."\n";
      }
      ?>
      <td>
        <a href="<? $qs->set('action','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
        <a href="<? $qs->set('action','Remove'); $qs->set('id',urlencode($p->GetId())); echo 'logic.php'.$qs->toFullString()?>">delete</a> 
      </td>
    </tr>
    <?
  }
  ?>
</table>
