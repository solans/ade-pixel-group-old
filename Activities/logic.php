<?
/** April 2020 **/
/** Nov 2020 Ignacio adding check lab occupancy
 * **/
include_once("../functions.php");
include_once("helper.php");
include_once("activity.php");
include_once("querystring.php");
include_once('email.php');

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$collection = $helper->GetData();

$app = "https://ade-pixel-group.web.cern.ch/Activities/";
    

function IsActivityValid($collection,$obj){
  $debug=False;
  $response="OK";
  // Check occupancy
  $maxOccupancy=2;
  $collitions=0;
  $lab = $obj->GetValue("lab");
  $requestbegin = new DateTime($obj->GetValue("startdate"));
  $requestend   = new DateTime($obj->GetValue("enddate"));//strtotime($obj->GetValue("enddate"));
  //echo "Checking from:".$begin->format('Y-m-d H:i:s')." to ". $end->format('Y-m-d H:i:s')."\n\n";
  for($i = $requestbegin; $i <= $requestend; $i->modify('+1 hour')){
      $occupancy_i=0;
      if ($debug) {echo "Checking from:".$i->format('Y-m-d H:i:s')."...<br>";}
      foreach($collection->data as $mk=>$mv){
          $collides=False;
          if (($mv->GetValue("startdate")!="") && ($mv->GetValue("id")!=$obj->GetValue("id")) && ($mv->GetValue("lab")==$lab)){
              $chkStartTime=new DateTime($mv->GetValue("startdate"));
              $chkEndTime=new DateTime($mv->GetValue("enddate"));
              if ($i>$chkStartTime && $i<$chkEndTime){
                  $participants=$mv->GetValue("participants");
                  if ($debug) {echo "Participants:". $participants;}
                  if ($participants!=null){
                      $occupancy_i+=int($participants);
                  }else{$occupancy_i+=1;}
                  }
              }
          }
          if ($occupancy_i>$maxOccupancy-1){
           $collitions+=1;
           if ($debug) {echo "Checking from:".$i->format('Y-m-d H:i:s')."...There are already: ".$occupancy_i." persons<br>";}
          }
       }
       if ($collitions>0){
           $response="Can't process request. Maximum occupancy of labs is ".$maxOccupancy." persons. Try another dates.";
       }
    return $response;
}


if(@$_POST["action"]=="Add"){
    
    //Check is not busy date
    
    $obj = new Activity();
    $_POST["id"]=$collection->GetNextId();
    $obj->FromArray($_POST);
    $collection->Add($obj);
    $helper->SetData($collection);
    //$resp=$helper->IsActivityValid($obj);
    $resp=IsActivityValid($collection,$obj);
    if ($resp=="OK"){
      //Add to collection
      $helper->Write($filename, 'w');
      //send a mail
      
      $muser = mailuser();
      $email = new email();
      $email->setSender("ATLAS Pixel group", $muser["email"]);
      $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
      $email->setAuthType("LOGIN");
      $email->setCrypto("TLS");
      $email->setSmtp(true);
      $email->setSubject("Activity schedule");
      $email->addTo("carlos.solans@cern.ch");
      $email->addTo("julia.cachet@cern.ch");
      $email->addTo("laetitia.michele.bardo@cern.ch");
      $message  = "Dear responsible,\n\n";
      $message .= "the following activity was added to the schedule:\n";
      $message .= $obj->ToString();
      $message .= "Link: ".$app."?action=edit&id=".$_POST["id"]."\n";
      $message .= "\n";
      $message .= "Have a nice day";
      $email->setMessage($message);
      $sent=$email->send();
      $email->setTo($obj->GetValue("email"));
      $sent=$email->send();
      
      //redirect 
      $qs = new querystring();
      $qs->remove('action');
      $qs->remove('id');
      $qs->set('date',$obj->GetValue('startdate'));
      redirect($app.$qs->toFullString());
      
    }else{
        echo $resp;
    } 
}else if(@$_POST["action"]=="Edit"){
   
  //Add to collection
  $obj = $collection->Get($_POST["id"]);
  $obj->FromArray($_POST);
  $collection->Del($obj->GetId());
  $collection->Add($obj);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');
  
  //send a mail
  $muser = mailuser();
  $email = new email();
  $email->setSender("ATLAS Pixel group", $muser["email"]);
  $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
  $email->setAuthType("LOGIN");
  $email->setCrypto("TLS");
  $email->setSmtp(true);
  $email->setSubject("Activity schedule");
  $email->addTo("carlos.solans@cern.ch");
  $email->addTo("julia.cachet@cern.ch");
  $email->addTo("laetitia.michele.bardo@cern.ch");
  $message  = "Dear responsible,\n\n";
  $message .= "the following activity was modified on the schedule:\n";
  $message .= $obj->ToString();
  $message .= "Link: ".$app."?action=edit&id=".$_POST["id"]."\n";
  $message .= "\n";
  $message .= "Have a nice day";
  $email->setMessage($message);
  $sent=$email->send();
  $email->setTo($obj->GetValue("email"));
  $sent=$email->send();
  
  //redirect 
  $qs = new querystring();
  $qs->remove('action');
  $qs->remove('id');

  //$obj->Dump();print_r($_POST);exit();
  if(@$_POST["redirect"]){redirect($app.$_POST["redirect"]);}
  redirect($app.$qs->toFullString());

}else if(@$_GET["action"]=="Remove"){

  //Delete from collection
  $obj = $collection->Get($_GET["id"]);
  $collection->Del($_GET["id"]);
  $helper->SetData($collection);
  $helper->Write($filename, 'w');
  
  //send a mail
  $muser = mailuser();
  $email = new email();
  $email->setSender("ATLAS Pixel group", $muser["email"]);
  $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
  $email->setAuthType("LOGIN");
  $email->setCrypto("TLS");
  $email->setSmtp(true);
  $email->setSubject("Activity schedule");
  $email->addTo("carlos.solans@cern.ch");
  $email->addTo("julia.cachet@cern.ch");
  $email->addTo("laetitia.michele.bardo@cern.ch");
  $message  = "Dear responsible,\n\n";
  $message .= "the following activity was removed from the schedule:\n";
  $message .= $obj->ToString();
  $message .= "Link: ".$app."?action=edit&id=".$_GET["id"]."\n";
  $message .= "\n";
  $message .= "Have a nice day";
  $email->setMessage($message);
  $sent=$email->send();
  $email->setTo($obj->GetValue("email"));
  $sent=$email->send();
  
  //redirect 
  $qs = new querystring();
  $qs->set('action','list');
  $qs->remove('id');
  redirect($app.$qs->toFullString());

}else{
  print_r($_POST);
  echo "<h1>Nothing to do here</h1>";
}

?>