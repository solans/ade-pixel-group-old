<?
/* Date: April 2020 */

include_once("helper.php");

Helper::AddType("Activity");

class Activity extends Collectible{

  function __construct(){
    $this->members = array(
      "id"=>array("type"=>"string","label"=>"id","display"=>"hidden","value"=>"0"),
      "lab"=>array("type"=>"enum","values"=>array("161-1-023","161-1-024","161-1-009","161-1-008","161-1-006"),"value"=>"161-1-009","required"=>true,"display"=>"50","label"=>"Lab"),
      "task"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Task","value"=>""),
      "name"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Participant","value"=>""),
      "pax"=>array("type"=>"enum","values"=>array("1","2"),"value"=>"1","required"=>true,"display"=>"2","label"=>"Number of participants"),
      "startdate"=>array("type"=>"datetime","required"=>true,"display"=>"50","label"=>"Start time and date","value"=>"NOW"),
      "enddate"=>array("type"=>"datetime","required"=>true,"display"=>"50","label"=>"End time and date","value"=>"NOW+1h"),
      "phone"=>array("type"=>"string","required"=>false,"display"=>"50","label"=>"Phone number","value"=>""),
      "email"=>array("type"=>"string","required"=>false,"display"=>"50","label"=>"E-mail","value"=>""),
      "comments"=>array("type"=>"text","required"=>false,"display"=>"10","cols"=>"50","rows"=>"10","label"=>"Comments","value"=>""),
      "live"=>array("type"=>"bool","required"=>false,"display"=>"hidden","label"=>"Live","value"=>"0"),
    );
    $this->classname = "Activity";
    $this->tag = "activity";
    $this->id = "id";
  }
  
}

?>