<?
include_once("helper.php");
include_once("activity.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
?>

<p class="SUBTITLE">Edit an activity in the schedule</p>
<?
$collection = $helper->GetData();
$activity=$collection->Get($qs->get("id"));
if(!$activity){echo "<h3>Activity id not found: ".$qs->get("id")."<h3>";}
else{
  echo $activity->ToHtmlForm(array("url"=>"logic.php", "method"=>"POST", "action"=>"Edit", "redirect"=>"?action=edit&id=".$qs->get("id")));  
}
?>
<p>
<a href="<? $qs->set('action','Remove'); $qs->set('id',urlencode($qs->get("id"))); echo 'logic.php'.$qs->toFullString()?>">Delete the activity</a> 
</p>