<div id="view" style="display:inline-block"></div>


<script>

$(function() {
  load_topics();
});


function load_topics(){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_topics"
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#view").empty();
      for (row of rows){
        tt ="<h2>"+row["name"]+"</h2>";
        tt+="<div id='topic_"+row["topic_id"]+"'>\n";
        tt+="</div>\n"; 
        $("#view").append(tt);
        load_topic(row["topic_id"]);
      }
    }
  }); 
}

function set_title_for_topic(topic_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_topic",
      topic_id:topic_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      for (row of rows){
        $("title").text(row["name"]+" Public Plots");
        $("#title").text(row["name"]+" Public Plots");
      }
    }
  }); 
}



function load_topic(topic_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_sections_by_topic",
      topic_id:topic_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#topic_"+topic_id).empty();
      for (row of rows){
        tt ="<h3>"+row["name"]+"&nbsp;<a href='?page=view&topic_id="+topic_id+"&section_id="+row["section_id"]+"'>link</a></h3>";
        $("#topic_"+topic_id).append(tt);
      }
    }
  }); 
}

</script>
