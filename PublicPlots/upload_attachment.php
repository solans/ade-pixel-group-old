<!--https://www.jqueryscript.net/form/drag-drop-upload.html -->
<h2>Upload attachment</h2>
<form method="GET" id="upload_attachment">
<input type="file" name="file" multiple="false" id="upload">
<div id="upload_drop_zone" style="height:150px; border: 1px solid #ccc; text-align: center; line-height: 150px; font-size: 1.5em;">
  <div>Drop files here</div>
</div>
<div id="upload_progress"></div>
<div id="upload_message"></div>
<script>
  $("#upload").simpleUpload({
    url: "<?=$gobase;?>/PublicPlots/upload.php",
    method: "post",
    params: { param: 'test' },
    dropZone: "#upload_drop_zone",
	  progress: "#upload_progress"
   }).on('upload:before', function(e, files) {
     console.log('before');
     console.log(files);
   }).on('upload:after', function(e, files) {
     console.log('after');
     console.log(files);
   }).on('upload:start', function(e, file, i) {
     console.log('start ' + i);
     console.log(file);
   }).on('upload:progress', function(e, file, i, loaded, total) {
     console.log('progress ' + i + ' ' + loaded + '/' + total);
   }).on('upload:end', function(e, file, i) {
     console.log('end ' + i);
     console.log(file);
   }).on('upload:done', function(e, file, i) {
     console.log('done ' + i);
     console.log(file);
     if(file.name.includes(".pdf")){$("#pdf").val(file.name);}
     if(file.name.includes(".png")){$("#png").val(file.name);}
     $('#upload_message').prepend('<p>done: ' + file.name + '</p>');
   }).on('upload:fail', function(e, file, i) {
     console.log('fail ' + i);
     console.log(file);
     $('#upload_message').prepend('<p>fail: ' + file.name + '</p>');
   });
</script>
