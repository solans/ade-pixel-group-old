<h2>Section</h2>
<form method="GET" id="section">
<table>
  <tr><th>Topic</th><td><select id="topic_id"></select></td></tr>
  <tr><th>Name</th><td><input type="text" id="name"></td></tr>
</table>
<input type="hidden" id="section_id" value="<?=@$_GET["section_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_section_reply" style="display:inline-block;"></div>

<script>
$(function() {
 load_topics(load_section);
});
  
$("#section").submit(function(){
  if($("#section_id").val()==""){alert("Add section?");add_section();}
  else{alert("Update section?");update_section();}
  return false;
});

function load_topics(nested){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_topics"
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#topic_id").html("");
      for(topic of reply){
        $("#topic_id").append('<option value="'+topic["topic_id"]+'">'+topic["name"]+'</option>');
      }
      nested();
    }
  });
}

function load_section(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_section",
      section_id:$("#section_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      section=reply[0];
      $("#topic_id").val(section["topic_id"]);
      $("#section_id").val(section["section_id"]);
      $("#name").val(section["name"]);
    }
  });
}

function add_section(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_section",
      topic_id:$("#topic_id").val(),
      name:$("#name").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_section_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_section_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_section_reply").text("Stored");
        if(typeof load_plots === 'function'){
          load_sections();
        }
        $("#section_id").val(reply["last_insert_id"]);
      }
    }
  });
}

function update_section(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_section",
      topic_id:$("#topic_id").val(),
      section_id:$("#section_id").val(),
      name:$("#name").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_section_eply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_section_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_section_reply").text("Stored");
      }
    }
  });
}


</script>
