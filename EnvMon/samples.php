<h2>Samples</h2>
<button id="samples_reset">Reset filters</button>
<button id="samples_export">Export</button>
<div id="samples_found" style="display:inline-block"></div>
<table id="samples" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Other ID</th>
		<th data-placeholder="Search...">Description</th>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Measurement date</th>
		<th class="filter-select">Location</th>
		<th data-placeholder="Search...">Location date</th>
	</thead>
	<tbody id="samples_body">
	</tbody>
</table>

<div id="samples_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_samples();
});

$("#samples").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

$("#samples").on("filterEnd",function(){
  $("#samples_found").html("Found: "+($("#samples tr:visible").length-2))
});

$("#samples_export").click(function() {
  $("#samples").trigger("outputTable");
});

$("#samples_reset").click(function() {
  $("#samples").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/** Then load members **/
function load_samples(){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_samples_summary"},
    success: function(data) {
      console.log(data);
      samples=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#samples_body").empty();
      for (sample of samples){
        tt="<tr>\n";
        tt+="<td><a href=\"index.php?page=sample&sample_id="+sample["sample_id"]+"\" title=\"Open\">"+sample["other_id"]+"</a></td>";
        tt+="<td>"+sample["description"]+"</td>";
        tt+="<td>"+sample["measurement"]+"&nbsp;"+sample["measurement_unit"]+"</td>";
        tt+="<td>"+sample["measurement_date"]+"</td>";
        tt+="<td>"+sample["location"]+"</td>";
        tt+="<td>"+sample["location_date"]+"</td>";
        tt+="</tr>\n"; 
        $("#samples_body").append(tt);
      }
      $('#samples').trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#samples_found").html("Found: "+($("#samples tr:visible").length-2));
    }
  });
}


</script>
