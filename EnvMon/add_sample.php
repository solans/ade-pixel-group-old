<h2>Add sample</h2>
<form method="GET" id="add_sample">
<table id="sources" class="tablesorter">
  <tr><th>Other ID</th><td><input id="sample_other_id" type="text"/></td></tr>
  <tr><th>Description</th><td><input id="sample_description" type="text"/></td></tr>
</table>
<input type="hidden" id="sample_id" value="<?=@$_GET["sample_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_sample_reply" style="display:inline-block;"></div>

<script>
$("#add_sample").submit(function(){
  add_sample();
  return false;
});

function add_sample(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"add_sample",
      other_id:$("#sample_other_id").val(),
      description:$("#sample_description").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_sample_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_sample_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_sample_reply").text("Stored");
      }
    }
  });
}
</script>
