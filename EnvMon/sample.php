<h2>Sample</h2>
<table id="sample">
  <tr><th>Other ID</th><td><input id="other_id" type="text"/></td></tr>
  <tr><th>Description</th><td><input id="description" type="text"/></td></tr>
</table>

<script>
$(function() {
  load_sample("<?=$_GET["sample_id"];?>");
});

function load_sample(sample_id){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {
      cmd:"get_sample",
      sample_id:sample_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      sample=reply[0];
      $("#other_id").val(sample['other_id']);
      $("#description").val(sample['description']);
    }
  });
}
</script>
