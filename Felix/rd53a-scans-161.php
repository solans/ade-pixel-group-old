<?php
include_once('../functions.php');
$HOME="/home/itkpix";
$FELIXSW=$HOME."/felix-sw/felix-5.0-latest";
$FELIXDR=$HOME."/felix-dw";
$FELIXFW=$HOME."/felix-fw";
$ITKSW=$HOME."/itk-felix-sw";
$TDAQ_VERSION="9.4.0";
$FLX_FW_VERSION="FLX712_PIXEL_24CH_2022-03-14";
$FLX_SW_VERSION="4.2.0 RM5";
$FLX_DR_VERSION="4.9";
$FLX_DR_FILE="tdaq_sw_for_Flx-4.9.0-2dkms.noarch.rpm";
$BINARY_TAG="x86_64-centos7-gcc11-opt";
$ITK_FLX_SW_VERSION="master";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="/img/ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>RD53A setup in building 161</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">RD53A setup in building 161</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="Introduction" class="SUBTITLE">Introduction</h2>

An RD53A-FELIX setup is available in 161-R-009 at CERN. 

<div>
    <img class="IMAGEW600" src="images-rd53a-tests-161/setup-rd53a-161-labels.png"/>
</div>
 
<h2 id="DP_to_pigtail_data" class="SUBTITLE">Check the connections</h2>

<table>
    <tr>
        <td>
            <img class="SMALLIMAGE ZOOM" src="images-rd53a-tests-161/pigbt-vldb-bob-chip.jpg"/>
            <p>PIGBT, VLDB+, BOB, chip and fiber plant</p>
        </td>
        <td>
            <img class="SMALLIMAGE ZOOM" src="images-rd53a-tests-161/computer-screen.jpg"/>
            <p>The computer screen</p>
        </td>
    </tr>
    <tr>
        <td>
            <img class="SMALLIMAGE ZOOM" src="images-rd53a-tests-161/power-supplies.jpg"/>
            <p>The power supplies</p>
        </td>
        <td>
            <img class="SMALLIMAGE ZOOM" src="images-rd53a-tests-161/felix-server.jpg"/>
            <p>The FELIX server</p>
        </td>
    </tr>
</table>

<h2 id="Power_control" class="SUBTITLE">Powering up the setup</h2>
To power LpGBT and RD53A you can use the DCS functionality in pcatlidros02.
</p>
<pre>
source <?=$ITKSW;?>/setup.sh
PSUControlGUI.py -f <?=$ITKSW;?>/PySerialComm/CRCard/FLX712.card
</pre>

<img class="MEDIUMIMAGE" src="images/psu-control-gui.png"/>


<h2 id="config-lpgbt-pigbt" class="SUBTITLE">Configure the VLDB+</h2>
<p>
After powering the LpGBT (Check DCS section for more details), star a browser connected to CERN network and go to the following url: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a> 

Then select Real LpGBT and connect to the hardware.
The following setting are used: 
<ul>
<li> TRX
<li> TXdata rate = 10 Gbps
<li> TX encoding = FEC5
<li> Uplink EPRX (Click gear symbol) 
<ul>
<li> EPRX0 
<li> Data Rate = 1280
<li> Track Mode = Continues
<li> Control TERM
<li> Equalization OFF

<li> EPRX1<li>1-6
<li> Data Rate off
</ul>		
<li> Downlink EPTX
<ul>
<li> Data Rate 160 Mpbs
<li> Drive strength 4.0 mA
</ul>		 		  
<li> click on the menu -> high speed
<ul>
<li> Invert high speed data output enabled (Invert polarity uplinks)
</ul>
</ul>
</p>

<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-1.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-2.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-3.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-4.png"/>
</div>
<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-5.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-6.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-7.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-8.png"/>
</div>




<h2 id="felixcore" class="SUBTITLE">Running felixcore</h2>

Felixcore is an application provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIO protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIO.

<ol>
    <li>Setup the felixsw
        <pre>source <?=$HOME.$FELIXSW;?>/setup.sh</pre>
    </li>
    <li> Start felixcore
        <pre> <?=$HOME.$FELIXSW.$BINARY_TAG;?>/felixcore/felixcore -d 0 --data-interface lo --elinks 0,4,8,12    </pre>
    </li>
    <li>Wait until the message "Felixcore is up and running" appears on the screen</li>
</ol>


<h2 id="scan_manager" class="SUBTITLE">Running scan_manager</h2>
  
<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s [scan-name] 
</pre>



<h2 id="connectivity" class="SUBTITLE">Connectivity file</h2>

The format of the connectivity file are the follwing:
<pre>
{
  "connectivity" : [
    {"name": "rd53a_1", 
     "config" : "default_rd53a.json", 
     "rx" :  0, 
     "tx" :  0, 
     "host": "127.0.0.1", 
     "cmd_port": 12340, 
     "data_port": 12350, 
     "enable" : 1, 
     "locked" : 0
    }    
  ]
}
</pre>

Each line of the file needs the following:

<table>
  <tr><td>Attribute</td><td>Type</td><td>Description</td></tr>
  <tr><td>name</td><td>string</td><td>Front-end identifier name (rd53a_1, A_BM_01_1, Quad13_1...)</td></tr>
  <tr><td>config</td><td>string</td><td>JSON configuration file:  (default_rd53a.json should be fine)</td></tr>
  <tr><td>rx</td><td>int</td><td>is the data e-link (we can keep it at 0)</td></tr>
  <tr><td>tx</td><td>int</td><td>is the command e-link (we can keep it at 0)</td></tr>
  <tr><td>host</td><td>string</td><td>"127.0.0.1" if you run  scan_manager_rd53 on the same FELIX server</td></tr>
  <tr><td>cmd_port</td><td>int</td><td>the port used by felix-core to receive the commands (12340)</td></tr>
  <tr><td>data_port</td><td>int</td><td>the port used by felix-core to receive the subscription for the data (12350)</td></tr>
  <tr><td>enabled</td><td>int</td><td>1 to mark you want to use that front-end</td></tr>
</table>


<h2 id="connectivity" class="SUBTITLE">Available scans</h2>

<h3>Digital scan</h3>
A DigitalScan injects a signal in the digital circuit of the pixel, and checks for the corresponding hit in the data. Because the output data bandwidth is limited, the injection is done in a few pixels at a time. The analysis of the digital scan checks that every single pixel has the required number of hits. If there is a discrepancy in the number of hits in a given set of pixels, it is possible to generate an enable mask to ignore those pixels in subsequent scans. The digital injection pulse is ORed with the comparator output. Therefore, the analog state must be below threshold, or the discriminator must be off (bias current set to zero) for digital injection to work. This is accomplished by setting the threshold to a high value and enabling digital injection.

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s Digital
</pre>

<h3>Analog scan</h3>
An AnalogScan injects a signal in the analog circuit of the pixel, and checks for the corresponding hit in the data, according to whether the signal is above threshold or not. Because the output data bandwidth is limited, the injection is done in a few pixels at a time. The analysis of the analog scan checks that every single pixel has the required number of hits. It is thus crucial to set the injection signal well above the global threshold. If there is a discrepancy in the number of hits in a given set of pixels, it is possible to generate an enable mask to ignore those pixels in subsequent scans.

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s Analog
</pre>


<h3>Threshold scan</h3>
A ThresholdScan measures the threshold in each pixel by varying the injected analog charge and looking at the turning point of the s-curve. Because the output data bandwidth is limited, the injection is done in a few pixels at a time.

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s ThrScan
</pre>

<h3>TOT scan</h3>
A ToTScan injects a signal in the analog circuit of the pixel, checks for the corresponding hit in the data and computes the corresponding ToT. It also computes the sigma ToT map. The analog injection is performed as in the AnalogScan. The ToT is computed using 4 bits.

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s TOT 
</pre>

<h3>Threshold tune</h3>
A Threshold tuning identifies the value of the global threshold DACs that generates the desired threshold in each frontend. The tuning takes as input the target threshold, which corresponds to the injection charge. The tuning consists of a loop of analog scans, performed with different values of the global threshold DACs. At the end of the loop, the value that allows to obtain the desired threshold will be saved in the configuration file

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s ThresholdTune 
</pre>


<h3>PreampTune</h3>
A Preamplifier tuning identifies the values of the parameters IBIAS_KRUM_SYNC/KRUM_CURR_LIN/VFF_DIFF which allow to obtain the desired ToT. These parameters determine the value of the current which discharges the feedback capacitors in each frontend. The tuning takes as input the value of the charge to be injected and the ToT target.
The tuning consists of a loop of ToT scans, performed with different values of IBIAS_KRUM_SYNC/KRUM_CURR_LIN/VFF_DIFF. At the end of the loop, the values of the parameters which allow to obtain the desired ToT will be saved in the configuration file.

<pre>
  source <?=$HOME.$ITKSW;?>/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s PreampTune 
</pre>


</div>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
