<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>Multi-Module setup in SR1</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">Multi-Module setup in SR1</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="Introduction" class="SUBTITLE">Introduction</h2>

Independent multi-module test bench for DAQ firmware/software tests and development of commissioning tools.
Remotely accessible by experts to be able to contribute in the different DAQ areas.
Used trunk cable named itk pixel system test readout 1 that goes to the rack (realistic length).
Fibers already had LC connectors.
Felix server flx01 inside the SR1 rack room.

	<div>
    <img class="IMAGEW600" src="images/rd53a-multi-module-setup-sr1-diagram.png"/>
  </div>
  <div>
    <img class="IMAGEW600" src="images/rd53a-multi-module-setup-sr1-picture.png"/>
  </div>

 

<h2 id="DP_to_pigtail_data" class="SUBTITLE">DP to pigtail data</h2>
  <p>Add description here.
    <a href="https://gitlab.cern.ch/itk-pixel-hybrid/rd53a_testboard_japan">Link to repository</a>
  </p>


 <img class="SMALLIMAGE ZOOM" src="images/rd53a-DP-to-data-pigtail.jpg"/>


<h2 id="DP_to_pigtail_power" class="SUBTITLE">DP to pigtail power</h2>
  <p>Add description here.
    <a href="https://gitlab.cern.ch/itk-pixel-hybrid/rd53a_power_testboard_japan">Link to repository</a>
  </p>


 <img class="SMALLIMAGE ZOOM" src="images/rd53a-molex-to-power-pigtail.jpg"/>

<h2 id="Pigtails" class="SUBTITLE">Pigtails</h2>

<div> 
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-data-pigtail.jpg"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-power-pigtail.jpg"/>
</div>

<a href="https://gitlab.cern.ch/itk-pixel-hybrid/rd53a_datapigtail_liverpool">Link to repository</a>

<h2 id="DP_to_SMA" class="SUBTITLE">DP to SMA adapter board</h2>
  
  <p>Add description here</p>

	<div>
    <img class="SMALLIMAGE ZOOM" src="images/DP_to_SMA_Bonn_front.jpg"/>
	  <img class="SMALLIMAGE ZOOM" src="images/DP_to_SMA_Bonn_back.jpg"/>
  </div>
  
  
  Mapping of the DP to front-end
  
  <table>
    <tr><td>DP adapter [L0-L3]</td><td>Quad [1-4]</td></tr>
    <tr><td>L0</td><td>4</td></tr>
    <tr><td>L1</td><td>3</td></tr>
    <tr><td>L2</td><td>2</td></tr>
    <tr><td>L3</td><td>1</td></tr>
  </table>

<h2 id="SMA_to_ERF" class="SUBTITLE">SMA to ERF (Opto)</h2>

  <p>
    <a href="https://gitlab.cern.ch/itk-pixel-electronics/services/optosystem/-/tree/master/G6341B_OptoBoard_V2_SMA_ERF_adapter">Link to repository</a>
  </p>
    
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-SMA_to_ERF.jpg"/>


<h2 id="config_opto" class="SUBTITLE">Config Optoboard</h2>

Checkout slim_config from optoboard_felix package 
<ol>
  <li>git checkout slim_config</li> 
  <li>git clone ssh://git@gitlab.cern.ch:7999/bat/optoboard_felix.git</li>
</ol>

Configure the opto_board: Remember to change N to the physical felix fiber link
<ol>
  <li>cd optoboard_felix</li>
  <li>python quick_start.py -v 1.2 -s 524d0002 -G N -i 0 -d 0 </li>
</ol>



<h2 id="Power_control" class="SUBTITLE">Powering up the setup</h2>
<p>
  Power to the optoboard and the quads is controlled by the PCATLITKUSBPIX1. 
</p>
<ol>
  <li>Login to PCATLITKUSB1
    <pre>ssh -X itkpix@PCATLITKUSBPIX1</pre>
  </li>
  <li>Run the power supply control GUI 
    <pre>
      . /home/itkpix/RunDCS.sh
    </pre>
  </li>
  <li>A small GUI should pop-up. Each line corresponds to a different power supply. Two voltages control the opto. One controls the quads.
    Click on "read" to read the actual values. Click on "write" to send the new values of Vset and Ilim to the power supplies. 
    Click on "output" to enable or disable the output power.
    <br/>
    <img class="MEDIUMIMAGE ZOOM" src="images/rd53a-multi-module-setup-sr1-psu-control-gui.png"/>
  </li>
</ol>

<h2 id="felixcore" class="SUBTITLE">Running felixcore</h2>

Felixcore is an application provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIO protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIO.

<ol>
  <li>Login to flx01 as itkpix
    <pre>ssh itkpix@pcatlitkflx01</pre>
    </li>
  <li>source the felix-sw setup
    <pre>source /home/itkpix/felix-sw/felix-5.0-latest/setup.sh</pre>
  </li>
    <pre>x86_64-centos7-gcc11-opt/felixcore/felixcore -d 0 --data-interface lo --elinks 0,4,8,12</pre>
  <li>Wait for the following message before moving to the next step 
    <pre>"FelixCore Up and Running..."</pre>
  </li>
</ol>
 
<h2 id="itk-felix-sw" class="SUBTITLE">ITK FELIX SW</h2>

ITK FELIX SW is installed in the felix servers. ITK FELIX SW is a collection of ITK specific tools for FELIX.
More details can be found in the following <a href="https://ade-pixel-group.web.cern.ch/itk-felix-sw/html/">link</a>

<ol>
  <li>Login to flx01 as itkpix
    <pre>ssh itkpix@pcatlitkflx01</pre>
    </li>
  <li>source the itk-felix-sw setup
    <pre>source /home/itkpix/itk-felix-sw/setup.sh</pre>
  </li>
  <li>To compile go to the build directory invoke cmake
    <pre>cd /home/itkpix/itk-felix-sw/build</pre>
  </li>
  <li>Config the release for building 
    <pre>cmake ..</pre>
  </li>
    <li>Compile and install the software
    <pre>make -j install</pre>
  </li>  
</ol>


<h2 id="scan_manager" class="SUBTITLE">Running scan_manager</h2>
  
<pre>
  source /home/itkpix/itk-felix-sw/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s [scan-name] 
</pre>


<h2 id="connectivity" class="SUBTITLE">Connectivity</h2>

<pre style="font-size:xx-small;">
{
  "connectivity" : [
    {"name": "QUAD11", "config" : "quad_1.json", "rx" :  0, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD12", "config" : "quad_2.json", "rx" :  4, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD13", "config" : "quad_3.json", "rx" : 12, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD14", "config" : "quad_4.json", "rx" : 16, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD21", "config" : "quad_1.json", "rx" : 64, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD22", "config" : "quad_2.json", "rx" : 68, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD23", "config" : "quad_3.json", "rx" : 76, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD24", "config" : "quad_4.json", "rx" : 80, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD31", "config" : "quad_1.json", "rx" :128, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD32", "config" : "quad_2.json", "rx" :132, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD33", "config" : "quad_3.json", "rx" :140, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD34", "config" : "quad_4.json", "rx" :144, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD41", "config" : "quad_1.json", "rx" :192, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD42", "config" : "quad_2.json", "rx" :196, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD43", "config" : "quad_3.json", "rx" :204, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD44", "config" : "quad_4.json", "rx" :208, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD51", "config" : "quad_1.json", "rx" :256, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD52", "config" : "quad_2.json", "rx" :260, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD53", "config" : "quad_3.json", "rx" :268, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD54", "config" : "quad_4.json", "rx" :272, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD61", "config" : "quad_1.json", "rx" :320, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD62", "config" : "quad_2.json", "rx" :324, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD63", "config" : "quad_3.json", "rx" :332, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD64", "config" : "quad_4.json", "rx" :336, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD71", "config" : "quad_1.json", "rx" :384, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD72", "config" : "quad_2.json", "rx" :388, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD73", "config" : "quad_3.json", "rx" :396, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD74", "config" : "quad_4.json", "rx" :400, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD81", "config" : "quad_1.json", "rx" :448, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD82", "config" : "quad_2.json", "rx" :452, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD83", "config" : "quad_3.json", "rx" :460, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD84", "config" : "quad_4.json", "rx" :464, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0}
    ]
}
</pre>

</div>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
