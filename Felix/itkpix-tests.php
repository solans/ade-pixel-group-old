<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>RD53A FELIX read-out documentation</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">FELIX tests with ITKPix: UNDER CONSTRUCTION</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="introduction" class="SUBTITLE">Introduction</h2>
This page describes the tools and steps requried to read-out RD53A with FELIX Phase-I hardware.
Distribution of FELIX Phase-I servers (Supermicro X10SRW-F) and cards (FLX-712) is responsibility of the <a href="mailto:atlas-itk-common-electronics-coordinators@NOSPAM">ATLAS ITK Common Electronics coordinators</a>.
Background information about FELIX can be found in the FELIX <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/">manual</a>.

<h2 id="requirements" class="SUBTITLE">Requirements</h2>
<ol>
  <li>Xilinx Vivado (to program through JTAG)</li>
  <li>FELIX Phase-I server (Supermicro X10SRW-F)</li>
  <li>FELIX Phase-I card (FLX-712)</li>
  <li>FELIX drivers (<a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>)</li>
  <li>FELIX software (<a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/4.x/">link</a> <a href="https://gitlab.cern.ch/atlas-tdaq-felix/software/-/blob/master/README.md">readme</a>)</li>
  <li>FELIX firmware (<a href="rd53a-firmware.php">link</a>)</li>
  <li>LPGBT card (VLDB+ or Optoboard)</li>
  <li>LPGBT programmer (PiGBT for VLDB+ or Optoboard GUI for Optoboard)</li>
  <li>Fibers and adapter boards (<a href="https://ade-pixel-group.web.cern.ch/Felix/rd53a-setup.php">link</a>)</li>
  <li>ITK FELIX SW (<a href="https://gitlab.cern.ch/itk-felix-sw">link</a>)</li>
</ol>


<h2 id="setup_at_cern" class="SUBTITLE">Setup at CERN</h2>
<?
$HOME="/home/itkpix";
$FELIXSW=$HOME."/felix-sw/felix-ITKPix";
$FELIXDR=$HOME."/felix-dw";
$FELIXFW=$HOME."/felix-fw";
$ITKSW=$HOME."/itk-felix-sw";
$TDAQ_VERSION="9.4.0";
$FLX_FW_VERSION="FLX712_PIXEL_24CH_2022-03-14";
$FLX_SW_VERSION="4.2.0 RM5";
$FLX_DR_VERSION="4.9";
$FLX_DR_FILE="tdaq_sw_for_Flx-4.9.0-2dkms.noarch.rpm";
$BINARY_TAG="x86_64-centos7-gcc11-opt";
$ITK_FLX_SW_VERSION="master";
?>

<p>
Currently the FLX-712 is set-up at pcatlidros02 with a remote DCS setup.
It's composed of:
<ul>
<li> FELIX Phase-I server: pcatlidros02</li>
<li> PiGBT to configure LpGBT: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a></li>
<li> LpGBT Board (Powered by DCS)</li>
<li> RD53A (Powered by DCS)</li>
<li> Felix software: <?=$FELIXSW;?> </li>
<li> Felix driver:  <?=$FELIXDR;?> </li>
<li> Felix firmware:  <?=$FELIXFW;?> </li>
<li> Custom ITK sotware:  <?=$ITKSW;?> </li>
</ul>

<p>See more details of the setup at CERN in the following <a href="rd53a-setup.php">link</a></p>


<h2 id="firmware_versions" class="SUBTITLE">Firmware versions</h2>
<p>
Recommended firmware versions:
<ul>
    <li><a href="https://ade-pixel-group.web.cern.ch/Felix/rd53a-firmware.php?path=FLX712_PIXEL_24CH_2022-02-23">FLX712_PIXEL_24CH_2022-02-23</a></li>
    <li><a href="https://ade-pixel-group.web.cern.ch/Felix/rd53a-firmware.php?path=FLX712_PIXEL_4CH_2022-03-14">FLX712_PIXEL_4CH_2022-03-14</a></li>
</ul>
</p>

<h2 id="program_felix" class="SUBTITLE">How to program FELIX through PCI</h2>

<ol>
  <li>Download the desired FELIX firmware</li>
  <li>Setup the FELIX-SW
    <pre>
      source <?=$FELIXSW;?>/setup.sh 
    </pre>
  </li>
  <li>Program the card through PCI
    <pre>
      fflashprog -f3 [path_to_mcs_file] prog
    </pre>
  </li>
</ol>

<h2 id="program_felix712" class="SUBTITLE">How to program FELIX through JTAG</h2>

<ol>
  <li>Open Vivado 2020
    <pre>
export XILINXD_LICENSE_FILE="2112@lxlicen01.cern.ch,2112@lxlicen02.cern.ch,2112@lxlicen03.cern.ch"
source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2020.1/settings64.sh
vivado
    </pre>
  </li>
  <li>Open the hardware manager</li>
  <li>Connect to the existing device. If no device exists, try installing cable drivers, and flx-712 drivers</li>
  <li>Program device with <b>.bit</b> file and <b>.ltx</b> debug file provided in the link above.</li>
  <li>Restart the PC.</li>
</ol>


<h2 id="check_felix" class="SUBTITLE">Load and check FELIX drivers</h2>

<ol>
	<li>Login as root (superuser) to the FELIX server</li>
  <li>Download the recomended Felix driver (<?=$FLX_DR_FILE;?>) from the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>.</li>
	<li>Check the installed felix driver
		<pre>rpm -qa | grep tdaq</pre>
	</li>
	<li>Uninstall the previous driver
		<pre>rpm -e [filename given by previous command]</pre>
	</li>
	<li>Install a new driver: 
		<pre>yum install <?=$FLX_DR_FILE;?></pre>
	</li>
	<li>Start the newly installed drivers:
		<pre>./etc/init.d/drivers_flx start</pre>
	</li>
</ol>

<h2 id="compile-felix-sw" class="SUBTITLE">Compile the FELIX SW</h2>

<ol>
<li>Checkout the atlas-tdaq-felix software package of atlas-tdaq-felix
<pre>git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-felix/felix-distribution.git <?=$FELIXSW;?></pre>
</li>

<li>Change into the software directory
<pre>cd <?=$FELIXSW;?></pre>
</li>

<li>Initialize and update the submodules
<pre>
  git submodule init
  git submodule update
</pre>
</li>

<li>Activate and setup the environment
<pre>
source python_env/bin/activate
export REGMAP_VERSION=0x0500
source cmake_tdaq/bin/setup.sh <?=$BINARY_TAG;?>
</pre>

<li>Prepare the release for building
<pre>cmake_config <?=$BINARY_TAG;?></pre>
</li>

<li>Change to the build directory
<pre>cd <?=$BINARY_TAG;?></pre>
</li>

<li>Compile the software
<pre>make -j</pre>
</li>

</ol>


<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card</h2>

<ol>
  <li>Install the recommended FELIX driver</li>
  <li>Flash the recommended FELIX firmware</li>
  <li>Setup the recommended FELIX software</li>
  <li>Initialize the FELIX card to synchronize with the LPGBT 
    <pre>
      source <?=$FELIXSW;?>/setup.sh
      flx-init -c X
    </pre>
  </li>
</ol>
  
<h2 id="config-elink" class="SUBTITLE">Configure the FELIX elinks</h2>
Each optical (LPGBT) link can be divided into many logical elinks. These have to be configured in order to be recognized by the felix-sw. The e-link configuration is done with <b>elinkconfig</b> that is a tool provided by the felix-sw.

<ol>
	<li>Run <b>elinkconfig</b>
	<pre>
source <?=$FELIXSW;?>/setup.sh
elinkconfig
	</pre>	
	</li>
	<li>Click on <b>Open...</b><br/>
	</li>
	<li>Select the <b>.yelc</b> file<br/>
	</li>
	<li>Click on <b>Generate/Upload</b> <br/>
	</li>
	<li>Click on <b>Upload</b> <br/>
	</li>
</ol>
<div>	
	<img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-1.png"/>
	<img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-2.png"/>
	<img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-3.png"/>
</div>
<div>	
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-4.png"/>
	<img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-5.png"/>
	<img class="SMALLIMAGE ZOOM" src="images/rd53a-elinkconfig-6.png"/>
</div>
<p>
In addition you need to disable the emulators.
</p>
<p>
Then run rest of the commands. 
</p>
<pre>
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=0x3
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x1
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=0x4
flx-config -d Y set DECODING_REVERSE_10B=0x1
</pre>

<h2 id="config-lpgbt-pigbt" class="SUBTITLE">Configure LpGBT with PiGBT (recommended for VLDB+)</h2>
<p>
After powering the LpGBT (Check DCS section for more details), star a browser connected to CERN network and go to the following url: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a> 

Then select Real LpGBT and connect to the hardware.
The following setting are used: 
<ul>
<li> TRX
<li> TXdata rate = 10 Gbps
<li> TX encoding = FEC5
<li> Uplink EPRX (Click gear symbol) 
<ul>
<li> For EPRX in 0,1,2,3 
<li> Data Rate = 1280
<li> Track Mode = Continues
<li> Control TERM
<li> Control INV
<li> Equalization OFF

<li> EPRX1<li>1-6
<li> Data Rate off
</ul>		
<li> Downlink EPTX
<ul>
<li> Data Rate 160 Mpbs
<li> Drive strength 4.0 mA
</ul>		 		  
<li> click on the menu -> high speed
<ul>
<li> Invert high speed data output enabled (Invert polarity uplinks)
</ul>
</ul>
</p>


<h2 id="config-lpgbt-fice" class="SUBTITLE">Configure LpGBT through the optical link (recommended for Optoboard)</h2>

Alternatively, you can configure the LPGBT through the optical link.
This requires that the LPGBT has been initialized to communicate through the IC bus.
This will be the default configuration mode during production in SR1, and operation in the detector.

To configure a single register with fice you can use the following command:
<pre>
fice -G 0 -I 70 -a 0x1c5
</pre>
If you are dealing with opto-board V2 there needs to be 2 modifications.
<ul>
<li>Change the channel to 70 -> 74
<li>The lane pollarity needs to be changed: fgpolarity -r set
<li>The FEC5 sohuld be changed to FEC12:  flx-config set GBT_DATA_RXFORMAT2=0x1 
</ul>


Important not here is that, to acceses the different channels in the fice configuration you need to do the following configuration:
<ul>
<li> Channel 0: fice -G 0 -i 0 -d 0 # This is identical to fice -G 0
<li> Channel 1: fice -G 1 -i 1 -d 0 # 
<li> Channel 2: fice -G 0 -i 0 -d 1 # 
<li> Channel 3: fice -G 1 -i 1 -d 1 # 
</ul>


<h2 id="config-lpgbt-optotool" class="SUBTITLE">Configure LpGBT with Optoboard FELIX</h2>

Checkout slim_config from optoboard_felix package 
<ol>
  <li>git checkout slim_config</li> 
  <li>git clone ssh://git@gitlab.cern.ch:7999/bat/optoboard_felix.git</li>
</ol>

Configure the opto_board: Remember to change N to the physical felix fiber link
<ol>
  <li>cd optoboard_felix</li>
  <li>python quick_start.py -v 1.2 -s 524d0002 -G N -i 0 -d 0 </li>
</ol>




<h2 id="Power_control" class="SUBTITLE">Powering up the setup</h2>
To power LpGBT and RD53A you can use the DCS functionality in pcatlidros02. 
Fan needs to be always on. To use it:
</p>
<pre>
source <?=$ITKSW;?>/setup.sh
PSUControlGUI.py -f <?=$ITKSW;?>/PySerialComm/CRCard/FLX712.card
</pre>

<img class="MEDIUMIMAGE" src="images/itkpix-psu-control-gui.png"/>

<h2 id="aurora_alignment" class="SUBTITLE">Checking aurora alignment</h2>
We can check if the decoding of each RD53A front-end is running through the FELIX registers.

<ol>
    <li>List the FELIX registers
        <pre>flx-config list | grep DECODING_LINK_ALIGNED</pre>
    </li>
    <li>Only the front-ends being decoded in each link will show as high bit
<pre style="font-size:xx-small;">
0x2180 [R  57:00]                   DECODING_LINK_ALIGNED_00   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2190 [R  57:00]                   DECODING_LINK_ALIGNED_01   0x000000030000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21a0 [R  57:00]                   DECODING_LINK_ALIGNED_02   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21b0 [R  57:00]                   DECODING_LINK_ALIGNED_03   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21c0 [R  57:00]                   DECODING_LINK_ALIGNED_04   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21d0 [R  57:00]                   DECODING_LINK_ALIGNED_05   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21e0 [R  57:00]                   DECODING_LINK_ALIGNED_06   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21f0 [R  57:00]                   DECODING_LINK_ALIGNED_07   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2200 [R  57:00]                   DECODING_LINK_ALIGNED_08   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2210 [R  57:00]                   DECODING_LINK_ALIGNED_09   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2220 [R  57:00]                   DECODING_LINK_ALIGNED_10   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2230 [R  57:00]                   DECODING_LINK_ALIGNED_11   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2240 [R  57:00]                   DECODING_LINK_ALIGNED_12   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2250 [R  57:00]                   DECODING_LINK_ALIGNED_13   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2260 [R  57:00]                   DECODING_LINK_ALIGNED_14   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2270 [R  57:00]                   DECODING_LINK_ALIGNED_15   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2280 [R  57:00]                   DECODING_LINK_ALIGNED_16   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2290 [R  57:00]                   DECODING_LINK_ALIGNED_17   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22a0 [R  57:00]                   DECODING_LINK_ALIGNED_18   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22b0 [R  57:00]                   DECODING_LINK_ALIGNED_19   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22c0 [R  57:00]                   DECODING_LINK_ALIGNED_20   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22d0 [R  57:00]                   DECODING_LINK_ALIGNED_21   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22e0 [R  57:00]                   DECODING_LINK_ALIGNED_22   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22f0 [R  57:00]                   DECODING_LINK_ALIGNED_23   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
</pre>
</li>
</ol>




<h2 id="felixcore" class="SUBTITLE">Running felixcore</h2>

Felixcore is an application provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIO protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIO.

<ol>
    <li>Setup the felixsw
        <pre>source <?=$FELIXSW;?>/setup.sh</pre>
    </li>
    <li> Start felixcore
        <pre>x86_64-centos7-gcc8-opt/felixcore/felixcore -d 0 --data-interface lo --elinks 0,4,8,12    </pre>
    </li>
    <li>Wait until the message "Felixcore is up and running" appears on the screen</li>
</ol>

<h2 id="itk-felix-sw" class="SUBTITLE">Compile the ITK FELIX SW</h2>

ITK FELIX SW is a collection of ITK specific tools for FELIX.
More details can be found in the following <a href="https://ade-pixel-group.web.cern.ch/itk-felix-sw/html/">link</a>

<ol>
  <li>Login to flx01 as itkpix
    <pre>ssh itkpix@pcatlitkflx01</pre>
    </li>
  <li>source the itk-felix-sw setup
    <pre>source /home/itkpix/itk-felix-sw/setup.sh</pre>
  </li>
  <li>To compile go to the build directory invoke cmake
    <pre>cd /home/itkpix/itk-felix-sw/build</pre>
  </li>
  <li>Config the release for building 
    <pre>cmake ..</pre>
  </li>
    <li>Compile and install the software
    <pre>make -j install</pre>
  </li>  
</ol>



<h2 id="scan_manager" class="SUBTITLE">Running scan_manager</h2>
  
<pre>
  source /home/itkpix/itk-felix-sw/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s [scan-name] 
</pre>



<h2 id="connectivity" class="SUBTITLE">Connectivity file</h2>

Each line of the file needs the following:

<table>
  <tr><td>Attribute</td><td>Type</td><td>Description</td></tr>
  <tr><td>name</td><td>string</td><td>Front-end identifier name (rd53a_1, A_BM_01_1, Quad13_1...)</td></tr>
  <tr><td>config</td><td>string</td><td>JSON configuration file:  (default_rd53a.json should be fine)</td></tr>
  <tr><td>rx</td><td>int</td><td>is the data e-link (we can keep it at 0)</td></tr>
  <tr><td>tx</td><td>int</td><td>is the command e-link (we can keep it at 0)</td></tr>
  <tr><td>host</td><td>string</td><td>"127.0.0.1" if you run  scan_manager_rd53 on the same FELIX server</td></tr>
  <tr><td>cmd_port</td><td>int</td><td>the port used by felix-core to receive the commands (12340)</td></tr>
  <tr><td>data_port</td><td>int</td><td>the port used by felix-core to receive the subscription for the data (12350)</td></tr>
  <tr><td>enabled</td><td>int</td><td>1 to mark you want to use that front-end</td></tr>
</table>

<pre>
{
  "connectivity" : [
    {"name": "rd53a_1", 
     "config" : "default_rd53a.json", 
     "rx" :  0, 
     "tx" :  0, 
     "host": "127.0.0.1", 
     "cmd_port": 12340, 
     "data_port": 12350, 
     "enable" : 1, 
     "locked" : 0
    }    
  ]
}
</pre>

<h2 id="config_changes" class="SUBTITLE">Configuration file differences with respect to YARR</h2>

<table>
    <tr><td>Register name</td><td>YARR</td><td>FELIX</td></tr>
    <tr><td>CdrSelSerClk</td><td>1</td><td>0</td></tr>
    <tr><td>OutputActiveLanes</td><td>3</td><td>1</td></tr>
    <tr><td>SerSelOut1</td><td>1</td><td>3</td></tr>
    <tr><td>SerSelOut2</td><td>1</td><td>3</td></tr>
    <tr><td>SerSelOut3</td><td>1</td><td>3</td></tr>
</table>


</div>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
