<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>FELIX tests with Calypso BCMprime</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">FELIX tests with Calypso</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="disclaimer" class="SUBTITLE">Disclaimer</h2>
This page describes the tests done at CERN with FELIX and the Calypso ASIC from BCMPrime.
This page aims to be the documentation of such tests.

<p>Please refer to the FELIX manual in the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/">link</a></p>

<h2 id="requirements" class="SUBTITLE">Requirements</h2>
<ol>
  <li>Xilinx Vivado (to program through JTAG)</li>
  <li>FELIX Phase-I server (Supermicro X10SRW-F)</li>
  <li>FELIX Phase-I card (FLX-712)</li>
  <li>FELIX driver (4.9.0)</li>
  <li>FELIX software (felix-04-02-01-rm5-stand-alone centos7 gcc8)</li>
  <li>FELIX firmware (Phase2 LPGBT direct mode 21/12/22)</li>
  <li>LPGBT card (VLDB+)</li>
  <li>LPGBT programmer (PiGBT)</li>
  <li>Custom BCMP software</li>
</ol>

<h2 id="setup_at_cern" class="SUBTITLE">Setup at CERN</h2>
<p>
The FELIX setup at CERN is composed of:
<ul>
<li> FELIX Phase-I server: pcatlidros04</li>
<li> FELIX Phase-I card: <a href="https://ade-pixel-group.web.cern.ch/felix-db/card.php?uid=99">&num;99</a></li>
<li> PiGBT to configure LpGBT: <a href="ep-ade-pigbt-02:8080">ep-ade-pigbt-02:8080</a></li>
<li> LpGBT Board </li>
<li> Calypso </li>
<li> Pulser </li>
<li> Felix software: /home/sbmuser/felix-sw </li>
<li> Felix driver: /home/sbmuser/felix-dr </li>
<li> Felix firmware: /home/sbmuser/felix-sw </li>
<li> Custom BCMP sotware: /home/sbmuser/itk-felix-sw </li>
</ul>


<h2 id="firmware_versions" class="SUBTITLE">Firmware download</h2>
<p>
List of fimrware versions can be found in <a href="bcmp-firmware.php"> 
  https://ade-pixel-group.web.cern.ch/Felix/bcmp-firmware.php
</a>
<p>
Recommended firmware is <a href="https://ade-pixel-group.web.cern.ch/Felix/bcmp-firmware.php?path=FLX712_LPGBT_4CH_CLKSELECT_GIT_phase2-FLX-1768_LPGBT_DirectMode_rm-5.0_2105_211222_14_46">Phase2 LPGBT direct mode 21/12/22</a>

<h2 id="check_felix" class="SUBTITLE">Install FELIX drivers</h2>

<ol>
	<li>Download as superuser the lastest Felix driver from the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>.</li>
	<li>Check the installed felix driver
		<pre>rpm -qa | grep tdaq</pre>
	</li>
	<li>Uninstall the previous driver
		<pre>rpm -e [filename given by previous command]</pre>
	</li>
	<li>Install a new driver: 
		<pre>yum install [new_downloaded_rpm_file]</pre>
	</li>
	<li>Start the newly installed drivers:
		<pre>./etc/init.d/drivers_flx start</pre>
	</li>
</ol>

<h2 id="program_felix" class="SUBTITLE">Program FELIX firmware</h2>

<pre>
source /home/sbmuser/felix-sw/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc8-opt/setup.sh
fflashprog -f0 [path_to_mcs_file] prog
fflashprog -f1 [path_to_mcs_file] prog
fflashprog -f2 [path_to_mcs_file] prog
fflashprog -f3 [path_to_mcs_file] prog
</pre>
  
<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card</h2>

<pre>
source /home/sbmuser/felix-sw/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc8-opt/setup.sh
flx-init
</pre>

<h2 id="config-elink" class="SUBTITLE">Configure the FELIX elinks</h2>
<pre>
flx-config DECODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=0x11110000
flx-config DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=0x4
</pre>
<pre>
flx-config DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA 15
flx-config DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH 4
</pre>

<h2 id="config-lpgbt-pigbt" class="SUBTITLE">Configure LpGBT with PiGBT</h2>
<p>
Open the following link from within the CERN network: <a href="http://ep-ade-pigbt-02:8080">ep-ade-pigbt-02:8080</a> 
Then select Real LpGBT and connect to the hardware.
The following setting are used: 
<ul>
<li> TRX
<li> TXdata rate = 10 Gbps
<li> TX encoding = FEC5
<li> Uplink EPRX (Click gear symbol) 
<ul>
<li> EPRX0 
<li> Data Rate = 1280
<li> Track Mode = Continues
<li> Control TERM
<li> Equalization OFF

<li> EPRX1<li>1-6
<li> Data Rate off
</ul>		
<li> Downlink EPTX
<ul>
<li> Data Rate 160 Mpbs
<li> Drive strength 4.0 mA
</ul>		 		  
<li> click on the menu -> high speed
<ul>
<li> Invert high speed data output enabled (Invert polarity uplinks)
</ul>
</ul>
</p>

<h2 id="daq" class="SUBTITLE">Get data directly from the FELIX card</h2>

<pre>
  source /home/sbmuser/felix-sw/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc8-opt/setup.sh
  fedump -e 0  
</pre>


<h2 id="felixcore" class="SUBTITLE">Running felixcore</h2>

Felixcore is an application provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIO protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIO.

In one terminal run:
<pre>
  source /home/sbmuser/felix-sw/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc8-opt/setup.sh
  felixcore -d 0 --data-interface lo --elinks 0  
</pre>

<h2 id="daq" class="SUBTITLE">Get data with netio</h2>

<pre>
  source /home/sbmuser/felix-sw/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc8-opt/setup.sh
  netio_cat subscribe -t 0 -e raw -H 127.0.0.1 -p 12350
</pre>
  
<h2 id="daq" class="SUBTITLE">Get data with ITK SW</h2>

<pre>
  source /home/sbmuser/itk-felix-sw/setup.sh
  test_bcmp_subscribe -e 0   
</pre>
  

<h2 id="compilefelixsw" class="SUBTITLE">Compile a new version of FELIX SW</h2>
  
In the example we compile release felix-04-02-01 for x86_64-centos7-gcc8-opt

<ol>
  <li>Clone the felix-distribution from atlas-tdaq-felix
    <pre> git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/felix-distribution.git felix-04-02-01 </pre>
  </li>
  <li>Change into the software directory
    <pre> cd felix-04-02-01</pre>
  </li>
  <li>Checkout the tags for release 04-02-01
    <pre> git checkout tags/felix-04-02-01</pre>
  </li>
  <li>Initialize and checkout the submodules (the actual felix sw packages) corresponding to that release
    <pre>git submodule init</pre>
    <pre>git submodule update</pre>
  </li>
  <li>Setup the environment
    <pre>source python_env/bin/activate</pre>
    <pre>source cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt</pre>
  </li>
  <li>Force the register map to 5.0
    <pre>export REGMAP_VERSION=0x0500</pre>
  </li>    
  <li>Prepare the release for building
    <pre> cmake_config x86_64-centos7-gcc8-opt</pre>
  </li>
  <li>Change to the build directory
    <pre>cd x86_64-centos7-gcc8-opt</pre>
  </li>
  <li>Compile the software
    <pre> make -j</pre>
  </li>
</ol>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
