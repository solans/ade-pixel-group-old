<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Lab24</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Lab24</p>

<p><img width="100%" src="Finished_Lab_20160412_small.jpg"></p>

Laboratory 161-1-024 is an ATLAS ITK module production installation and x-ray testing facility.
Phone number is 79772.
Access is controlled by dosimeter reader. Request access through 
<a href="https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:1120:13508100105021::NO:RP:P1120_PERMISSIONID:457">ADAMS</a>. 
Equipment present in the lab includes:

<ul>
  <li>3 USBPix setups</li>
  <li>1 MPI setup</li>
  <li>1 YARR setup</li>
  <li>1 microscope + 1 photocamera</li>
  <li>Climate chamber</li>
  <li>X-ray chamber</li>
  <li>Air dryer</li>
</ul>

<p><img class="IMAGE" src="Floorplan-20160420.png"><br></p>

<p class="SUBTITLE">X-ray machine</p>
<ul>
  <li><a href="../xray">Dedicated X-ray machine page</a></li>
</ul>

<p class="SUBTITLE">Radio Protection Safety</p>
<ul>
  <li><a href="RPLeaflet_SSF.pdf">RP Leaflet</a></li>
</ul>

<p class="SUBTITLE">Leica Microscope camera software</p>

<p>Leica microscope is equiped with a <a href="https://www.ximea.com">XIMEA</a> USB micro-camera model MQ013CG-E2.
XiCAM control software is available for Windows, Linux and Mac.</p>
<p><a href="https://www.ximea.com/support/wiki/allprod/XIMEA_CamTool">Link to Ximea webpage.</a>

<ul>
  <li><a href="XIMEA_API_Installer_Beta.exe">XIMEA API Installer Windows</a></li>
  <li><a href="XIMEA_Linux_SP.tgz">XIMEA XiCam for Linux</a></li>
  <li><a href="XIMEA_OSX_SP.dmg">XIMEA XiCam for Mac</a></li>
</ul>

<!--
<p class="SUBTITLE">Votsch VC2020 Climate chamber</p>

<ul>
  <li><a href="SM_VC_2020-2057_CTC_1995.11_60886926.pdf">Service manual</a></li>
</ul>
-->
<p class="SUBTITLE">Weiss Technik LabEvent T/110/70 EMC</p>

<ul>
    <li><a href="Weisstechnik/">Manual</a></li>
</ul>



<p class="SUBTITLE">Chillers</p>

<ul>
  <li><a href="JULABO_FP50-HE.pdf">Julabo FP50-HE manual</a></li>
  <li><a href="Compatible-Control_Ministat_Chiller_V3.7_EN.pdf">HUBER Intelligent Control manual</a></li>
  <li><a href="HUBER_IC012.pdf">HUBER Circulation Cooler IC012 manual</a></li>
  <li><a href="S276984_1001.0020.01_Unistat_705_gb.pdf">HUBER Unistat 705 manual</a></li>
</ul>

<p class="SUBTITLE">Ultra-air heatless adsorption dryer ultra.dry UDC-B</p>

<ul>
  <li><a href="UDC-B_-_manual_-_EN_-uf_-_R06_-_2019-04-01.pdf">Operating manual</a></li>
  <li><a href="UDC-B_-_ultra_matic_14_-_manual_-_EN_-_uf_-_R02_-_2019-04-01.pdf">Service manual</a></li>
  <li><a href="UDC-B_0005-0110_-_HeatLess_-_GB_-_Datenblatt_-_2018-01-01.pdf">Product data sheet</a></li>
</ul>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
