<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_POST;
$sql="";
if($d["cmd"]=="update_sample_comment"){
  $sql="UPDATE samples SET comment = ? WHERE id= ? ";
  $prep=$conn->prepare($sql);
  $prep->bind_param("si", $d["comment"],$d["uid"]);
}else if($d["cmd"]=="update_sample"){
  $sql="UPDATE samples SET wire_bonding_date = ?, wire_bonding_location = ?, board = ?, comment = ? WHERE id= ? ";
  $prep=$conn->prepare($sql);
  $prep->bind_param("ssssi", $d["wire_bonding_date"],$d["wire_bonding_location"],$d["board"],$d["comment"],$d["uid"]);
}else if($d["cmd"]=="add_trec_number"){
  $prep=$conn->prepare("INSERT INTO trec (sample_id,date,trec) VALUES (?,?,?)");
  $prep->bind_param("iss",$d["uid"],$d["date"],$d["trec"]);
}else if($d["cmd"]=="add_location"){
  $sql="INSERT INTO locations (sample_id,date,status,comment,location,edh) VALUES (?,?,?,?,?,?);";
  $prep=$conn->prepare($sql);
  $prep->bind_param("isssss",$d["uid"],$d["date"],$d["status"],$d["comment"],$d["location"],$d["edh"]);
}

echo $sql."\n";
print_r($d);

$prep->execute();
if ($prep->affected_rows > 0){echo "PASS";}else{echo "FAIL: ".mysqli_error($conn);}
$conn->close();		

?>