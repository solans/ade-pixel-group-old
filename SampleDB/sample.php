<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="sampledb.css" rel="stylesheet" type="text/css" />

<title>Sample Database</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
  //$dt1 = round(microtime(true) - $time_start,1);
?>
<div class="CONTENT">
<?php   
  show_certificate();
  show_login(true); 
  $dbOK=False;
  try {
    // Create connection
    $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
    // Check connection
    if ($conn->connect_error) {
        throw new Exception('Database error');
        die("Connection failed: " . $conn->connect_error);
        echo "Error connection to database";
    }else{
    
    $dbOK=True;
      if(isset($_GET['uid'])){
        $uid=intval($_GET['uid']);
      }  
      else if(isset($_GET['sample']) && isset($_GET['sensor']) && isset($_GET['submission'])){
        $sql = "SELECT id FROM samples";
        $sql.= " WHERE sample LIKE '".$_GET["sample"]."' ";
        $sql.= " AND sensor LIKE '".$_GET["sensor"]."' ";
        $sql.= " AND submission = '".$_GET["submission"]."' ";
        //echo $sql;
        $res=$conn->query($sql);
        if(!$res){echo $conn->error;}
        $row=$res->fetch_assoc();
        $uid=intval($row["id"]);
        //echo "UID:".$uid;
      }  
      else if(isset($_GET['trec'])){
        $res=$conn->query("SELECT sample_id FROM trec WHERE trec.trec='".$_GET["trec"]."'");
        $uid=intval($res->fetch_assoc()["sample_id"]);
        //echo "UID:".$uid; 
        if($uid==0){
            echo "<h2>TREC ID not found: ".$_GET["trec"]."</h2>";
            exit();        
        }
      }
      else{
        echo "<h2>Not enough parameters to find sample</h2>";
        exit();
      }
      $sample_sql = "SELECT * FROM samples WHERE samples.id='".$uid."'";
      $trec_sql = "SELECT date, trec  FROM trec WHERE sample_id = ".$uid;
      $loc_sql = "SELECT date, status, comment, location, edh FROM locations WHERE sample_id = '".$uid."' ORDER BY date DESC" ;
      $sample_result = $conn->query($sample_sql);
      //echo $sample_sql;
      //$conn->error;
      $trec_result = $conn->query($trec_sql);
      //echo $trec_sql;
      //$conn->error;
      $loc_result = $conn->query($loc_sql);
      //echo $loc_sql;
      //$conn->error;
    }
    $conn->close();
  }catch (Exception $e) {
      echo "<h2>Error connecting to database</h2>";//.$e->getMessage();
  }
  ?>
<p class="TITLE">Sample <?php echo $uid;?></p>
<?php
if ($dbOK==True){?>
    <form method="POST" id="update_sample">
    <input id="update_sample_id" value="<?=$uid;?>" type="hidden"/>
    <table class="border">
    <?php 
        $row = $sample_result->fetch_assoc();
          echo "<tr><th class='regform-done-caption'>Sample</th><td class=\"regform-done-title\"><input type='text' value='".$row["sample"]."' id='sample' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Sensor</th><td><input type='text' value='".$row["sensor"]."' id='sensor' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Submission</th><td><input type='text' value='".$row["submission"]."' id='submission' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Substrate</th><td><input type='text' value='".$row["substrate"]."' id='substrate' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Flavour</th><td><input type='text' value='".$row["flavour"]."' id='flavour' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Thickness</th><td><input type='text' value='".$row["thickness"]."' id='thickness' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Serial number</th><td><input type='text' value='".$row["serialnumber"]."' id='serialnumber' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Dicing</th><td><input type='text' value='".$row["dicing"]."' id='dicing' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Doping</th><td><input type='text' value='".$row["doping"]."' id='doping' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Comment</th><td><input type='text' value='".$row["comment"]."' id='comment' style='width: 400px;'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Dose</th><td><input type='text' value='".$row["dose"]."' id='dose' readonly='readonly'></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Board</th><td><input type='text' value='".$row["board"]."' id='board' ></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Wire-bonding date</th><td><input type='text' value='".$row["wire_bonding_date"]."' id='wire_bonding_date' ></td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Wire-bonding location</th><td><input type='text' value='".$row["wire_bonding_location"]."' id='wire_bonding_location' ></td></tr>";
    	
    
    ?>
    </table>
	<input type="submit" value="Save comments">
	<input type="reset" value="Reset" id="update_sample_reset">
	</form>

  <h3>TREC numbers</h3>
  <table class="border">
  <?php
    //var_dump($trec_result);
    if ($trec_result->num_rows > 0) {
        while($row = $trec_result->fetch_assoc()) {
    	    //echo "<tr><th class='regform-done-caption'>Date</th><td>".$row["date"]."</td></tr>";
    	    //echo "<tr><th class='regform-done-caption'>Trec</th><td>".$row["trec"]."</td></tr>";
    	    echo "<tr><th class='regform-done-caption'>Trec</th><td>".$row["trec"]."&nbsp;".$row["date"]."&nbsp;";
          echo "<a href='https://eamlight.cern.ch/asset/".$row["trec"]."'>eam</a></td></tr>";
    	}
    }
    ?>
  </table>
  
  <h3>Locations</h3>
  <table class="border">
  <?php
  if ($loc_result->num_rows > 0) {
    while($row = $loc_result->fetch_assoc()) {
  	  echo "<tr><th>&nbsp;</th><td>&nbsp;</td></tr>";
      echo "<tr><th class='regform-done-caption'>Location</th><td><input type='text' value='".$row["location"]."' readonly='readonly'></td></tr>";
		  echo "<tr><th class='regform-done-caption'>Location date</th><td><input type='text' value='".$row["date"]."' readonly='readonly'></td></tr>";
		  echo "<tr><th class='regform-done-caption'>Location status</th><td><input type='text' value='".$row["status"]."' readonly='readonly'></td></tr>";
		  echo "<tr><th class='regform-done-caption'>Location comment</th><td><input type='text' value='".$row["comment"]."' style='width: 400px; readonly='readonly'></td></tr>";
      echo "<tr><th class='regform-done-caption'>EDH</th><td><input type='text' value='".$row["edh"]."' readonly='readonly'></td></tr>";
    }
  }
  ?>
  </table>
  
  <h3>Add TREC number</h3>
  <form method="POST" id="add_trec_number">
    <input id="add_trec_number_sample_id" value="<?=$uid;?>" type="hidden"/>
    <table>
      <tr><td>TREC:</td><td><input id="add_trec_number_trec"/></td></tr>
      <tr><td>Date:</td><td><input id="add_trec_number_date"/></td></tr>
    </table>
    <input type="submit" value="Add new TREC number">
    <input type="reset" value="Reset" id="add_trec_number_reset">
  </form>
  
  <h3>Add Location</h3>
  <form method="POST" id="add_location">
    <input id="add_location_sample_id" value="<?=$uid;?>" type="hidden"/>
    <table>
      <tr><td>Location:</td><td><input id="add_location_location"/></td></tr>
      <tr><td>Date:</td><td><input id="add_location_date"/></td></tr>
      <tr><td>Status:</td><td><select id="add_location_status"><option selected value="OK">OK</option><option value="BROKEN">Broken</option></select></td></tr>
      <tr><td>Comment:</td><td><input id="add_location_comment"/></td></tr>
      <tr><td>EDH:</td><td><input id="add_location_edh"/></td></tr>
    </table>
    <input type="submit" value="Add new Location">
    <input type="reset" value="Reset" id="add_location_reset">
  </form>
  
    <?php 
}else{
    echo "Error connecting to database";
}

?>
<div id="db_response"></div>
</div>
</div>
<script>

var sample_uid="<?=$uid;?>";

$( function() {
  $( "#wire_bonding_date" ).datepicker({dateFormat:'yy-mm-dd'});
  $( "#add_trec_number_date" ).datepicker({dateFormat:'yy-mm-dd'});
  $( "#add_location_date" ).datepicker({dateFormat:'yy-mm-dd'});
} );

$('#update_sample').submit(function () {
  $("#db_response").text("");
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"update_sample",
           uid:$("#update_sample_id").val(),
           wire_bonding_date:$("#wire_bonding_date").val(),
           wire_bonding_location:$("#wire_bonding_location").val(),
           comment:$("#comment").val(),
           board:$("#board").val()
          },
    type: 'post',
    success: function(data) {
      console.log(data);        
		  if (data.includes("PASS")){console.log("reset form");$('#update_sample_reset').click();location.reload();}
		  else{$("#db_response").text("Error:"+data);}
    }
  });
  return false;
});

$('#add_trec_number').submit(function () {
  $("#db_response").text("");
  $.ajax({
	  url: 'dbfunctions.php',
    data: {cmd:"add_trec_number",
           uid:$("#add_trec_number_sample_id").val(),
           trec:$("#add_trec_number_trec").val(),
           date:$("#add_trec_number_date").val()
          },
    type: 'post',
    success: function(data) {
      console.log(data);        
		  if (data.includes("PASS")){console.log("reset form");$('#add_trec_number_reset').click();location.reload();}
		  else{$("#db_response").text("Error:"+data);}
    }
  });
  return false;
});
 
$('#add_location').submit(function () {
 $("#db_response").text("");
 $.ajax({
   url: 'dbfunctions.php',
   data: {cmd:"add_location",
          uid:$("#add_location_sample_id").val(),
          date:$("#add_location_date").val(),
          location:$("#add_location_location").val(),
          status:$("#add_location_status").val(),
          comment:$("#add_location_comment").val(),
          edh:$("#add_location_edh").val()
        },
   type: 'post',
   success: function(data) {
     console.log(data);        
 		 if (data.includes("PASS")){console.log("reset form");$('#add_location_reset').click();location.reload();}
 		 else{$("#db_response").text("Error: "+data);}
   }
 });
 return false;
});


</script>
<?php
  show_footer();
  // Display Script End time
?>
</body>
</html>

