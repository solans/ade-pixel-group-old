<table>
<tr><td>You can use basic filter operands such as:</td><td>For example, search for the sample: </td></tr>
<tr><td><pre>| or  OR , < <= >= >, *, ! or !=, etc</pre></td><td><pre>W**R2</pre></td></tr>
</table>

<span id="search_list">
	<input  hidden class="getsearch" type="search" data-column="1">
	<button onclick="export_tablesorter('search_table');">CSV</button> Export <span class="search_table_row_counter"></span> <button id="reset-link" style="float: right;">Reset filters</button>
	<div id="pager" class="pager">
	  <form>
	    <img src="../img/first.png" class="first"/>
	    <img src="../img/prev.png" class="prev"/>
	    <!-- the "pagedisplay" can be any element, including an input -->
	    <span class="pagedisplay" data-pager-output-filtered="{startRow:input} &ndash; {endRow} / {filteredRows} of {totalRows} total rows"></span>
	    <img src="../img/next.png" class="next"/>
	    <img src="../img/last.png" class="last"/>
	    <select class="pagesize">
	      <option value="10">10</option>
	      <option value="20">20</option>
	      <option value="30">30</option>
	      <option value="40">40</option>
	      <option value="all">All Rows</option>
	    </select>
	  </form>
	</div>
	<table id="samples_table" class="tablesorter" style="font-size: smaller">
		<thead>
			<th class="first-name filter-select" data-placeholder="Select...">Sensor</th>
			<th data-placeholder="Search...">Sample </th>
			<th class="first-name filter-select" data-placeholder="Select...">Submission</th>
			<th class="first-name filter-select" data-placeholder="Select...">Substrate</th>
			<th class="first-name filter-select" data-placeholder="Select...">Flavor</th>
			<th class="first-name filter-select" data-placeholder="Select...">Thickness</th>
			<th data-placeholder="Search...">Serial</th>
			<th class="first-name filter-select" data-placeholder="Select...">Dicing</th>
			<th class="first-name filter-select" data-placeholder="Select...">Doping</th>
			<th data-placeholder="Search...">Comment</th>
			<th class="first-name filter-select" data-placeholder="Select...">Wired</th>
			<th class="first-name filter-select" data-placeholder="Select...">Wiredat</th>
			<th class="first-name filter-select" data-placeholder="Select...">Dose</th>
		</thead>
		<tbody id="samples_body">
		</tbody>
	</table>
</span>
<script>
	var pager=true;
	var def_visible_rows=40;
	
  $(function() {
    load_samples();
  });

  function load_samples(){
    $.ajax({
      url: 'dbread.php',
      type: 'get',
      data: {cmd:"get_samples"},
      success: function(data) {
        console.log(data);
        rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
        $("#samples_body").empty();
        for (row of rows){
          tt ="<tr>";
          tt+="<td>"+row["sensor"]+"</td>";
          tt+="<td>"+row["sample"]+"<a target=\"_blank\" href=\"sample.php?uid="+row["id"]+"\"> <img title=\"Full description in new tab\" class=\"cursor newtabicon\" src=\"../img/newtab.png\"></a></td>";
          tt+="<td>"+row["submission"]+"</td>";
          tt+="<td>"+row["substrate"]+"</td>";
          tt+="<td>"+row["flavour"]+"</td>";
          tt+="<td>"+row["thickness"]+"</td>";
          tt+="<td>"+row["serialnumber"]+"</td>";
          tt+="<td>"+row["dicing"]+"</td>";
          tt+="<td>"+row["doping"]+"</td>";
          tt+="<td>"+row["comment"]+"</td>";
          tt+="<td>"+row["wire_bonding_date"]+"</td>";
          tt+="<td>"+row["wire_bonding_location"]+"</td>";
          tt+="<td>"+row["dose"]+"</td>";
          tt+="</tr>";
          $("#samples_body").append(tt);
        }
        $('#samples').trigger("update").trigger("appendCache").trigger("applyWidgets");
        $("#samples_found").html("Found: "+($("#samples tr:visible").length-2));
      	process_table2("samples", 0,0, pager);
        
      }
    });    
  }


</script>

