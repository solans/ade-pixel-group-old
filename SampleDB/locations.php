<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="sampledb.css" rel="stylesheet" type="text/css" />

<title>Sample Database</title>
<style>
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php   
  $dbOK=True;
  show_certificate();
  show_login(true); 
  $uid=0;
  try {
      if(isset($_GET['uid'])){
          // Create connection
          $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
          // Check connection
          if ($conn->connect_error) {
              throw new Exception('Database error');
              die("Connection failed: " . $conn->connect_error);
              echo "Error connection to database";
          }else{
              $sql_getid = "SELECT samples.id FROM samples INNER JOIN trec ON samples.id=trec.sample_id WHERE trec.trec='".$_GET['uid']."'";
              $res_getid = $conn->query($sql_getid);
              $sid=$res_getid->fetch_array()[0];
              echo "SAMPLE ID=".$sid;
              $qlocations = "SELECT locations.* FROM locations WHERE locations.sample_id='".$sid."' ORDER BY locations.date DESC;" ;
              $rlocations = $conn->query($qlocations);
              $conn->close();
          }
      }else{
          echo "No object";
      }
  }catch (Exception $e) {
      $dbOK=False;
      echo "<h2>Error connecting to database</h2>";//.$e->getMessage();
  }
  ?>
<p class="TITLE">Locations for sample <?php echo $_GET["uid"];?></p>
<?php
if ($dbOK==True){?>
    <form method="POST" id="edit_sample">
    <table class="border">
    <?php 
    echo "<tr>";
    echo "<td>Date</td>";
    echo "<td>Location</td>";
    echo "<td>EDH</td>";
    echo "<td>Comment</td>";
    echo "</tr>";
    if ($rlocations->num_rows > 0) {
        while($row = $rlocations->fetch_assoc()) {
          echo "<tr>";
          echo "<td>".$row["date"]."</td>";
          echo "<td>".$row["location"]."</td>";
          echo "<td>".$row["edh"]."</td>";
          echo "<td>".$row["comment"]."</td>";
          echo "</tr>";
        }
    }
    ?>
    </table>
	</form>
    <?php 
}else{
    echo "Error connecting to database";
}

?>
</div>
</div>
<?php
  show_footer();
  // Display Script End time
?>
</body>
</html>

