<?php
//echo "hello";
include_once("includes.php");

//echo "open";
$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}

$d=$_GET;

if($d["cmd"]=="get_samples"){
  $sql ="SELECT * FROM samples WHERE sensor LIKE '%MALTA%'";
}else if($d["cmd"]=="search"){
  $sql ="SELECT samples.* FROM samples ";
  $conds=array();
  //echo "array_key_exists("sample",$d):".array_key_exists("sample",$d);
  if(array_key_exists("sample",$d)){$conds[]="samples.`sample` LIKE '".$d["sample"]."'";}
  if(array_key_exists("trec",$d)){
    $sql ="SELECT samples.*,trec.trec FROM samples ";
    $sql .=" LEFT JOIN trec ON samples.id=trec.sample_id ";
    $conds[]="trec.`trec` LIKE '".$d["trec"]."'";
  }
  if(array_key_exists("submission",$d)){
    $submissions=array();
    foreach($d["submission"] as $submission){
      $submissions[]="samples.`submission`='".$submission."'"; 
    }
    $conds[]="(".join(" OR ",$submissions).")";
  }
  if(array_key_exists("sensor",$d)){
    $sensors=array();
    foreach($d["sensor"] as $sensor){
      $sensors[]="samples.`sensor`='".$sensor."'"; 
    }
    $conds[]="(".join(" OR ",$sensors).")";
  }
  if(count($conds)>0){$sql.="WHERE ".join(" AND ",$conds);}
}
  
echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>