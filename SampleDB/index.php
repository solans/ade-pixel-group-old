<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="<?=$gobase;?>JS/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?=$gobase;?>JS/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/jquery.tablesorter.pager.min.css" rel="stylesheet">
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-pager.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="sampledb.css" rel="stylesheet" type="text/css" />

<title>Sample Database</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php
show_login(true); 
?>
<p class="TITLE">Samples database</p>

<ul>
  <li><a href="?page=list">list</a></li>
  <li><a href="?page=search">search</a></li>
</ul>
<?php
if(@$_GET['page']=="list"){
  include("list.php");
}
else if(@$_GET['page']=="search"){
  include("search.php");
}
?>

</div>
<?php
  show_footer();
?>
</div>
</body>
</html>

