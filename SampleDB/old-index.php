<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/jquery.tablesorter.pager.min.css" rel="stylesheet">
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-pager.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="sampledb.css" rel="stylesheet" type="text/css" />

<title>Sample Database</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
  //$dt1 = round(microtime(true) - $time_start,1);
?>
<div class="CONTENT">
<?php
    $dbOK=True;
    show_certificate();
    show_login(true); 
    //$dt2 = round(microtime(true) - $time_start,1);
    try {
        // Create connection
        $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
        // Check connection
        if ($conn->connect_error) {
          throw new Exception('Database error');
          die("Connection failed: " . $conn->connect_error);
        }
        $sql = "SELECT id, sensor, sample, submission, substrate, flavour, thickness, serialnumber, dicing, doping, comment, wire_bonding_date, wire_bonding_location, dose FROM samples";
		$conds=array();
		if(isset($_GET["sample"])){$conds[]= " sample LIKE '".$_GET["sample"]."' ";}
		if(isset($_GET["sensor"])){$conds[]= " sensor LIKE '".$_GET["sensor"]."' ";}
		if(isset($_GET["submission"])){$conds[]= " submission = '".$_GET["submission"]."' ";}
		if(isset($_GET["wire_bonding_date"])){$conds[]= " wire_bonding_date = '".$_GET["wire_bonding_date"]."' ";}
		if(isset($_GET["wbd"])){$conds[]= " wire_bonding_date = '".$_GET["wbd"]."' ";}
		if(count($conds)==0){$sql .= " WHERE sensor LIKE '%MALTA%'";}
		else{ $sql .= " WHERE ".join("AND",$conds); }
		$result = $conn->query($sql);
        $conn->close();
    }catch (Exception $e) {
        $dbOK=False;
        echo "<h2>Error connecting to database</h2>";//.$e->getMessage();
    }
  ?>
<p class="TITLE">Samples database</p>

<?php
if ($dbOK==True){?>
<table>
<tr><td>You can use basic filter operands such as:</td><td>For example, search for the sample: </td></tr>
<tr><td><pre>| or  OR , < <= >= >, *, ! or !=, etc</pre></td><td><pre>W**R2</pre></td></tr>
</table>
<span id="search_list">
	<input  hidden class="getsearch" type="search" data-column="1">
	<button onclick="export_tablesorter('search_table');">CSV</button> Export <span class="search_table_row_counter"></span> <button id="reset-link" style="float: right;">Reset filters</button>
	<div id="pager" class="pager">
	  <form>
	    <img src="../img/first.png" class="first"/>
	    <img src="../img/prev.png" class="prev"/>
	    <!-- the "pagedisplay" can be any element, including an input -->
	    <span class="pagedisplay" data-pager-output-filtered="{startRow:input} &ndash; {endRow} / {filteredRows} of {totalRows} total rows"></span>
	    <img src="../img/next.png" class="next"/>
	    <img src="../img/last.png" class="last"/>
	    <select class="pagesize">
	      <option value="10">10</option>
	      <option value="20">20</option>
	      <option value="30">30</option>
	      <option value="40">40</option>
	      <option value="all">All Rows</option>
	    </select>
	  </form>
	</div>
	<table id="search_table" class="tablesorter" style="font-size: smaller">
		<thead>
			<th class="first-name filter-select" data-placeholder="Select...">Sensor</th>
			<th data-placeholder="Search...">Sample </th>
			<th class="first-name filter-select" data-placeholder="Select...">Submission</th>
			<th class="first-name filter-select" data-placeholder="Select...">Substrate</th>
			<th class="first-name filter-select" data-placeholder="Select...">Flavor</th>
			<th class="first-name filter-select" data-placeholder="Select...">Thickness</th>
			<th data-placeholder="Search...">Serial</th>
			<th class="first-name filter-select" data-placeholder="Select...">Dicing</th>
			<th class="first-name filter-select" data-placeholder="Select...">Doping</th>
			<th data-placeholder="Search...">Comment</th>
			<th class="first-name filter-select" data-placeholder="Select...">Wired</th>
			<th class="first-name filter-select" data-placeholder="Select...">Wiredat</th>
			<th class="first-name filter-select" data-placeholder="Select...">Dose</th>
		</thead>
		<tbody id="search_tbody">
		<?php
		if ($result->num_rows > 0) {
		// output data of each row
			while($row = $result->fetch_assoc()) {
			//echo "<tr><td>".$row["id"]. " - Name: " . $row["sample"]. " " . $row["sensor"]. "<br>";
			 echo "<tr><td>".$row["sensor"]."</td>".
"<td>".$row["sample"]."<a target=\"_blank\" href=\"sample.php?uid=".$row["id"]."\"> <img title=\"Full description in new tab\" class=\"cursor newtabicon\" src=\"../img/newtab.png\"></a></td>".
"<td>".$row["submission"]."</td>".
"<td>".$row["substrate"]."</td>".
"<td>".$row["flavour"]."</td>".
"<td>".$row["thickness"]."</td>".
"<td>".$row["serialnumber"]."</td>".
"<td>".$row["dicing"]."</td>".
"<td>".$row["doping"]."</td>".
"<td>".$row["comment"]."</td>".
"<td>".$row["wire_bonding_date"]."</td>".
"<td>".$row["wire_bonding_location"]."</td>".
"<td>".$row["dose"]."</td></tr>";
			}
		} else {
		echo "<tr><td>Empty</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		?>
		
		</tbody>
	</table>
<?php 
}else{
    echo "Error connecting to database";
}
?>
</span>
</div>
<?php
  show_footer();
  // Display Script End time
  ?>
</div>
<script>
	var pager=true;
	var def_visible_rows=40;
	process_table2("search", 0,0, pager);
</script>
</body>
</html>

